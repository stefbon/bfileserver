/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>
#include <sys/statvfs.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"

#include "sftp-common.h"
#include "filehandle.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "shared/sftp-handle.h"
#include "shared/sftp-reply.h"
#include "shared/sftp-attributes-read.h"
#include "shared/sftp-path.h"

void sftp_op_statvfs(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);
void sftp_op_fstatvfs(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);
void sftp_op_fsync(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);

static struct sftp_extension_s extensions[] = {
    { .len		= 19,
      .name		= "statvfs@openssh.com",
      .cb		= sftp_op_statvfs,
    },
    { .len		= 20,
      .name		= "fstatvfs@openssh.com",
      .cb		= sftp_op_statvfs,
    },
    { .len		= 17,
      .name		= "fsync@openssh.com",
      .cb		= sftp_op_fsync,
    },
    { .len		= 0,
      .name		= NULL,
      .cb		= NULL,
    },
};

struct sftp_extension_s *get_next_sftp_extension(struct sftp_extension_s *extension)
{

    if (extension==NULL) return &extensions[0];

    if ((char *) extension >= (char *) &extensions[0]) {
	unsigned int count = (((char *) extension - (char *) &extensions[0]) / sizeof (struct sftp_extension_s));

	if (count <= sizeof(extensions) / sizeof (struct sftp_extension_s)) {

	    extension=&extensions[count+1];

	    if (extension->len>0) return extension;

	}

    }

    return NULL;

}

static struct sftp_extension_s *get_sftp_extension(char *name, unsigned int len)
{
    unsigned int count=0;
    struct sftp_extension_s *extension=&extensions[count];

    while (extension->len>0) {

	if (extension->len==len && strncmp(name, extension->name, len)==0) goto found;
	count++;
	extension=&extensions[count];

    }

    return NULL;

    found:

    return extension;

}

/* SSH_FXP_EXTENDED
    message has the form:
    - byte 				SSH_FXP_OPEN
    - uint32				id
    - string				name extended request
    - specfic data
    */

void sftp_op_extended(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)

{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_extended (%i)", (int) gettid());

    /* message should at least have 4 bytes for the path string, and 4 for the flags
	note an empty path is possible */

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	/* sftp packet size is at least:
	    - 4 + len ... name  */

	if (header->len >= len + 4) {
	    struct ssh_user_s *user=&session->user;
	    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
	    struct sftp_extension_s *extension=NULL;

	    extension=get_sftp_extension(&buffer[pos], len);
	    pos+=len;

	    if (extension) {

		header->len -= pos;
		(* extension->cb)(session, header, &buffer[pos]);
		return;

	    }

	    status=SSH_FX_OP_UNSUPPORTED;

	}

    }

    error:

    logoutput("sftp_op_extended: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_EXTENDED - statvfs@openssh.com
    message has the form:
    - string				path
    */

void sftp_op_statvfs(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_extended - statvfs$openssh.com (%i)", (int) gettid());

    /* handle is 16 bytes long, so message is 4 + 16 + 8 + 4 = 32 bytes */

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (header->len==4 + len) {
	    unsigned int lenpath=get_fullpath_len(&session->user, len, &buffer[pos]);
	    char path[lenpath];
	    struct statvfs st;

	    get_fullpath(&session->user, len, &buffer[pos], path);

	    if (statvfs(path, &st)==0) {
		char data[88];

		pos=0;

		store_uint64(&data[pos], st.f_bsize);
		pos+=8;
		store_uint64(&data[pos], st.f_frsize);
		pos+=8;
		store_uint64(&data[pos], st.f_blocks);
		pos+=8;
		store_uint64(&data[pos], st.f_bfree);
		pos+=8;
		store_uint64(&data[pos], st.f_bavail);
		pos+=8;
		store_uint64(&data[pos], st.f_files);
		pos+=8;
		store_uint64(&data[pos], st.f_ffree);
		pos+=8;
		store_uint64(&data[pos], st.f_favail);
		pos+=8;
		store_uint64(&data[pos], st.f_fsid);
		pos+=8;
		store_uint64(&data[pos], st.f_flag);
		pos+=8;
		store_uint64(&data[pos], st.f_namemax);
		pos+=8;

		reply_sftp_extension(session, header->id, data, pos);
		return;

	    } else {

		if (errno==ENOENT) {

		    status=SSH_FX_NO_SUCH_FILE;

		} else if (errno==EACCES) {

		    status=SSH_FX_PERMISSION_DENIED;

		} else if (errno==ENOTDIR) {

		    status=SSH_FX_NOT_A_DIRECTORY;

		} else {

		    status=SSH_FX_FAILURE;

		}

	    }

	}

    }

    logoutput("sftp_op_statvfs: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

/* SSH_FXP_EXTENDED - fstatvfs@openssh.com
    message has the form:
    - string				handle
    */

void sftp_op_fstatvfs(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_extended - fstatvfs$openssh.com (%i)", (int) gettid());

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (header->len==4 + len && len==SFTP_HANDLE_SIZE) {
	    struct commonhandle_s *commonhandle=find_sftp_commonhandle(&buffer[pos], session);
	    struct statvfs st;

	    /* use commonhandle cause it can be file or a directory */

	    if (commonhandle==NULL) {

		status=SSH_FX_INVALID_HANDLE;
		goto out;

	    }

	    if (fstatvfs(commonhandle->fd, &st)==0) {
		char data[88];

		pos=0;

		store_uint64(&data[pos], st.f_bsize);
		pos+=8;
		store_uint64(&data[pos], st.f_frsize);
		pos+=8;
		store_uint64(&data[pos], st.f_blocks);
		pos+=8;
		store_uint64(&data[pos], st.f_bfree);
		pos+=8;
		store_uint64(&data[pos], st.f_bavail);
		pos+=8;
		store_uint64(&data[pos], st.f_files);
		pos+=8;
		store_uint64(&data[pos], st.f_ffree);
		pos+=8;
		store_uint64(&data[pos], st.f_favail);
		pos+=8;
		store_uint64(&data[pos], st.f_fsid);
		pos+=8;
		store_uint64(&data[pos], st.f_flag);
		pos+=8;
		store_uint64(&data[pos], st.f_namemax);
		pos+=8;

		reply_sftp_extension(session, header->id, data, pos);
		return;

	    } else {

		if (errno==ENOENT) {

		    status=SSH_FX_NO_SUCH_FILE;

		} else if (errno==ENOTDIR) {

		    status=SSH_FX_NOT_A_DIRECTORY;

		} else if (errno==EACCES) {

		    status=SSH_FX_PERMISSION_DENIED;

		} else if (errno==EBADF) {

		    status=SSH_FX_INVALID_HANDLE;

		} else {

		    status=SSH_FX_FAILURE;

		}

	    }

	}

    }

    out:

    logoutput("sftp_op_statvfs: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

/* SSH_FXP_EXTENDED - fsync@openssh.com
    message has the form:
    - string				handle
    */

void sftp_op_fsync(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_extended - fstatvfs$openssh.com (%i)", (int) gettid());

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (header->len==4 + len && len==SFTP_HANDLE_SIZE) {
	    unsigned int error=0;
	    struct commonhandle_s *commonhandle=find_sftp_commonhandle(&buffer[pos], session);
	    struct statvfs st;

	    /* use commonhandle cause it can be file or a directory */

	    if (commonhandle==NULL) {

		status=SSH_FX_INVALID_HANDLE;
		goto out;

	    }

	    if (fsync(commonhandle->fd)==0) {

		reply_sftp_status_simple(session, header->id, SSH_FX_OK);
		return;

	    } else {

		if (errno==ENOENT) {

		    status=SSH_FX_NO_SUCH_FILE;

		} else if (errno==ENOTDIR) {

		    status=SSH_FX_NOT_A_DIRECTORY;

		} else if (errno==EBADF) {

		    status=SSH_FX_INVALID_HANDLE;


		} else {

		    status=SSH_FX_FAILURE;

		}

	    }

	}

    }

    out:

    logoutput("sftp_op_fsync: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

