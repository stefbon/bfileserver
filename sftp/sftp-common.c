/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "shared/sftp-reply-status.h"
#include "shared/sftp-op.h"
#include "sftp-op-extended.h"

#include "sftp-common.h"

/*
    for sftp init data looks like:

    - 4 bytes				len sftp packet excl this field
    - byte				sftp type
    - 4 bytes				sftp version server
    - extension-pair			extension[0..n]

    where one extension has the form:
    - string name
    - string data

    for version 6 the extensions supported are:
    - supported2
    - acl-supported
*/

static int send_sftp_init(struct ssh_session_s *session)
{
    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
    unsigned int attrib_extensions_count=0;
    unsigned int extensions_count=0;
    unsigned int len_h=4 + strlen("supported2");
    unsigned int len_hs=32 + get_attrib_extensions(session, NULL, 0, &attrib_extensions_count) + get_sftp_extensions(session, NULL, 0, &extensions_count);
    unsigned int len_a=4 + strlen("acl-supported");
    unsigned int len_as=4;
    unsigned int len=9 + len_h + len_hs + len_a + len_as;
    char data[len];
    unsigned int pos=0;
    unsigned int error=0;

    /* SSH_FXP_VERSION message */

    store_uint32(&data[pos], len-4);
    pos+=4;

    data[pos]=SSH_FXP_VERSION;
    pos++;

    store_uint32(&data[pos], sftp->version);
    pos+=4;

    /* header supported2 */

    pos+=copy_ssh_char(&data[pos], "supported2");

    /* supported-structure */

    store_uint32(&data[pos], len_hs);
    pos+=4;

    /* valid attribute flags */

    store_uint32(&data[pos], get_supported_attribute_flags(session));
    pos+=4;

    /* valid attribute bits */

    store_uint32(&data[pos], get_supported_attribute_bits(session));
    pos+=4;

    /* supported open flags */

    store_uint32(&data[pos], get_supported_open_flags(session));
    pos+=4;

    /* supported access mask */

    store_uint32(&data[pos], get_supported_acl_access_mask(session));
    pos+=4;

    /* max read size */

    store_uint32(&data[pos], get_supported_max_readsize(session));
    pos+=4;

    /* supported open-block-vector */

    store_uint16(&data[pos], get_supported_open_block_flags(session));
    pos+=2;

    /* supported block-vector */

    store_uint16(&data[pos], get_supported_block_flags(session));
    pos+=2;

    /* attrib extension count */

    store_uint32(&data[pos], attrib_extensions_count);
    pos+=4;

    pos+=get_attrib_extensions(session, &data[pos], len - pos, &attrib_extensions_count);

    /* attrib extension count */

    store_uint32(&data[pos], extensions_count);
    pos+=4;

    pos+=get_sftp_extensions(session, &data[pos], len - pos, &extensions_count);

    /* acl supported */

    pos+=copy_ssh_char(&data[pos], "acl-supported");

    /* acl structure */

    store_uint32(&data[pos], len_as);
    pos+=4;

    /* acl capabilities */

    store_uint32(&data[pos], get_supported_acl_cap(session));
    pos+=4;

    /* store length in first 4 bytes */

    store_uint32(&data[0], pos-4);

    if (send_session_data(session, data, pos, &error)==0) {

	sftp->status=SFTP_STATUS_UP;
	return 0;

    }

    if (error==0) error=EIO;
    logoutput("send_sftp_init: error %i sending init (%s)", error, strerror(error));

    return -1;

}

/* sometimes receiving a message is an error
    like in init phase after receiving the init message
    the client should not send any message before the sending of the 
    version message */

void process_sftp_error(struct ssh_payload_s *payload)
{
    struct ssh_session_s *session=payload->session;

    free(payload);
    finish_session(session);
}

/* process an incoming message and call the right cb */

void process_sftp_session(struct ssh_payload_s *payload)
{
    struct ssh_session_s *session=payload->session;
    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
    char *buffer=payload->buffer;
    struct sftp_header_s header;
    unsigned int pos=0;

    if (payload->size<9) goto error;

    header.len=get_uint32(&buffer[pos]);
    pos+=4;

    if (payload->size != header.len + 4) goto error;

    header.type=(unsigned char) buffer[pos];
    pos++;
    header.len--;

    header.id=get_uint32(&buffer[pos]);
    pos+=4;
    header.len-=4;

    logoutput("process_sftp_session: received %i bytes length %i type %i id %i", payload->size, header.len, header.type, header.id);

    (* sftp->cb[header.type])(session, &header, &buffer[pos]);

    logoutput("process_sftp_session: free");

    free(payload);
    payload=NULL;

    logoutput("process_sftp_session: return");

    return;

    error:

    free(payload);
    payload=NULL;
    finish_session(session);

}

/* function to process the first (init) message */

void process_sftp_init(struct ssh_payload_s *payload)
{
    unsigned int clientversion=0;
    unsigned int pos=0;
    unsigned int length=0;
    char *buffer=payload->buffer;
    struct ssh_session_s *session=payload->session;
    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;

    logoutput("process_sftp_init");

    /* set function to process to handle error:
	the client has to wait for reply! */

    set_session_process_payload(session, process_sftp_error);

    if (payload->size!=9) {

	logoutput("process_sftp_init: error size not equal to 9 (%i)", payload->size);
	goto disconnect;

    }

    length=get_uint32(&buffer[pos]);
    pos+=4;

    if (length+4 != payload->size) {

	logoutput("process_sftp_init: length + 4 not equal to size (%i=%i)", length, payload->size);
	goto disconnect;

    }

    if ((unsigned char) buffer[pos] != SSH_FXP_INIT) {

	logoutput("process_sftp_init: type byte not SSH_FXP_INIT");
	goto disconnect;

    }

    pos++;

    if (sftp->status!=SFTP_STATUS_INIT) {

	logoutput("process_sftp_init: receiving init while not in int phase");
	goto disconnect;

    }

    clientversion=get_uint32(&buffer[pos]);
    pos+=4;

    if (clientversion<6) {

	logoutput("process_sftp_init: received unsupported version %i", clientversion);
	goto disconnect;

    }

    free(payload);
    payload=NULL;

    if (send_sftp_init(session)==0) {

	/* switch to processing "normal" payload */

	set_session_process_payload(session, process_sftp_session);
	session->status.status=SSH_STATUS_SUBSYSTEM;

    } else {

	goto disconnect;

    }

    return;

    disconnect:

    if (payload) {

	free(payload);
	payload=NULL;

    }

    finish_session(session);

}

static void process_sftp_notsupported(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    if (reply_sftp_status_simple(session, header->id, SSH_FX_OP_UNSUPPORTED)==-1) {

	logoutput_info("process_sftp_notsupported: error");
    }
}

void init_sftp_subsystem(struct ssh_session_s *session)
{
    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
    unsigned int error=0;

    sftp->status=SSH_FXP_INIT;
    sftp->version=6;

    for (unsigned int i=0; i<256; i++) sftp->cb[i]=process_sftp_notsupported;

    set_session_process_payload(session, process_sftp_init);

    sftp->cb[SSH_FXP_LSTAT]=sftp_op_lstat;
    sftp->cb[SSH_FXP_STAT]=sftp_op_stat;
    sftp->cb[SSH_FXP_FSTAT]=sftp_op_fstat;

    sftp->cb[SSH_FXP_SETSTAT]=sftp_op_setstat;
    sftp->cb[SSH_FXP_FSETSTAT]=sftp_op_fsetstat;

    sftp->cb[SSH_FXP_OPEN]=sftp_op_open;
    sftp->cb[SSH_FXP_READ]=sftp_op_read;
    sftp->cb[SSH_FXP_WRITE]=sftp_op_write;
    sftp->cb[SSH_FXP_CLOSE]=sftp_op_close;

    sftp->cb[SSH_FXP_OPENDIR]=sftp_op_opendir;
    sftp->cb[SSH_FXP_READDIR]=sftp_op_readdir;

    sftp->cb[SSH_FXP_READLINK]=sftp_op_readlink;

    sftp->cb[SSH_FXP_EXTENDED]=sftp_op_extended;

    pthread_mutex_init(&sftp->mutex, NULL);
    sftp->extended_op_start=SFTP_EXTENDED_OP_MIN;

}

static void clear_sftp_subsystem(struct sftp_subsystem_s *sftp)
{
}

