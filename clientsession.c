/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <pwd.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#include "logging.h"

#include "main.h"
#include "utils.h"
#include "pathinfo.h"
#include "workerthreads.h"
#include "beventloop.h"
#include "beventloop-signal.h"
#include "beventloop-timer.h"
#include "beventloop-xdata.h"
#include "simple-list.h"
#include "pidfile.h"
#include "localsocket.h"
#include "ssh/ssh-utils.h"

#include "options.h"
#include "clientconnection.h"
#include "clientsession.h"
#include "sftp/sftp-common.h"

#define _RECEIVE_BUFFER_SIZE 12288

static pthread_mutex_t signal_mutex=PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t signal_cond=PTHREAD_COND_INITIALIZER;

struct fs_options_struct fs_options;
static void queue_ssh_data(struct ssh_session_s *session, char *buffer, unsigned int len);

struct ssh_session_s *get_session_connection(struct fs_connection_s *conn)
{
    return (struct ssh_session_s *) ( ((char *) conn) - offsetof(struct ssh_session_s, connection));
}

static void process_payload_dummy(struct ssh_payload_s *payload)
{
    free(payload);
}

/* function to process the first payload
    lock the queue */

static void process_payload_queue_init(void *ptr)
{
    struct ssh_session_s *session=(struct ssh_session_s *) ptr;
    struct ssh_receive_s *receive=&session->receive;
    struct payload_queue_s *queue=&receive->payload_queue;
    struct list_element_s *list=NULL;
    struct ssh_status_s *status=&session->status;
    void (* process_payload_cb)(struct ssh_payload_s *payload);
    pthread_mutex_t *mutex=queue->signal.mutex;

    readqueue:

    pthread_mutex_lock(mutex);
    list=get_list_head(&queue->head, &queue->tail);
    process_payload_cb=queue->process_payload;
    pthread_mutex_unlock(mutex);

    if (list) {
	struct ssh_payload_s *payload=(struct ssh_payload_s *) ( ((char *) list) - offsetof(struct ssh_payload_s, list));

	/* process the first payload: it should be an indication which service to start....
	    sftp, fuse, ...*/

	(* process_payload_cb)(payload);

	if (status->status & SSH_STATUS_FINISH) {

	    terminate_session_unlocked(session);

	} else {

	    receive->queue_data=queue_ssh_data;

	}

    } else {

	/* no payload in list ?? this function is called when there is... should not happen */
	logoutput("process_payload_queue_init: no payload");

    }

}

/* process payload queue */
static void process_payload_queue(void *ptr)
{
    struct ssh_session_s *session=(struct ssh_session_s *) ptr;
    struct ssh_receive_s *receive=&session->receive;
    struct payload_queue_s *queue=&receive->payload_queue;
    struct list_element_s *list=NULL;
    struct ssh_status_s *status=&session->status;
    void (* process_payload_cb)(struct ssh_payload_s *payload);
    pthread_mutex_t *mutex=queue->signal.mutex;

    readqueue:

    pthread_mutex_lock(mutex);
    list=get_list_head(&queue->head, &queue->tail);
    receive->threads++;
    process_payload_cb=queue->process_payload;
    pthread_mutex_unlock(mutex);

    if (list) {
	struct ssh_payload_s *payload=(struct ssh_payload_s *) ( ((char *) list) - offsetof(struct ssh_payload_s, list));

	(* process_payload_cb)(payload);
	goto readqueue;

    }

    pthread_mutex_lock(mutex);

    receive->threads--;
    if (status->status & SSH_STATUS_FINISH) {

	/* when this is the latest thread free the session 
	    (when it's not the latest this will lead to errors */
	if (receive->threads==0) terminate_session_unlocked(session);

    }

    pthread_mutex_unlock(mutex); /* this is possible when session is freed since mutex is static allocated */

}

static void queue_ssh_data(struct ssh_session_s *session, char *buffer, unsigned int len)
{
    struct ssh_payload_s *payload=NULL;

    logoutput("queue_ssh_data: received %i", len);

    payload=malloc(sizeof(struct ssh_payload_s) + len);

    if (payload) {
	struct ssh_receive_s *receive=&session->receive;
	struct payload_queue_s *queue=&receive->payload_queue;
	unsigned int error=0;

	payload->type=0;
	payload->session=session;
	payload->size=len;
	payload->list.next=NULL;
	payload->list.prev=NULL;
	memcpy(payload->buffer, buffer, len);

	pthread_mutex_lock(queue->signal.mutex);

	add_list_element_last(&queue->head, &queue->tail, &payload->list);

	work_workerthread(NULL, 0, process_payload_queue, (void *)session, &error);
	if (error>0) logoutput("work_payload_queue: error %i starting thread (%s)", error, strerror(error));

	pthread_mutex_unlock(queue->signal.mutex);

    } else {

	logoutput("queue_ssh_data: error allocating payload");

    }

}

static void queue_ssh_data_first(struct ssh_session_s *session, char *buffer, unsigned int len)
{
    struct ssh_payload_s *payload=NULL;
    struct ssh_receive_s *receive=&session->receive;
    struct payload_queue_s *queue=&receive->payload_queue;

    /* add payload to queue... it's possible that the client sends a lot of messages making the queue full...
	    howto deal with this situation? 
    */

    pthread_mutex_lock(queue->signal.mutex);

    if (session->status.status==0) {

	payload=malloc(sizeof(struct ssh_payload_s) + len);

	if (payload) {
	    unsigned int error=0;

	    payload->type=0;
	    payload->session=session;
	    payload->size=len;
	    payload->list.next=NULL;
	    payload->list.prev=NULL;
	    memcpy(payload->buffer, buffer, len);

	    add_list_element_last(&queue->head, &queue->tail, &payload->list);

	    session->status.status=SSH_STATUS_INIT;
	    work_workerthread(NULL, 0, process_payload_queue_init, (void *)session, &error);
	    if (error>0) logoutput("work_payload_queue: error %i starting thread (%s)", error, strerror(error));

	} else {

	    logoutput("queue_ssh_data: error allocating payload");

	}

    } else {

	/* receiving an extra message in the init phase is an error
	    client should wait for reply from server */
	finish_session(session);

    }

    pthread_mutex_unlock(queue->signal.mutex);

}

static void queue_ssh_data_ignore(struct ssh_session_s *session, char *buffer, unsigned int len)
{
}

static int init_ssh_user(struct ssh_session_s *session, uid_t uid, unsigned int *error)
{
    struct ssh_user_s *user=&session->user;
    struct passwd *result=NULL;

    memset(user, 0, sizeof(struct ssh_user_s));
    user->size=128;
    user->buffer=NULL;
    user->len_username=0;
    user->len_home=0;

    getpw:

    user->buffer=realloc(user->buffer, user->size);

    if (user->buffer==NULL) {

	*error=ENOMEM;
	goto error;

    }

    if (getpwuid_r(uid, &user->pwd, user->buffer, user->size, &result)==-1) {

	if (errno==ERANGE) {

	    /* buffer too small: try again */
	    user->size+=128;
	    goto getpw;

	}

	*error=errno; /* any other error is fatal */
	goto error;

    }

    logoutput("init_ssh_user: found user %s (uid %i, %s) home %s", result->pw_name, result->pw_uid, result->pw_gecos, result->pw_dir);

    /* keep values used a lot */
    user->len_username=strlen(result->pw_name);
    user->len_home=strlen(result->pw_dir);

    return 0;

    error:

    if (user->buffer) {

	free(user->buffer);
	user->buffer=NULL;

    }

    return -1;

}

static int init_ssh_receive(struct ssh_session_s *session, unsigned int *error)
{
    struct ssh_receive_s *receive=&session->receive;
    struct payload_queue_s *queue=&receive->payload_queue;

    receive->queue_data=queue_ssh_data_first;
    receive->threads=0;
    receive->buffer=malloc(_RECEIVE_BUFFER_SIZE);

    if (receive->buffer) {

	receive->size=_RECEIVE_BUFFER_SIZE;
	memset(receive->buffer, '\0', _RECEIVE_BUFFER_SIZE);

    } else {

	*error=ENOMEM;
	return -1;

    }

    queue->head=NULL;
    queue->tail=NULL;
    queue->process_payload=process_payload_dummy;
    queue->signal.flags=0;
    queue->signal.mutex=&signal_mutex;
    queue->signal.cond=&signal_cond;

    return 0;

}

static void free_ssh_session(struct ssh_session_s *session)
{
    struct ssh_receive_s *receive=&session->receive;
    struct payload_queue_s *queue=&receive->payload_queue;
    struct list_element_s *list=NULL;

    list=get_list_head(&queue->head, &queue->tail);

    while (list) {
	struct ssh_payload_s *payload=(struct ssh_payload_s *) ( ((char *) list) - offsetof(struct ssh_payload_s, list));

	free(payload);
	list=get_list_head(&queue->head, &queue->tail);

    }

    if (receive->buffer) {

	free(receive->buffer);
	receive->buffer=NULL;

    }

    if (session->user.buffer) {

	free(session->user.buffer);
	session->user.buffer=NULL;

    }

    free(session);

}

struct ssh_session_s *create_session(uid_t uid, unsigned char type)
{
    struct ssh_session_s *session=NULL;
    unsigned int error=0;

    session=malloc(sizeof(struct ssh_session_s));

    if (session) {

	memset(session, 0, sizeof(struct ssh_session_s));

	session->status.status=0;
	session->status.type=0;

	session->address.type=type;

	if (init_ssh_user(session, uid, &error)==-1) {

	    logoutput("create_session: failed to init user (%i:%s)", error, strerror(error));
	    free_ssh_session(session);
	    session=NULL;
	    return NULL;

	}

	if (init_ssh_receive(session, &error)==-1) {

	    logoutput("create_session: failed to init receive buffer (%i:%s)", error, strerror(error));
	    free_ssh_session(session);
	    session=NULL;
	    return NULL;

	}

    }

    return session;

}

/*
    set the callback to process the received payload */

void set_session_process_payload(struct ssh_session_s *session, void (* process_payload_cb)(struct ssh_payload_s *payload))
{
    struct ssh_receive_s *receive=&session->receive;
    struct payload_queue_s *queue=&receive->payload_queue;

    pthread_mutex_lock(queue->signal.mutex);
    queue->process_payload=process_payload_cb;
    pthread_mutex_unlock(queue->signal.mutex);

}

void finish_session(struct ssh_session_s *session)
{
    struct ssh_receive_s *receive=&session->receive;
    struct payload_queue_s *queue=&receive->payload_queue;
    struct ssh_status_s *status=&session->status;

    pthread_mutex_lock(queue->signal.mutex);

    queue->process_payload=process_payload_dummy;
    receive->queue_data=queue_ssh_data_ignore;
    status->status |= SSH_STATUS_FINISH;

    pthread_mutex_unlock(queue->signal.mutex);

}

void terminate_session_unlocked(struct ssh_session_s *session)
{
    struct payload_queue_s *queue=&session->receive.payload_queue;

    queue->process_payload=process_payload_dummy;
    disconnect_client_connection(&session->connection, 0);
    free_ssh_session(session);
}

int send_session_data(struct ssh_session_s *session, char *data, unsigned int size, unsigned int *error)
{
    struct fs_connection_s *conn=&session->connection;
    ssize_t written=0;

    written=write(conn->fd, data, size);
    if (written==-1) {

	*error=errno;
	return -1;

    }

    // logoutput("send_session_data: %i bytes send", (unsigned int) written);
    *error=0;

    return 0;

}
