/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef FUSE_BACKUP_DB_SQLITE_COMMON_H
#define FUSE_BACKUP_DB_SQLITE_COMMON_H


// Prototypes

int sqlite_execute_simple_common(sqlite3 *sqlite, char *sqlquery, unsigned int len, unsigned char *changed, void (* _get_data_cb)(sqlite3_stmt *stmt, void *data), void *data, unsigned int *error);
void *sqlite_getdata_common(sqlite3 *sqlite, char *sqlquery, unsigned int len, unsigned int *error);
void free_data_sqlite(void *data);

void *get_changed_files_sqlite(struct db_conn_s *db_conn, unsigned long long backupid, unsigned long long snapshot);
unsigned char get_changed_file_sqlite(void *data, unsigned long long *id, unsigned long long *fileid);

#endif
