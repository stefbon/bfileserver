/*
  2010, 2011, 2012, 2013, 2014 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#define LOGGING
#include "logging.h"

#include "backup/db-common-base.h"
#include "backup/db-common.h"
#include "db-sqlite-check.h"

extern int sqlite_execute_simple_common(sqlite3 *sqlite, char *sqlquery, unsigned int len, unsigned char *changed, void (* _get_data_cb)(sqlite3_stmt *stmt, void *data), void *data, unsigned int *error);

/*
    file table: the directory structure

    - id: unique id per entry (compare: ino)
    - type: (deleted) file or (deleted) directory
    - name: name of entry
    - parent: id of parent

*/

static int create_dentry_table(sqlite3 *sqlite, unsigned int *error)
{
    char sqlquery[512];
    uid_t guest_owner=(uid_t) -1;
    gid_t guest_group=(gid_t) -1;
    unsigned int len=snprintf(sqlquery, 512,
	    "CREATE TABLE IF NOT EXISTS dentry "
	    "(id INTEGER PRIMARY KEY AUTOINCREMENT, "
	    "owner UNSIGNED INT DEFAULT 0,"
	    "type UNSIGNED INT DEFAULT 0,"
	    "name TEXT NOT NULL,"
	    "parent UNSIGNED BIGINT DEFAULT 0,"
	    "mode UNSIGNED INT DEFAULT 0,"
	    "nlink UNSIGNED INT DEFAULT 2,"
	    "unix_user UNSIGNED INT DEFAULT %i,"
	    "unix_group UNSIGNED INT DEFAULT %i,"
	    "mtim_sec UNSIGNED INT DEFAULT 0,"
	    "mtim_nsec UNSIGNED INT DEFAULT 0,"
	    "ctim_sec UNSIGNED INT DEFAULT 0,"
	    "ctim_nsec UNSIGNED INT DEFAULT 0,"
	    "UNIQUE (parent, name))", guest_owner, guest_group);

    return sqlite_execute_simple_common(sqlite, sqlquery, len, NULL, NULL, NULL, error);

}

/* version table: the versions per file */

static int create_version_table(sqlite3 *sqlite, unsigned int *error)
{
    char sqlquery[512];
    int len=snprintf(sqlquery, 512, "CREATE TABLE IF NOT EXISTS version"
				    " (id INTEGER PRIMARY KEY AUTOINCREMENT, "
				    "fileid UNSIGNED BIGINT, "
				    "snapshot UNSIGNED BIGINT DEFAULT 0, "
				    "type UNSIGNED INT DEFAULT 0, "
				    "subtype UNSIGNED INT DEFAULT 0, "
				    "mode UNSIGNED INT DEFAULT 0,"
				    "nlink UNSIGNED INT DEFAULT 2,"
				    "unix_user UNSIGNED INT DEFAULT %i,"
				    "unix_group UNSIGNED INT DEFAULT %i,"
				    "mtim_sec UNSIGNED INT DEFAULT 0,"
				    "mtim_nsec UNSIGNED INT DEFAULT 0,"
				    "ctim_sec UNSIGNED INT DEFAULT 0,"
				    "ctim_nsec UNSIGNED INT DEFAULT 0,"
				    "UNIQUE (fileid, snapshot))");
    return sqlite_execute_simple_common(sqlite, sqlquery, len, NULL, NULL, NULL, error);
}

static void _check_table(sqlite3_stmt *stmt, void *data)
{
    unsigned char *found=(unsigned char *) data;
    if (stmt) *found=1;
}

static int check_table_exists(struct db_conn_s *db_conn, char *name, int (* _create_table_cb)(sqlite3 *sqlite, unsigned int *error), unsigned int rebuild)
{
    unsigned char found=0;
    char sqlquery[512];
    int len=0;
    unsigned int error=0;

    len=snprintf(sqlquery, 512, "SELECT * FROM sqlite_master WHERE type='table' AND name='%s'", name);

    if (sqlite_execute_simple_common(db_conn->sqlite, sqlquery, len, NULL, _check_table, &found, &error)==0) {

	if (found==0) {

	    if (_create_table_cb(db_conn->sqlite, &error)==0) {

		logoutput("check_table_exists: table %s created", name);

	    } else {

		logoutput_error("check_table_exists: error create table %s", name);
		return -1;

	    }

	}

    }

    return 0;

}

int check_db_sqlite(struct db_conn_s *db_conn, unsigned int *error)
{
    int result=0;

    *error=0;

    logoutput("check_db_sqlite: check table dentry");

    if (check_table_exists(db_conn, "dentry", create_dentry_table, FUSE_BACKUP_DB_TABLE_FILE)==-1) {

	result=-1;
	goto out;

    }

    logoutput("check_db_sqlite: check table version");

    if (check_table_exists(db_conn, "version", create_version_table, FUSE_BACKUP_DB_TABLE_VERSION)==-1) {

	result=-1;
	goto out;

    }

    out:

    return result;

}
