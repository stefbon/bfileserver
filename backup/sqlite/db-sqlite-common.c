/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include "backup/db-common-base.h"
#include "backup/db-common.h"

#include "logging.h"

extern struct fs_options_s fs_options;

int sqlite_execute_simple_common(sqlite3 *sqlite, char *sqlquery, unsigned int len, unsigned char *changed, void (* _get_data_cb)(sqlite3_stmt *stmt, void *data), void *data, unsigned int *error)
{
    sqlite3_stmt *stmt=NULL;
    int result=0;

    result=sqlite3_prepare_v2(sqlite, sqlquery, len, &stmt, NULL);

    if (result==SQLITE_OK) {
	int changes=0;

	if (changed) {

	    *changed=0;
	    changes=sqlite3_changes(sqlite);

	}

	dostep:

	result=sqlite3_step(stmt);

	if (result==SQLITE_DONE) {

	    if (_get_data_cb) (* _get_data_cb)(NULL, data);

	    result=0;

	} else if (result==SQLITE_ROW) {

	    if (_get_data_cb) (* _get_data_cb)(stmt, data);

	    result=0;

	} else {

	    if (result==SQLITE_ERROR) {

		logoutput("sqlite_execute_simple_common: error executing sql statement %s", sqlquery);
		*error=EIO;

	    } else {

		logoutput("sqlite_execute_simple_common: unknown return code (%i) executing sql statement %s", result, sqlquery);
		*error=EIO;

	    }

	    result=-1;

	}

	sqlite3_finalize(stmt);

	if (changed) {

	    if (sqlite3_changes(sqlite)>changes) *changed=1;

	}

    } else {

	logoutput("sqlite_execute_simple_common: error (%i) preparing sql statement %s", result, sqlquery);

    }

    return result;

}

void *sqlite_getdata_common(sqlite3 *sqlite, char *sqlquery, unsigned int len, unsigned int *error)
{
    void *data=NULL;
    sqlite3_stmt *sql_stmt=NULL;

    if (sqlite3_prepare_v2(sqlite, sqlquery, len, &sql_stmt, NULL)==SQLITE_OK) {

	data=(void *) sql_stmt;
	*error=0;

    } else {

	if (*error==0) *error=EIO;

    }

    return data;

}

void free_data_sqlite(void *data)
{
    sqlite3_stmt *sql_stmt=(sqlite3_stmt *) data;
    sqlite3_finalize(sql_stmt);
}

/*
    count the number of versions for a specific parent directory (backupid) for a snapshot
*/

void *get_changed_files_sqlite(struct db_conn_s *db_conn, unsigned long long backupid, unsigned long long snapshot)
{
    char sqlquery[512];
    int len=0;
    unsigned int error=0;

    len=snprintf(sqlquery, 512, "SELECT id,fileid FROM version WHERE snapshot=%lli AND fileid IN (SELECT id FROM file WHERE parent=%lli)", snapshot, backupid);

    return sqlite_getdata_common(db_conn->sqlite, sqlquery, len, &error);

}

unsigned char get_changed_file_sqlite(void *data, unsigned long long *id, unsigned long long *fileid)
{
    sqlite3_stmt *sql_stmt=(sqlite3_stmt *) data;
    unsigned char step=0;
    int result=0;

    result=sqlite3_step(sql_stmt);

    if (result==SQLITE_ROW) {

	*id=sqlite3_column_int64(sql_stmt, 0);
	*fileid=sqlite3_column_int64(sql_stmt, 1);
	step=1;

    }

    return step;

}
