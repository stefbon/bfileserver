/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include "backup/db-common-base.h"
#include "backup/db-common.h"
#include "db-sqlite-common.h"

#define LOGGING
#include "logging.h"

struct _get_record_s {
    struct db_name_s				*db_name;
    unsigned long long 				db_id_parent;
    struct db_stat_s 				*db_stat;
    signed char					found;
    unsigned int				error;
};

static void _get_record_cb(sqlite3_stmt *sql_stmt, void *data)
{
    struct _get_record_s *_get_record=(struct _get_record_s *) data;
    struct db_stat_s *db_stat=_get_record->db_stat;

    if (sql_stmt) {

	_get_record->found=1;

	_get_record->db_id_parent=(unsigned long long) sqlite3_column_int64(sql_stmt, 0);
	db_stat->type=(unsigned int) sqlite3_column_int(sql_stmt, 1);

	if (_get_record->db_name) {

	    /* is the name already present (=buffer) if so use that */

	    if (_get_record->db_name->name) {
		char *name=(char *) sqlite3_column_text(sql_stmt, 2);
		unsigned int len=strlen(name);

		if (len<_get_record->db_name->len) {

		    memcpy(_get_record->db_name->name, name, len+1);
		    _get_record->db_name->len=len;

		} else {

		    memcpy(_get_record->db_name->name, name, _get_record->db_name->len);
		    _get_record->error=ENAMETOOLONG;

		}

	    } else {

		_get_record->db_name->name=strdup((char *) sqlite3_column_text(sql_stmt, 2));

		if (_get_record->db_name->name) {

		    _get_record->db_name->len=strlen(_get_record->db_name->name);

		} else {

		    _get_record->error=ENOMEM;
		    _get_record->found=-1;

		}

	    }

	}

	db_stat->mode=(unsigned int) sqlite3_column_int(sql_stmt, 3);
	db_stat->nlink=(unsigned int) sqlite3_column_int(sql_stmt, 4);
	db_stat->uid=(uid_t) sqlite3_column_int(sql_stmt, 5);
	db_stat->gid=(gid_t) sqlite3_column_int(sql_stmt, 6);
	db_stat->mtim=(unsigned int) sqlite3_column_int(sql_stmt, 7);
	db_stat->mtim_nsec=(unsigned int) sqlite3_column_int(sql_stmt, 8);
	db_stat->ctim=(unsigned int) sqlite3_column_int(sql_stmt, 9);
	db_stat->ctim_nsec=(unsigned int) sqlite3_column_int(sql_stmt, 10);

    } else {

	_get_record->found=0;

    }

}

signed char get_dentry_sqlite(struct db_conn_s *db_conn, struct db_name_s *db_name, unsigned long long *db_id_parent, struct db_stat_s *db_stat, unsigned int *error)
{
    char sqlquery[512];
    int len=0;
    struct _get_record_s _get_record;

    _get_record.db_id_parent=0;
    _get_record.db_name=db_name;
    _get_record.db_stat=db_stat;
    _get_record.found=0;
    _get_record.error=0;

    len=snprintf(sqlquery, 512, "SELECT parent,type,name,mode,nlink,unix_user,unix_group,mtim_sec,mtim_nsec,ctim_sec,ctim_nsec FROM dentry WHERE id=%lli", db_stat->id);

    if (sqlite_execute_simple_common(db_conn->sqlite, sqlquery, len, NULL, _get_record_cb, (void *) &_get_record, error)==0) {

	if (db_id_parent) *db_id_parent=_get_record.db_id_parent;
	*error=_get_record.error;

    }

    return _get_record.found;

}

/*
    function to lookup an entry (xname) in a directory (parent_db_id)
    in the mysql db

    return true: entry found and parameters filled

    - *db_id: id of the entry found

*/

struct _lookup_dentry_sqlite_s {
    struct db_stat_s 				*db_stat;
    signed char					found;
    unsigned int				error;
};

static void _lookup_dentry_cb(sqlite3_stmt *stmt, void *data)
{
    struct _lookup_dentry_sqlite_s *_lookup=(struct _lookup_dentry_sqlite_s *) data;
    struct db_stat_s *db_stat=_lookup->db_stat;

    if (stmt) {

	db_stat->id=(unsigned long long) sqlite3_column_int64(stmt, 0);
	db_stat->type=(unsigned int) sqlite3_column_int(stmt, 1);
	db_stat->mode=(unsigned int) sqlite3_column_int(stmt, 2);
	db_stat->nlink=(unsigned int) sqlite3_column_int(stmt, 3);
	db_stat->uid=(uid_t) sqlite3_column_int(stmt, 4);
	db_stat->gid=(gid_t) sqlite3_column_int(stmt, 5);
	db_stat->mtim=(unsigned int) sqlite3_column_int(stmt, 6);
	db_stat->mtim_nsec=(unsigned int) sqlite3_column_int(stmt, 7);
	db_stat->ctim=(unsigned int) sqlite3_column_int(stmt, 8);
	db_stat->ctim_nsec=(unsigned int) sqlite3_column_int(stmt, 9);

	_lookup->found=1;
	_lookup->error=0;

    } else {

	_lookup->found=0;

    }

}

signed char lookup_dentry_sqlite(struct db_conn_s *db_conn, unsigned long long db_id_parent, struct db_name_s *db_name, unsigned int owner, struct db_stat_s *db_stat)
{
    unsigned char found=0;
    char sqlquery[512];
    int len=0, result=0;
    struct _lookup_dentry_sqlite_s _lookup;
    char name[db_name->len + 1];
    unsigned int error=0;

    memcpy(name, db_name->name, db_name->len);
    name[db_name->len]='\0';

    logoutput("lookup_dentry_sqlite: lookup parent %i name %s", db_id_parent, name);

    _lookup.db_stat=db_stat;
    _lookup.found=0;
    _lookup.error=ENOENT;

    len=snprintf(sqlquery, 512, "SELECT id,type,mode,nlink,unix_user,unix_group,mtim_sec,mtim_nsec,ctim_sec,ctim_nsec FROM dentry WHERE parent=%lli AND name='%s' AND owner=%i", db_id_parent, name, owner);

    if (sqlite_execute_simple_common(db_conn->sqlite, sqlquery, len, NULL, _lookup_dentry_cb, (void *) &_lookup, &error)==0) {

	logoutput("lookup_dentry_sqlite: error %i", error);

    }

    return _lookup.found;

}

static void _set_dentry_stat_cb(sqlite3_stmt *sql_stmt, void *data)
{
    unsigned char *found=(unsigned char *) data;

    if (sql_stmt) {

	*found=1;

    } else {

	*found=0;

    }

}

unsigned char set_dentry_stat_sqlite(struct db_conn_s *db_conn, struct db_stat_s *db_stat)
{
    unsigned char found=0;
    unsigned int error=0;
    char sqlquery[512];
    int len=0;

    len=snprintf(sqlquery, 512, "UPDATE dentry SET type=%i,mode=%i,nlink=%i,unix_user=%i,unix_group=%i,mtim_sec=%li,mtim_nsec=%li,ctim_sec=%li,ctim_nsec=%li WHERE id=%lli",
				    db_stat->type, db_stat->mode, db_stat->nlink, (unsigned int)db_stat->uid, (unsigned int)db_stat->gid, db_stat->mtim, db_stat->mtim_nsec, db_stat->ctim, db_stat->ctim_nsec, db_stat->id);

    logoutput("set_dentry_stat_sqlite: query %s", sqlquery);

    if (sqlite_execute_simple_common(db_conn->sqlite, sqlquery, len, NULL, _set_dentry_stat_cb, (void *) &found, &error)==-1) {

	logoutput("set_dentry_stat_sqlite: error (%i:%s)", error, strerror(error));

    }

    return found;

}

void delete_dentry_record_sqlite(struct db_conn_s *db_conn, unsigned long long db_id)
{
    unsigned int error=0;
    char sqlquery[512];
    int len=0;

    len=snprintf(sqlquery, 512, "DELETE FROM dentry WHERE id=%lli", db_id);

    if (sqlite_execute_simple_common(db_conn->sqlite, sqlquery, len, NULL, NULL, NULL, &error)==0) {

	logoutput("delete_file_found_sqlite: deleted file with id %lli", db_id);

    }

}

void *opendir_sqlite(struct db_conn_s *db_conn, unsigned long long db_id, unsigned char type, unsigned int owner, unsigned int *error)
{
    char sqlquery[512];
    int len=0;

    if (type==_DB_ENTRY_TYPE_DIRECTORY || type==_DB_ENTRY_TYPE_BACKUP || type==_DB_ENTRY_TYPE_BACKUPSUB) {

	len=snprintf(sqlquery, 512, "SELECT id,name,type,mode,nlink,unix_user,unix_group,mtim_sec,mtim_nsec,ctim_sec,ctim_nsec FROM dentry WHERE parent=%lli AND owner=%i", db_id, owner);

    } else if (type==_DB_ENTRY_TYPE_FILE) {

	len=snprintf(sqlquery, 512, "SELECT id,snapshot,mode,unix_user,unix_group,mtim_sec,mtim_nsec,ctim_sec,ctim_nsec FROM version WHERE fileid=%lli", db_id);

    }

    return sqlite_getdata_common(db_conn->sqlite, sqlquery, len, error);
}

unsigned char readdir_sqlite(void *data, unsigned char type, struct db_name_s *db_name, struct db_stat_s *db_stat)
{
    sqlite3_stmt *sql_stmt=(sqlite3_stmt *) data;
    unsigned char step=0;
    int result=0;

    result=sqlite3_step(sql_stmt);

    if (result==SQLITE_ROW) {

	if (type==_DB_ENTRY_TYPE_DIRECTORY || type==_DB_ENTRY_TYPE_BACKUP || type==_DB_ENTRY_TYPE_BACKUPSUB) {

	    db_stat->id=sqlite3_column_int64(sql_stmt, 0);

	    db_name->name=(char *) sqlite3_column_text(sql_stmt, 1);
	    db_name->len=strlen(db_name->name);

	    db_stat->type=sqlite3_column_int(sql_stmt, 2);
	    db_stat->mode=sqlite3_column_int(sql_stmt, 3);
	    db_stat->nlink=sqlite3_column_int(sql_stmt, 4);
	    db_stat->uid=(uid_t) sqlite3_column_int(sql_stmt, 5);
	    db_stat->gid=(gid_t) sqlite3_column_int(sql_stmt, 6);
	    db_stat->mtim=sqlite3_column_int(sql_stmt, 7);
	    db_stat->mtim_nsec=sqlite3_column_int(sql_stmt, 8);
	    db_stat->ctim=sqlite3_column_int(sql_stmt, 9);
	    db_stat->ctim_nsec=sqlite3_column_int(sql_stmt, 10);

	} else {

	    /* TODO: where to get the values from the VERSION table
	     - name -> snapshot
	     - stat -> ??? */

	    db_stat->id=sqlite3_column_int64(sql_stmt, 0);

	    /* name has to be a buffer, not allocating it here  */

	    snprintf(db_name->name, db_name->len, "%li", sqlite3_column_int64(sql_stmt, 1));

	    db_stat->mode=sqlite3_column_int(sql_stmt, 2);
	    db_stat->nlink=1;
	    db_stat->uid=(uid_t) sqlite3_column_int(sql_stmt, 3);
	    db_stat->gid=(gid_t) sqlite3_column_int(sql_stmt, 4);
	    db_stat->mtim=sqlite3_column_int(sql_stmt, 5);
	    db_stat->mtim_nsec=sqlite3_column_int(sql_stmt, 6);
	    db_stat->ctim=sqlite3_column_int(sql_stmt, 7);
	    db_stat->ctim_nsec=sqlite3_column_int(sql_stmt, 8);


	}

	step=1;

    }

    logoutput("readdir_sqlite: step %i name %.*s", step, strlen(db_name->name), db_name->name);

    return step;

}

void closedir_sqlite(void *data)
{
    sqlite3_stmt *sql_stmt=(sqlite3_stmt *) data;
    sqlite3_finalize(sql_stmt);

}

static void _create_record_cb(sqlite3_stmt *stmt, void *ptr)
{
    unsigned char *found=(unsigned char *) ptr;
    *found=1;
}

unsigned char create_dentry_sqlite(struct db_conn_s *db_conn, unsigned long long db_id_parent, unsigned int owner, struct db_name_s *db_name, unsigned char *created)
{
    unsigned int error=0;
    char sqlquery[512];
    int len=0;
    unsigned char found=0;

    logoutput("create_dentry_sqlite: parent %li name %.*s owner %i", db_id_parent, db_name->len, db_name->name, owner);

    len=snprintf(sqlquery, 512, "INSERT OR IGNORE INTO dentry (owner,name,parent) VALUES (%i,'%.*s',%lli)", owner, db_name->len, db_name->name,db_id_parent);

    if (sqlite_execute_simple_common(db_conn->sqlite, sqlquery, len, created, _create_record_cb, (void *) &found, &error)!=0) {

	logoutput("create_dentry_sqlite: error");

    }

    return found;

}
