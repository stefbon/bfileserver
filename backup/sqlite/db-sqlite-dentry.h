/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef FUSE_BACKUP_DB_SQLITE_FILE_H
#define FUSE_BACKUP_DB_SQLITE_FILE_H

// Prototypes

unsigned char get_dentry_sqlite(struct db_conn_s *db_conn, struct db_name_s *db_name, unsigned long long *db_id_parent, struct db_stat_s *db_stat, unsigned int *error);
unsigned char lookup_dentry_sqlite(struct db_conn_s *db_conn, unsigned long long db_id_parent, struct db_name_s *db_name, unsigned int owner, struct db_stat_s *db_stat);
unsigned char set_dentry_stat_sqlite(struct db_conn_s *db_conn, struct db_stat_s *db_stat);

void delete_dentry_record_sqlite(struct db_conn_s *db_conn, unsigned long long db_id);

void *opendir_sqlite(struct db_conn_s *db_conn, unsigned long long db_id, unsigned char type, unsigned int owner, unsigned int *error);
unsigned char readdir_sqlite(void *data, unsigned char type, struct db_name_s *db_name, struct db_stat_s *db_stat);
void closedir_sqlite(void *data);

unsigned char create_dentry_sqlite(struct db_conn_s *db_conn, unsigned long long db_id_parent, unsigned int owner, struct db_name_s *db_name, unsigned char *created);

#endif
