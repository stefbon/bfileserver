/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include "backup/db-common-base.h"
#include "backup/db-common.h"
#include "db-sqlite-common.h"

#define LOGGING
#include "logging.h"

static void _create_record_cb(sqlite3_stmt *stmt, void *ptr)
{
    unsigned char *found=(unsigned char *) ptr;
    *found=1;
}

unsigned char create_version_sqlite(struct db_conn_s *db_conn, unsigned long long fileid, unsigned long long snapshot, unsigned char *created)
{
    unsigned int error=0;
    char sqlquery[512];
    int len=0;
    unsigned char found=0;

    logoutput("create_version_sqlite: fileid %li snapshot %li", fileid, snapshot);

    len=snprintf(sqlquery, 512, "INSERT OR IGNORE INTO version (fileid,snapshot) VALUES (%lli,%lli)", fileid, snapshot);

    if (sqlite_execute_simple_common(db_conn->sqlite, sqlquery, len, created, _create_record_cb, (void *) &found, &error)!=0) {

	logoutput("create_version_sqlite: error");

    }

    return found;

}
