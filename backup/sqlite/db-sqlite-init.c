/*
  2010, 2011, 2012, 2013, 2014 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <time.h>

#include "options.h"
#include "backup/db-common-base.h"
#include "db-sqlite-init.h"
// #include "backup/db-common-base.h"
#include "backup/db-common.h"

#define lOGGING
#include "logging.h"

extern struct fs_options_struct fs_options;

static struct timespec busy_wait={0, 5000000};

static int custom_busy_handler(void *data, int times)
{
    logoutput("custom_busy_handler: wait 5000 nanoseconds");

    nanosleep(&busy_wait, NULL);

    if (times>3) {

	return 0;

    }

    return 1;

}

int init_connection_sqlite(struct db_conn_s *db_conn, unsigned int *error)
{
    int result=0;
    struct backup_subsystem_s *backup=db_conn->backup;
    struct ssh_session_s *session=(struct ssh_session_s *) ( ((char *) backup) - offsetof(struct ssh_session_s, subsystem.backup));
    struct ssh_user_s *user=&session->user;

    logoutput("init_connection_sqlite");

    unsigned int len=strlen("system.sqlite") + fs_options.socket.len + 64; /* enough bytes */
    char path[len];

    logoutput("init_connection_sqlite; (socket path %s)", fs_options.socket.path);

    if (snprintf(path, len, "%s/%i-system.sqlite", fs_options.socket.path, (unsigned int) user->pwd.pw_uid)>0) {
	struct stat st;
	unsigned int openflags=0;

	if (lstat(path, &st)==-1) {

	    logoutput("init_connection_sqlite: db %s not found: creating it", path);

	}

	openflags |= SQLITE_OPEN_READWRITE;
	openflags |= SQLITE_OPEN_CREATE;
	openflags |= SQLITE_OPEN_NOMUTEX; /* multithread */
	openflags |= SQLITE_OPEN_SHAREDCACHE;

	if (sqlite3_open_v2(path, &db_conn->sqlite, openflags, NULL)==SQLITE_OK) {

	    logoutput("init_connection_sqlite: connection to db %s", path);

	    sqlite3_busy_handler(db_conn->sqlite, custom_busy_handler, NULL);

	} else {

	    logoutput("init_connection_sqlite: error opening db %s", path);
	    result=-1;
	    *error=EIO; /* translate the return code of sqlite_open to a good error code ? */

	}

    }

    return result;

}

void close_connection_sqlite(struct db_conn_s *db_conn)
{

    sqlite3_close(db_conn->sqlite);
    db_conn->sqlite=NULL;

}

int init_db_sqlite()
{
    int result=0;

    logoutput("init_db_sqlite");

    result=sqlite3_config(SQLITE_CONFIG_MULTITHREAD);

    if (result==SQLITE_OK) {

	logoutput("init_db_sqlite: set config options MULTITHREAD");

    } else {

	if (result==SQLITE_ERROR) {

	    logoutput_error("init_db_sqlite: error setting config options MULTITHREAD");

	} else {

	    logoutput_error("init_db_sqlite: setting config options MULTITHREAD result in %i", result);

	}

    }

    logoutput("init_db_sqlite: finish");

    return sqlite3_initialize();

}

void close_db_sqlite()
{
    sqlite3_shutdown();
}
