/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <pwd.h>
#include <grp.h>

#include <linux/kdev_t.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "simple-hash.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "backuphandle.h"

extern void db_free_data(void *data);
static struct simple_hash_s backuphandle_group;

void insert_backuphandle_hash(struct backuphandle_s *handle)
{
    add_data_to_hash(&backuphandle_group, (void *) handle);
}

void remove_backuphandle_hash(struct backuphandle_s *handle)
{
    remove_data_from_hash(&backuphandle_group, (void *) handle);
}

int writelock_backuphandles(struct simple_lock_s *lock)
{
    init_wlock_hashtable(&backuphandle_group, lock);
    return lock_hashtable(lock);
}

int readlock_backuphandles(struct simple_lock_s *lock)
{
    init_rlock_hashtable(&backuphandle_group, lock);
    return lock_hashtable(lock);
}

int unlock_backuphandles(struct simple_lock_s *lock)
{
    return unlock_hashtable(lock);
}

unsigned int calculate_backup_hash(unsigned long long id)
{
    unsigned int hashvalue=id % backuphandle_group.len;
    return hashvalue;
}

static unsigned int backup_hashfunction(void *data)
{
    struct backuphandle_s *handle=(struct backuphandle_s *) data;
    return calculate_backup_hash(handle->id);
}

unsigned int get_backuphandle_hashvalue(struct backuphandle_s *handle)
{
    return backup_hashfunction((void *) handle);
}

static void close_backuphandle(struct backuphandle_s *handle)
{
    struct simple_lock_s lock;

    if (handle->data) {

	db_free_data(handle->data);
	handle->data=NULL;

    }

    if (writelock_backuphandles(&lock)==0) {

	remove_backuphandle_hash(handle);
	unlock_backuphandles(&lock);

    }

}

void free_backuphandle(struct backuphandle_s *handle)
{
    close_backuphandle(handle);
    free(handle);
}

struct backuphandle_s *create_backuphandle(struct ssh_session_s *session, unsigned long long id, unsigned int *error)
{
    struct backuphandle_s *handle=NULL;

    handle=malloc(sizeof(struct backuphandle_s));

    if (handle) {
	struct simple_lock_s lock;

	handle->session=session;
	handle->id=id;
	handle->data=NULL;
	handle->flags=0;
	handle->type=0; /* really required ? */

	handle->close=close_backuphandle;

	if (writelock_backuphandles(&lock)==0) {

	    insert_backuphandle_hash(handle);
	    unlock_backuphandles(&lock);

	}

    }

    return handle;

}

struct backuphandle_s *find_backuphandle(struct ssh_session_s *session, unsigned long long id, unsigned int *error)
{
    struct backuphandle_s *handle=NULL;
    unsigned int hashvalue=0;
    void *index=NULL;
    struct simple_lock_s lock;

    hashvalue=calculate_backup_hash(id);
    writelock_backuphandles(&lock);
    handle=(struct backuphandle_s *) get_next_hashed_value(&backuphandle_group, &index, hashvalue);

    while (handle) {

	if (handle->id==id) {

	    if (handle->session!=session) {

		*error=EPERM;
		handle=NULL;

	    }

	    break;

	}

	handle=(struct backuphandle_s *) get_next_hashed_value(&backuphandle_group, &index, hashvalue);

    }

    unlock_backuphandles(&lock);
    return handle;

}

struct backuphandle_s *get_next_backuphandle(void **index, unsigned int hashvalue)
{
    return (struct backuphandle_s *) get_next_hashed_value(&backuphandle_group, index, hashvalue);
}

int init_backuphandles(unsigned int *error)
{
    return initialize_group(&backuphandle_group, backup_hashfunction, 64, error);
}

void free_backuphandles()
{
    free_group(&backuphandle_group, NULL);
}

void write_backuphandle(struct backuphandle_s *handle, char *bytes)
{
    store_uint64(bytes, handle->id);
}

unsigned int read_backuphandle(char *bytes, unsigned int len, unsigned long long *id)
{

    if (len==BACKUP_HANDLE_SIZE) {

	*id=get_uint64(bytes);
	return BACKUP_HANDLE_SIZE;

    }

    return 0;

}
