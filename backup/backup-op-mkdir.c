/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "shared/sftp-attributes-write.h"

#include "shared/sftp-reply-attrs.h"
#include "shared/sftp-reply-status.h"

#include "db-common.h"

static void set_mkdir_attributes(struct sftp_attr_s *attr, struct stat *st)
{

    if (attr->valid[SFTP_ATTR_INDEX_SIZE]) {

	logoutput("set_mkdir_attributes: size %i", attr->size);
	st->st_size=attr->size;

    }

    if (attr->valid[SFTP_ATTR_INDEX_USERGROUP]) {
	uid_t uid=(attr->flags & SFTP_ATTR_FLAG_VALIDUSER) ? attr->uid : (uid_t) -1;
	gid_t gid=(attr->flags & SFTP_ATTR_FLAG_VALIDGROUP) ? attr->gid : (gid_t) -1;

	/* set the uid and gid
	    NOTE: using (uid_t) -1 and (gid_t) -1 in fchown will ignore this value
	    */

	st->st_uid=uid;
	st->st_gid=gid;

	logoutput("set_mkdir_attributes: uid %i gid %i", uid, gid);

    }

    if (attr->valid[SFTP_ATTR_INDEX_PERMISSIONS]) {

	st->st_mode = S_IFDIR | attr->permissions;
	logoutput("set_mkdir_attributes: mode %i", st->st_mode);

    }

    if (attr->valid[SFTP_ATTR_INDEX_ATIME] || attr->valid[SFTP_ATTR_INDEX_MTIME]) {

	if (attr->valid[SFTP_ATTR_INDEX_ATIME]) {

	    st->st_atim.tv_sec=attr->atime.sec;
	    st->st_atim.tv_nsec=attr->atime.nsec;

	}

	if (attr->valid[SFTP_ATTR_INDEX_MTIME]) {

	    st->st_mtim.tv_sec=attr->mtime.sec;
	    st->st_mtim.tv_nsec=attr->mtime.nsec;

	}

    }

}



/* SSH_FXP_MKDIR
    message has the form:
    - byte 				SSH_FXP_MKDIR
    - uint32				id
    - string				path
    - ATTRS				attrs
    */

void backup_op_mkdir(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    struct backup_subsystem_s *backup=&session->subsystem.backup;
    unsigned int status=SSH_FX_BAD_MESSAGE;
    unsigned int valid=SSH_FILEXFER_ATTR_SIZE | SSH_FILEXFER_ATTR_OWNERGROUP | SSH_FILEXFER_ATTR_PERMISSIONS | SSH_FILEXFER_ATTR_ACCESSTIME | SSH_FILEXFER_ATTR_MODIFYTIME | SSH_FILEXFER_ATTR_SUBSECOND_TIMES | SSH_FILEXFER_ATTR_CTIME;
    char path[header->len]; /* big enough */
    unsigned long long db_id=0;
    struct db_name_s db_name;
    unsigned char created=0;
    struct sftp_attr_s attr;
    struct stat st;
    int result=0;
    unsigned int pos=0;

    logoutput("backup_op_mkdir");

    if (header->len>=4) {
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	/* sftp packet size is at least:
	    - 4 + len ... path (len maybe zero) */

	if (header->len > len + 4) {

	    memcpy(path, &buffer[pos], len);
	    path[len]='\0';
	    pos+=len;

	} else {

	    logoutput("backup_op_mkdir: ! header len %i > len %i", header->len, len);
	    goto error;

	}

    }

    logoutput("backup_op_mkdir: path %s", path);

    if (strlen(path)==0 || (strlen(path)==1 && (strcmp(path, "/")==0 || strcmp(path, ".")==0)) || (strlen(path)==2 && strcmp(path, "/.")==0)) {

	db_id=0; /* root */
	logoutput("backup_op_mkdir: root, parent id 0");

    } else {

	db_id=get_db_id_path(backup, path, session->user.pwd.pw_uid, GET_PATH_MODE_PARENT, &db_name);
	logoutput("backup_op_mkdir: parent id %li name %.*s", db_id, db_name.len, db_name.name);

    }

    /* set defaults of the to be created directory */

    st.st_size=0;
    st.st_uid=session->user.pwd.pw_uid;
    st.st_gid=session->user.pwd.pw_gid;
    st.st_mode=(S_IFDIR | (S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)); /* for now */
    st.st_nlink=2;

    get_current_time(&st.st_mtim);
    get_current_time(&st.st_ctim);

    if (read_attributes_v06(backup, &buffer[pos], header->len - pos, &attr)>0) {

	set_mkdir_attributes(&attr, &st);

    } else {

	logoutput("backup_op_mkdir: error reading attributes");

    }

    result=db_create_dentry(backup, db_id, session->user.pwd.pw_uid, &db_name, &created);

    if (result==1 || result==0) {
	struct db_stat_s db_stat;
	unsigned int error=0;

	// db_stat.id=

	if (db_lookup_dentry(backup, db_id, &db_name, session->user.pwd.pw_uid, &db_stat)==1) {
	    struct backup_subsystem_s *backup=&session->subsystem.backup;
	    unsigned int size=0;

	    logoutput("backup_op_mkdir: new db_id %li", db_stat.id);

	    // copy_db_stat_stat(&db_stat, &st);

	    // size=write_attributes_len(backup, &attr, &st, valid);
	    // char buffer[size];

	    // size=write_attributes(backup, buffer, size, &attr, valid);

	    // if (reply_sftp_attrs(session, header->id, buffer, size)==-1) {

	    //	logoutput("backup_op_mkdir: error sending attr");

	    //}

	    reply_sftp_status_simple(session, header->id, SSH_FX_OK);

	    /* write backup to db with updated values */

	    copy_stat_db_stat(&db_stat, &st);
	    db_set_dentry_type(backup, &db_stat);

	    return;

	}

    }

    error:

    reply_sftp_status_simple(session, header->id, status);
}

