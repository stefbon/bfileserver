/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef FUSE_BACKUP_CREATESNAPSHOT_H
#define FUSE_BACKUP_CREATESNAPSHOT_H


// Prototypes

int createsubvolume(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned int *error);
int removesubvolume(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned int *error);

int createsnapshot(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned long long db_snapshot, unsigned int *error);
int removesnapshot(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned long long db_snapshot, unsigned int *error);


#endif



