/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <time.h>

#include "fuse-backup.h"

#include "entry-management.h"

#include "db-common-base.h"
#include "db-common.h"

#include "options.h"

#include "logging.h"

extern struct fs_options_struct fs_options;

static int _get_yearweek(char *buffer, unsigned int len, unsigned int *year, unsigned int *week, unsigned int *day, unsigned char *weekday)
{
    int result=0;
    struct tm version_tm;

    if (strptime(buffer, "%Y%m%d%H%M", &version_tm)) {
	unsigned int sunday=0;

	*year=version_tm.tm_year;
	*day=version_tm.tm_yday;
	*weekday=version_tm.tm_wday;

	/* previous sunday */

	sunday=version_tm.tm_yday - version_tm.tm_wday;

	if (sunday % 7 == 0 ) {

	    /* first january is on a sunday */

	    *week=sunday/7;

	} else {

	    *week=sunday/7 + 1;

	}

    } else {

	result=-1;

    }

    return result;

}

static int get_yearweek(unsigned long long db_date, unsigned int *year, unsigned int *week, unsigned int *day, unsigned char *weekday)
{
    char buffer[32];

    if (snprintf(buffer, 32, "%lli", db_date)>0) {

	return _get_yearweek(buffer, 32, year, week, day, weekday);

    }

    return -1;

}

/*
    function to remove versions of db_fileid when they are too old or there to many
*/

void apply_backup_policy(unsigned long long db_fileid, unsigned long long session_id)
{
    /*
	look at all the files
	test the versions of these files
	remove files if there are enough around in the same period

    */

    unsigned int current_week=0;
    unsigned int current_year=0;
    unsigned int current_day=0;
    unsigned char current_wday=0;

    if (get_yearweek(session_id, &current_year, &current_week, &current_day, &current_wday)==0) {
	void *data=NULL;

	data=db_get_versions_order(db_fileid);

	if (data) {
	    unsigned long long db_id_version=0;
	    unsigned long long db_date_version=0;
	    unsigned char db_type=0;
	    unsigned int version_week=0, previous_week=0;
	    unsigned int version_year=0, previous_year=0;
	    unsigned int version_day=0;
	    unsigned char version_wday=0;
	    unsigned count_2=0;
	    unsigned count_1=0;

	    while(db_read_version_order(data, &db_id_version, &db_date_version, &db_type)==1) {

		if (db_type==0 || db_type==_DB_VERSION_TYPE_CHANGED || db_type==_DB_VERSION_TYPE_FIRST) {

		    if (get_yearweek(db_date_version, &version_year, &version_week, &version_day, &version_wday)==0) {

			if (previous_week==0 && previous_year==0) {

			    previous_week=version_week;
			    previous_year=version_year;
			    count_2=1;
			    count_1=1;

			} else if (previous_week==version_week && previous_year==version_year) {

			    if (version_year < current_year - 1) {

				/* in any case long ago */

				count_2++;

				if (count_2>1) {

				    db_delete_version_record(db_id_version);

				}

			    } else if (version_year == current_year - 1) {

				if (current_week>1 || version_week<50) {

				    count_2++;

				    if (count_2>1) {

					db_delete_version_record(db_id_version);

				    }

				} else {
				    unsigned char leap=0;
				    unsigned int hlp_week=0;
				    unsigned int hlp_days=0;

				    /* calculate the difference in weeks when dealing with two different years */

			    	    leap = ((version_year % 4 == 0) && (version_year % 100 != 0)) || (version_year % 400 == 0);
				    hlp_days=(current_day + 364 + leap - version_day);
				    hlp_week=(hlp_days + version_wday) / 7;

				    if (hlp_week > 1) {

					count_2++;

					if (count_2>1) {

					    db_delete_version_record(db_id_version);

					}

				    } else if (hlp_week==1) {

					count_1++;

					if (count_1>1) {

					    db_delete_version_record(db_id_version);

					}

				    }

				}

			    } else if (version_year==current_year) {

				if (version_week + 1 < current_week) {

				    count_2++;

				    if (count_2>1) {

					db_delete_version_record(db_id_version);

				    }

				} else if (version_week + 1 == current_week) {

				    count_1++;

				    if (count_1>1) {

					db_delete_version_record(db_id_version);

				    }

				}

			    } else {

				logoutput("apply_backup_policy: found version %lli of %lli is in future....", db_date_version, db_fileid);

			    }

			} else {

			    /* version in other week/year than previous version: start counting with 1 */

			    previous_week=version_week;
			    previous_year=version_year;
			    count_2=1;
			    count_1=1;

			}

		    }

		}

	    }

	    db_free_data(data);

	}

    }

}

