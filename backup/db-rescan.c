/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/statvfs.h>

#include <pthread.h>
#include <dirent.h>

#include "fuse-backup.h"
#include "entry-management.h"
#include "directory-management.h"
#include "entry-utils.h"

#include "db-common.h"
#include "db-utils.h"
#include "db-rescan.h"

#include "backupjobs-base.h"
#include "backupjobs-util.h"
#include "systemdirectories.h"

#include "backup-stat.h"

#include "options.h"
#include "utils.h"

#include "logging.h"

extern struct fs_options_struct fs_options;
extern const char *rootpath;
static unsigned int max_namelen=255;

static void _read_stat_backup(char *path, struct stat *st)
{

    if (stat(path, st)==0) {

	st->st_mode&=(S_IRWXU | S_IRWXG | S_IRWXO);
	st->st_nlink=2;

    } else {

	st->st_mode=S_IRWXU | S_IRGRP | S_IXGRP;
	st->st_nlink=2;
	st->st_uid=0;
	st->st_gid=0;

	get_current_time(&st->st_mtim);
	st->st_ctim.tv_sec=st->st_mtim.tv_sec;
	st->st_ctim.tv_nsec=st->st_mtim.tv_nsec;

    }

}

static void _read_first_snapshot(unsigned long long snapshot, struct pathinfo_s *pathinfo, unsigned long long db_id_parent, unsigned long long db_id_backup)
{
    DIR *dp=NULL;

    logoutput("_read_first_snapshot: read %s", pathinfo->path);

    dp=opendir(pathinfo->path);

    if (dp) {
	struct dirent *de=readdir(dp);
	struct name_s xname={NULL, 0, 0};
	int len=pathinfo->len + 1 + max_namelen; /* create buffer large enough for every entry */
	char path[len+1]; /* including trailing zero */
	unsigned char filesfound=0;

	snprintf(path, len+1, "%s/", pathinfo->path);

	while (de) {

	    if (strcmp(de->d_name, ".")==0 || strcmp(de->d_name, "..")==0) {

		de=readdir(dp);
		continue;

	    }

	    if (de->d_type==DT_DIR) {
		unsigned char created=0;

		xname.name=de->d_name;
		xname.len=strlen(xname.name);

		if (db_create_map(db_id_parent, xname.name, &created)==1) {
		    unsigned long long db_id=0;
		    unsigned char db_type=0;

		    if (created==1) db_increase_nlink(db_id_parent); /* TODO: */

		    if (db_lookup_file(db_id_parent, xname.name, &db_id, &db_type)==1) {
			struct pathinfo_s sub_pathinfo={NULL, 0, 0};

			/* set type of this map as a subdirectory of a backup */

			db_set_file_type(db_id, _DB_ENTRY_TYPE_BACKUPSUB);

			/* do this subdirectory recursive */

			memcpy(path+pathinfo->len+1, xname.name, xname.len+1); /* append the name to the fixed part, including the trailing zero */

			sub_pathinfo.path=path;
			sub_pathinfo.len=pathinfo->len+1+xname.len;

			/* read this subdirectory */

			_read_first_snapshot(snapshot, &sub_pathinfo, db_id, db_id_backup);

		    }

		}

	    } else if (de->d_type==DT_REG) {

		filesfound=1;

		xname.name=de->d_name;
		xname.len=strlen(xname.name);

		if (db_create_file(db_id_parent, xname.name, NULL)==1) {
		    unsigned long long db_id=0;
		    unsigned char db_type=0;

		    if (db_lookup_file(db_id_parent, xname.name, &db_id, &db_type)==1) {

			db_create_version(db_id, snapshot, _DB_VERSION_TYPE_FIRST, _DB_VERSION_SUBTYPE_UNKNOWN, NULL, _DB_VERSION_FLAG_REPLACE);

		    } else {

			logoutput("read_first_snapshot: unable to create file %s", xname.name);

		    }

		} else {

		    logoutput("read_first_snapshot: unable to create file %s", xname.name);

		}

	    }

	    de=readdir(dp);

	}

	if (filesfound==1 && (db_id_parent != db_id_backup)) {

	    /* in a subdirectory and files found: dealing with a backup sub */

	    db_create_backupsub(db_id_backup, db_id_parent, NULL);

	}

	closedir(dp);

    }

}

static void compare_snapshot_db(unsigned long long snapshot, unsigned long long prev_snapshot, struct pathinfo_s *pathinfo, unsigned long long db_id_parent, unsigned long long db_id_backup)
{
    /* read records from db and check they are still present */

    unsigned int error=0;
    void *data=db_opendir(db_id_parent, 0, &error);

    if (data) {
	struct name_s xname={NULL, 0, 0};
	unsigned long long db_id=0;
	unsigned char db_type=0;
	int len=pathinfo->len + 1 + max_namelen; /* create buffer large enough for every entry */
	char path[len+1]; /* including trailing zero */
	unsigned char db_file_type=0;
	struct stat st;

	while (db_readdir(data, &xname, &db_id, &db_type)==1) {

	    db_file_type=(db_type & _DB_ENTRY_TYPE_MASK);

	    if (db_file_type==_DB_ENTRY_TYPE_FILE || db_file_type==_DB_ENTRY_TYPE_DIRECTORY || db_file_type==_DB_ENTRY_TYPE_BACKUP || db_file_type==_DB_ENTRY_TYPE_BACKUPSUB) {

		memcpy(path+pathinfo->len+1, xname.name, xname.len+1);

		error=0;

		/* test it's still present */

		if (stat(path, &st)==-1) {

		    if (error==ENOENT) {

			/*
			    mark it is deleted in the previous version
			    it's also possible that it has been deleted after that....(but before this snapshot)
			*/

			if ( ! (db_type & _DB_ENTRY_TYPE_DELETED) ) {

			    db_set_file_type(db_id, db_type & _DB_ENTRY_TYPE_DELETED);
			    db_create_version(db_id, prev_snapshot, _DB_VERSION_TYPE_LAST, _DB_VERSION_SUBTYPE_UNKNOWN, NULL, _DB_VERSION_FLAG_REPLACE);

			}

		    }

		} else {

		    if (db_type & _DB_ENTRY_TYPE_DELETED) {

			db_set_file_type(db_id, db_file_type); /* without the DELETED bit */
			if (snapshot>0) db_create_version(db_id, snapshot, _DB_VERSION_TYPE_FIRST, _DB_VERSION_SUBTYPE_UNKNOWN, NULL, _DB_VERSION_FLAG_REPLACE);

		    }

		}

		/* do directory recursive */

		if (db_type==_DB_ENTRY_TYPE_DIRECTORY || db_file_type==_DB_ENTRY_TYPE_BACKUP || db_file_type==_DB_ENTRY_TYPE_BACKUPSUB) {
		    struct pathinfo_s sub_pathinfo={NULL, 0, 0};

		    sub_pathinfo.path=path;
		    sub_pathinfo.len=pathinfo->len + 1 + xname.len;

		    compare_snapshot_db(snapshot, prev_snapshot, &sub_pathinfo, db_id, db_id_backup);

		}

	    }

	}

	db_free_data(data);

    }

}

static void _read_next_snapshot(unsigned long long snapshot, unsigned long long prev_snapshot, struct pathinfo_s *pathinfo, struct pathinfo_s *prev_pathinfo, unsigned long long db_id_parent, unsigned long long db_id_backup)
{
    DIR *dp=NULL;

    logoutput("_read_next_snapshot: read %s", pathinfo->path);

    /* check the directory in the snapshot */

    dp=opendir(pathinfo->path);

    if (dp) {
	struct dirent *de=readdir(dp);
	struct name_s xname={NULL, 0, 0};
	int len=pathinfo->len + 1 + max_namelen;
	int prev_len=prev_pathinfo->len + 1 + max_namelen;
	char path[len+1]; /* including trailing zero */
	char prev_path[prev_len+1]; /* including trailing zero */
	unsigned char filesfound=0;

	snprintf(path, len+1, "%s/", pathinfo->path);
	snprintf(prev_path, prev_len+1, "%s/", prev_pathinfo->path);

	while (de) {

	    if (strcmp(de->d_name, ".")==0 || strcmp(de->d_name, "..")==0) {

		de=readdir(dp);
		continue;

	    }

	    if (de->d_type==DT_DIR) {
		unsigned char created=0;

		xname.name=de->d_name;
		xname.len=strlen(xname.name);

		if (db_create_map(db_id_parent, xname.name, &created)==1) {
		    unsigned long long db_id=0;
		    unsigned char db_type=0;

		    if (db_lookup_file(db_id_parent, xname.name, &db_id, &db_type)==1) {
			struct pathinfo_s sub_pathinfo={NULL, 0, 0};
			struct pathinfo_s sub_prev_pathinfo={NULL, 0, 0};

			db_set_file_type(db_id, _DB_ENTRY_TYPE_BACKUPSUB);

			memcpy(path+pathinfo->len+1, xname.name, xname.len+1);
			memcpy(prev_path+prev_pathinfo->len+1, xname.name, xname.len+1);

			sub_pathinfo.path=path;
			sub_pathinfo.len=pathinfo->len + 1 + xname.len;

			sub_prev_pathinfo.path=prev_path;
			sub_prev_pathinfo.len=prev_pathinfo->len + 1 + xname.len;

			_read_next_snapshot(snapshot, prev_snapshot, &sub_pathinfo, &sub_prev_pathinfo, db_id, db_id_backup);

		    }

		}

	    } else if (de->d_type==DT_REG) {
		unsigned long long db_id=0;
		unsigned char db_type=0;

		filesfound=1;

		xname.name=de->d_name;
		xname.len=strlen(xname.name);

		if (db_lookup_file(db_id_parent, xname.name, &db_id, &db_type)==0) {

		    /* not found in db: new file */

		    if (db_create_file(db_id_parent, xname.name, NULL)==1) {

			if (db_lookup_file(db_id_parent, xname.name, &db_id, &db_type)==1) {

			    db_create_version(db_id, snapshot, _DB_VERSION_TYPE_FIRST, _DB_VERSION_SUBTYPE_UNKNOWN, NULL, _DB_VERSION_FLAG_REPLACE);

			} else {

			    logoutput("_read_next_snapshot: created file %s not found", xname.name);

			}

		    } else {

			logoutput("_read_next_snapshot: unable to create file %s", xname.name);

		    }

		} else { /* file found in db */
		    struct stat st;

		    /* this should always return 0... the readdir gives this entry */

		    memcpy(path+pathinfo->len+1, xname.name, xname.len+1);
		    memcpy(prev_path+prev_pathinfo->len+1, xname.name, xname.len+1);

		    if (stat(path, &st)==0) {
			struct stat prev_st;

			if (stat(prev_path, &prev_st)==0) {
			    unsigned int what=0;

			    /* file also present in previous snapshot : look at the differences */

			    if (compare_stat_backup(&st, &prev_st, _DB_VERSION_SUBTYPE_CHANGED_SIZE | _DB_VERSION_SUBTYPE_CHANGED_MODIFY, &what)==1) {

				logoutput("read_next_snapshot: create a changed version for %s in %lli", path, snapshot);

				db_create_version(db_id, snapshot, _DB_VERSION_TYPE_CHANGED, what, NULL, _DB_VERSION_FLAG_REPLACE);

			    }

			} else {

			    if (errno==ENOENT) {

				/* file found in db, but not in previous snapshot.... how come? */

				if (db_type & _DB_ENTRY_TYPE_DELETED) {

				    db_set_file_type(db_id, (db_type & _DB_ENTRY_TYPE_MASK));
				    db_create_version(db_id, snapshot, _DB_VERSION_TYPE_FIRST, _DB_VERSION_SUBTYPE_UNKNOWN, NULL, _DB_VERSION_FLAG_REPLACE);

				} else {

				    logoutput("_read_next_snapshot: internal error, file %s found in snapshot %lli but not in previous %lli and not deleted", path, snapshot, prev_snapshot);

				}

			    } else {

				logoutput("_read_next_snapshot: unknown error (%i:%s) when reading file %s", errno, strerror(errno), prev_path);

			    }

			}

		    } else {

			logoutput("_read_next_snapshot: unknown error (%i:%s) when reading file %s", errno, strerror(errno), path);

		    }

		}

	    } /* de->d_type==DT_DIR || de->d_type==DT_REG */

	    de=readdir(dp);

	}

	if (filesfound==1 && (db_id_parent != db_id_backup)) {

	    /* in a subdirectory and files found: dealing with a backup sub */

	    db_create_backupsub(db_id_backup, db_id_parent, NULL);

	}

	closedir(dp);

    }

}

static int read_target_backup(struct pathinfo_s *pathinfo, struct pathinfo_s *target, unsigned int *error)
{
    unsigned int lenlink=pathinfo->len + 1 + strlen("target");
    char linkpath[lenlink+1];

    /* read the target */

    if (snprintf(linkpath, lenlink+1, "%s/target", pathinfo->path)>0) {
	unsigned int lentarget=512;
	int lenread=0;

	while(lentarget<PATH_MAX) {

	    char targetpath[lentarget+1];

	    lenread=readlink(linkpath, targetpath, lentarget+1);

	    if (lenread==-1) {

		/* error reading the target */

		*error=errno;
		return -1;

	    } else if (lenread<lentarget+1) {

		/* a valid target read */

		targetpath[lenread]='\0';

		target->path=malloc(lenread+1);

		if (target->path) {

		    memcpy(target->path, targetpath, lenread+1); /* including the just added zero */
		    target->len=lenread;
		    target->flags=PATHINFO_FLAGS_ALLOCATED;
		    return 0;

		} else {

		    *error=ENOMEM;
		    return -1;

		}

	    } else {

		/* lenread>=lentarget+1 */

		lentarget+=512;

	    }

	}

    }

    if (*error==0) *error=EIO;
    return -1;

}

/*
    analyse the contents of the %backup%/%uid%/%nr%/ directory
    normally this should contain the backup and the snapshots
*/

void recreate_db_backup(unsigned int db_userid, unsigned long long backupnr, struct pathinfo_s *pathinfo)
{
    struct pathinfo_s target={NULL, 0, 0};
    unsigned int error=0;

    if (read_target_backup(pathinfo, &target, &error)==0) {
	unsigned long long db_id=0;

	/* make sure the path does exist in the db */

	db_id=db_create_path(&target, NULL, &error);

	if (db_id>0) {
	    struct directory_s directory;

	    /* read the different snapshots */

	    if (init_directory(&directory, &error)==0) {
		DIR *dp=NULL;
		struct entry_s *entry=NULL;

		dp=opendir(pathinfo->path);

		/* create a sorted list: use the directory/entry for that */

		if (dp) {
		    struct dirent *de=readdir(dp);
		    struct name_s xname={NULL, 0, 0};

		    while (de) {

			if (strcmp(de->d_name, ".")==0 || strcmp(de->d_name, "..")==0 || strcmp(de->d_name, "backup")==0 || strcmp(de->d_name, "target")==0) {

			    de=readdir(dp);
			    continue;

			}

			logoutput("recreate_db_backup: found snapshot %s", de->d_name);

			xname.name=de->d_name;
			xname.len=strlen(xname.name);
			calculate_nameindex(&xname);

			entry=create_entry(NULL, &xname);

			if (entry) entry=insert_entry_batch(&directory, entry, &error, 0);

			de=readdir(dp);

		    }

		    closedir(dp);

		}

		if (directory.first) {
		    unsigned long long snapshot=0;
		    unsigned long long prev_snapshot=0;
		    int len=pathinfo->len + 1 + max_namelen;
		    char path[len+1];
		    char prev_path[len+1];
		    struct pathinfo_s snapshot_pathinfo={NULL, 0, 0};
		    struct pathinfo_s prev_snapshot_pathinfo={NULL, 0, 0};

		    snprintf(path, len+1, "%s/", pathinfo->path);
		    snprintf(prev_path, len+1, "%s/", pathinfo->path);

		    snapshot_pathinfo.path=path;
		    prev_snapshot_pathinfo.path=prev_path;

		    /* walk through the entries: sorted */

		    entry=directory.first;

		    snapshot=atoll(entry->name.name);
		    memcpy(path+pathinfo->len+1, entry->name.name, entry->name.len+1);
		    snapshot_pathinfo.len=pathinfo->len + 1 + entry->name.len;

		    logoutput("recreate_db_backup: process the first snapshot %s", entry->name.name);

		    _read_first_snapshot(snapshot, &snapshot_pathinfo, db_id, db_id);

		    entry=entry->name_next;

		    while (entry) {

			/* copy current to previous */

			prev_snapshot=snapshot;
			memcpy(prev_path, snapshot_pathinfo.path, snapshot_pathinfo.len + 1);
			prev_snapshot_pathinfo.len=snapshot_pathinfo.len;

			/* create the new path */

			memcpy(path+pathinfo->len+1, entry->name.name, entry->name.len+1);
			snapshot_pathinfo.len=pathinfo->len+1+entry->name.len;
			snapshot=atoll(entry->name.name);

			logoutput("recreate_db_backup: process the next snapshot %s", entry->name.name);

			compare_snapshot_db(snapshot, prev_snapshot, &snapshot_pathinfo, db_id, db_id);
			_read_next_snapshot(snapshot, prev_snapshot, &snapshot_pathinfo, &prev_snapshot_pathinfo, db_id, db_id);

			entry=entry->name_next;

		    }

		    prev_snapshot=snapshot;
		    memcpy(prev_path, snapshot_pathinfo.path, snapshot_pathinfo.len + 1);
		    prev_snapshot_pathinfo.len=snapshot_pathinfo.len;

		    strcpy(path+pathinfo->len+1, "backup");
		    snapshot_pathinfo.len=pathinfo->len+7; /* 1 + strlen("backup") = 7 */

		    /*
			for the backup subvolume it's not required to compare every file, and look for new files
			it's only required to test for deleted files and directories
		    */

		    compare_snapshot_db(0, prev_snapshot, &snapshot_pathinfo, db_id, db_id);

		}

		/* free the allocated runtime memory for the directory */

		entry=directory.first;
		free_directory(&directory);

		while (entry) {

		    if (entry->name_next) {

			entry=entry->name_next;
			destroy_entry(entry->name_prev);
			entry->name_prev=NULL;

		    } else {

			destroy_entry(entry);
			entry=NULL;
			break;

		    }

		}

	    }

	    db_set_file_type(db_id, _DB_ENTRY_TYPE_BACKUP);

	    /* create the backup record using:
		- backupnr
		- db_id
		- userid
		- maxsize: determine it TODO
		- options: determine it TODO
	    */

	    db_create_backup(backupnr, db_id, 0, db_userid, _BACKUPJOB_OPTION_DEFAULT, NULL);

	} else {

	    logoutput("recreate_db_backup: unable to get db_id for target %s", target.path);

	}

    } else {

	logoutput("recreate_db_backup: unable to read the link %s/target", pathinfo->path);

    }

}


void recreate_db_user(uid_t uid, struct pathinfo_s *pathinfo)
{
    DIR *dp=NULL;
    unsigned int db_userid=0;
    unsigned long long backupmaxid=0;

    if (db_create_user(uid, NULL)==1) {

	if (db_lookup_user(uid, &db_userid, &backupmaxid)==1) {

	    logoutput("recreate_db_user: created db_userid %i for uid %i", db_userid, (int) uid);

	} else {

	    logoutput("recreate_db_user: error, db_userid for uid %i not found", (int) uid);
	    return;

	}

    } else {

	logoutput("recreate_db_user: failed to create db_userid for uid %i", (int) uid);
	return;

    }

    dp=opendir(pathinfo->path);

    if (dp) {
	struct dirent *de=readdir(dp);
	unsigned long long backupnr=0;

	while(de) {

	    /* every entry represents a backupjob */

	    if (strcmp(de->d_name, ".")==0 || strcmp(de->d_name, "..")==0) {

		de=readdir(dp);
		continue;

	    }

	    backupnr=atoll(de->d_name);

	    if (backupnr>0) {
		struct pathinfo_s backup_pathinfo={NULL, 0, 0};
		unsigned int len=pathinfo->len + 1 + strlen(de->d_name);
		char path[len+1]; /* plus including zero */

		logoutput("recreate_db_user: found backpnr %lli", backupnr);

		if (backupnr>backupmaxid) backupmaxid=backupnr;

		if (snprintf(path, len+1, "%s/%s", pathinfo->path, de->d_name)>0) {

		    backup_pathinfo.path=path;
		    backup_pathinfo.len=len;

		    recreate_db_backup(db_userid, backupnr, &backup_pathinfo);

		}

	    }

	    de=readdir(dp);

	}

	closedir(dp);
	dp=NULL;

    }

    if (backupmaxid>0) {

	db_set_backupmaxid(db_userid, backupmaxid);

    }

}



/*
    fill the database
    this is done when the database is dropped

    start at the backup, compare with every snapshot back
    - first compare the snapshot with the current state
    any difference is stored and made current
    - then compare the current state with the snapshot
    this is for files which are "last" in the snapshot: deleted files

    TODO:
    - what is not used yet

*/

void recreate_db()
{
    DIR *dp=NULL;

    /*
	take every directory under backup...
	every entry represents the uid of the user using this service
	every entry in this "user" directory represents a backup
	TODO: where to get the target from...
    */

    dp=opendir(fs_options.backup.path);

    if (dp) {
	struct dirent *de=readdir(dp);
	uid_t uid=0;

	while (de) {

	    if (strcmp(de->d_name, ".")==0 || strcmp(de->d_name, "..")==0) {

		de=readdir(dp);
		continue;

	    }

	    uid=(uid_t) atoi(de->d_name);

	    if (uid>=0) {
		unsigned int len=fs_options.backup.len + 1 + strlen(de->d_name);
		char path[len+1];

		if (snprintf(path, len+1, "%s/%s", fs_options.backup.path, de->d_name)>0) {
		    struct pathinfo_s pathinfo={NULL, 0, 0};

		    pathinfo.path=path;
		    pathinfo.len=len;

		    logoutput("recreate_db: found user %s", de->d_name);

		    recreate_db_user(uid, &pathinfo);

		}

	    }

	    de=readdir(dp);

	}

	closedir(dp);

    }

}
