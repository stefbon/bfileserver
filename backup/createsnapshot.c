/*
 
  2010, 2011, 2012, 2013, 2014 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>

#include <sys/stat.h>
#include <sys/param.h>
#include <sys/fsuid.h>
#include <sys/types.h>
#include <dirent.h>
#include <pthread.h>
#include <grp.h>
#include <pwd.h>

#include "fuse-backup.h"
#include "entry-management.h"

#include "options.h"
#include "utils.h"
#include "workerthreads.h"

#include "db-common.h"

#include "beventloop-utils.h"
#include "workspaces.h"

#include "backupjobs-base.h"
#include "backupjobs-util.h"
#include "backupjobs.h"

#include "logging.h"

extern struct fs_options_struct fs_options;
extern struct workerthreads_queue_struct workerthreads_queue;

#define _CREATEBACKUP_SCRIPT		"createbackup.sh"
#define _CREATESNAPSHOT_SCRIPT		"createsnapshot.sh"
#define _REMOVESNAPSHOT_SCRIPT		"removesnapshot.sh"
#define _BUFFERSIZE			4096

int createsubvolume(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned int *error)
{
    unsigned int len=fs_options.createsubvolume.len + strlen("--backup=") + backup->len + strlen("--uid=") + strlen("--db_id=") + 64;
    char path[len];
    int result=0;

    if (snprintf(path, len, "%s --backup=\"%s\" --uid=%i --db_id=%lli", fs_options.createsubvolume.path, backup->path, (int) uid, db_id)>0) {
        FILE *fp=NULL;

	logoutput("createsubvolume: run %s", path);

	fp=popen(path, "r");

	if (fp) {
	    char line[_BUFFERSIZE];
	    char *sep=NULL;

	    while(fgets(line, _BUFFERSIZE, fp)) {

		sep=strchr(line, '\n');
		if (sep) *sep='\0';

		if (strncmp(line, "E:", 2)==0) {

		    /* dealing with an error */

		    logoutput("createsubvolume: got error %s", line+2);

		} else {

		    logoutput("createsubvolume: unknown line %s", line);

		}

	    }

	    pclose(fp);

	} else {

	    *error=errno;
	    logoutput("createsubvolume: error %i:%s when trying to run command %s", errno, strerror(errno), path);
	    result=-1;

	}

    } else {

	*error=errno;
	logoutput("createsubvolume: error %i:%s when trying to run command", errno, strerror(errno));
	result=-1;

    }

    return result;

}

int removesubvolume(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned int *error)
{
    unsigned int len=fs_options.removesubvolume.len + strlen("--backup=") + backup->len + strlen("--uid=") + strlen("--db_id=") + 64;
    char path[len];
    int result=0;

    if (snprintf(path, len, "%s --backup=\"%s\" --uid=%i --db_id=%lli", fs_options.removesubvolume.path, backup->path, (int) uid, db_id)>0) {
        FILE *fp=NULL;

	logoutput("removesubvolume: run %s", path);

	fp=popen(path, "r");

	if (fp) {
	    char line[_BUFFERSIZE];
	    char *sep=NULL;

	    while(fgets(line, _BUFFERSIZE, fp)) {

		sep=strchr(line, '\n');
		if (sep) *sep='\0';

		if (strncmp(line, "E:", 2)==0) {

		    /* dealing with an error */

		    logoutput("removesubvolume: got error %s", line+2);

		} else {

		    logoutput("removesubvolume: unknown line %s", line);

		}

	    }

	    pclose(fp);

	} else {

	    *error=errno;
	    logoutput("removesubvolume: error %i:%s when trying to run command %s", errno, strerror(errno), path);
	    result=-1;

	}

    } else {

	*error=errno;
	logoutput("removesubvolume: error %i:%s when trying to run command", errno, strerror(errno));
	result=-1;

    }

    return result;

}

int createsnapshot(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned long long db_snapshot, unsigned int *error)
{
    unsigned int len=fs_options.createsnapshot.len + strlen("--backup=") + backup->len + strlen("--uid=") + strlen("--db_id=") + strlen("--snapshot=") + 64;
    char path[len];
    int result=0;

    if (snprintf(path, len, "%s --backup=\"%s\" --uid=%i --db_id=%lli --snapshot=%lli", fs_options.createsnapshot.path, backup->path, (int) uid, db_id, db_snapshot)>0) {
        FILE *fp=NULL;

	logoutput("createsnapshot: run %s", path);

	fp=popen(path, "r");

	if (fp) {
	    char line[_BUFFERSIZE];
	    char *sep=NULL;

	    while(fgets(line, _BUFFERSIZE, fp)) {

		sep=strchr(line, '\n');
		if (sep) *sep='\0';

		if (strncmp(line, "E:", 2)==0) {

		    /* dealing with an error */

		    logoutput("createsnapshot: got error %s", line+2);

		} else {

		    logoutput("createsnapshot: unknown line %s", line);

		}

	    }

	    pclose(fp);

	} else {

	    *error=errno;
	    logoutput("createsnapshot: error %i:%s when trying to run command %s", errno, strerror(errno), path);
	    result=-1;

	}

    } else {

	*error=errno;
	logoutput("createsnapshot: error %i:%s when trying to run command", errno, strerror(errno));
	result=-1;

    }

    return result;

}

int removesnapshot(struct pathinfo_s *backup, uid_t uid, unsigned long long db_id, unsigned long long db_snapshot, unsigned int *error)
{
    unsigned int len=fs_options.removesnapshot.len + strlen("--backup=") + backup->len + strlen("--uid=") + strlen("--db_id=") + strlen("--snapshot=") + 64;;
    char path[len];
    int result=0;

    if (snprintf(path, len, "%s --backup=\"%s\" --uid=%i --db_id=%lli --snapshot=%lli", fs_options.removesnapshot.path, backup->path, (int) uid, db_id, db_snapshot)>0) {
        FILE *fp=NULL;

	logoutput("removesnapshot: run %s", path);

	fp=popen(path, "r");

	if (fp) {
	    char line[_BUFFERSIZE];
	    char *sep=NULL;

	    while(fgets(line, _BUFFERSIZE, fp)) {

		sep=strchr(line, '\n');
		if (sep) *sep='\0';

		if (strncmp(line, "E:", 2)==0) {

		    /* dealing with an error */

		    logoutput("removesnapshot: got error %s", line+2);

		} else {

		    logoutput("removesnapshot: unknown line %s", line);

		}

	    }

	    pclose(fp);

	} else {

	    *error=errno;
	    logoutput("removesnapshot: error %i:%s when trying to run command %s", errno, strerror(errno), path);
	    result=-1;

	}

    } else {

	*error=errno;
	logoutput("removesnapshot: error %i:%s when trying to run command", errno, strerror(errno));
	result=-1;

    }

    return result;

}

