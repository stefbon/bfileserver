/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef BFILESERVER_BACKUP_DB_COMMON_H
#define BFILESERVER_BACKUP_DB_COMMON_H

#include "db-common-base.h"

struct db_name_s {
    char				*name;
    unsigned int			len;
};

struct db_stat_s {
    unsigned long long			id;
    unsigned int			type;
    unsigned int			size;
    unsigned int			mode;
    unsigned int			nlink;
    uid_t				uid;
    gid_t				gid;
    unsigned long			mtim;
    unsigned long			mtim_nsec;
    unsigned long			ctim;
    unsigned long			ctim_nsec;
};

int init_db();
void close_db();

int check_database_backend(unsigned char type);
void close_all_db_conn(struct backup_subsystem_s *backup);

int db_check(struct backup_subsystem_s *backup, unsigned int *error);
void db_free_data(void *data);

unsigned char db_get_dentry(struct backup_subsystem_s *backup, struct db_name_s *db_name, unsigned long long *db_id_parent, struct db_stat_s *db_stat, unsigned int *error);
unsigned char db_lookup_dentry(struct backup_subsystem_s *backup, unsigned long long parent_db_id, struct db_name_s *db_name, unsigned int owner, struct db_stat_s *db_stat);
unsigned char db_set_dentry_type(struct backup_subsystem_s *backup, struct db_stat_s *db_stat);

// void db_delete_dentry_record(unsigned long long db_id);

void *db_opendir(struct backup_subsystem_s *backup, unsigned long long db_id, unsigned char type, unsigned int owner, unsigned int *error);
unsigned char db_readdir(void *data, unsigned char type, struct db_name_s *name, struct db_stat_s *db_stat);
void db_closedir(void *data);

unsigned char db_create_dentry(struct backup_subsystem_s *backup, unsigned long long parent_db_id, unsigned int owner, struct db_name_s *db_name, unsigned char *created);
unsigned char db_create_version(struct backup_subsystem_s *backup, unsigned long long fileid, unsigned long long snapshot, unsigned char *created);

#define GET_PATH_MODE_FULL				1
#define GET_PATH_MODE_PARENT				2

unsigned long long get_db_id_path(struct backup_subsystem_s *backup, char *path, unsigned int owner, unsigned int mode, struct db_name_s *db_name);

void copy_db_stat_stat(struct db_stat_s *db_stat, struct stat *st);
void copy_stat_db_stat(struct db_stat_s *db_stat, struct stat *st);

#endif
