/*
  2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef BFILESERVER_BACKUPHANDLE_H
#define BFILESERVER_BACKUPHANDLE_H

#define BACKUP_HANDLE_SIZE			8

#define BACKUPHANDLE_TYPE_DIR			1
#define BACKUPHANDLE_TYPE_FILE			2
#define BACKUPHANDLE_TYPE_VERSION		3

#define BACKUPHANDLE_FLAG_EOF			1

struct backuphandle_s {
    struct ssh_session_s 	*session;
    unsigned int		flags;
    unsigned long long		id;
    unsigned char		type;
    void			*data;
    void			(* close)(struct backuphandle_s *handle);
};

// Prototypes

void write_backuphandle(struct backuphandle_s *handle, char *bytes);
unsigned int read_backuphandle(char *bytes, unsigned int len, unsigned long long *id);

int writelock_backuphandles(struct simple_lock_s *lock);
int readlock_backuphandles(struct simple_lock_s *lock);
int unlock_backuphandles(struct simple_lock_s *lock);

struct backuphandle_s *create_backuphandle(struct ssh_session_s *session, unsigned long long id, unsigned int *error);
void remove_backuphandle_hash(struct backuphandle_s *handle);
unsigned int calculate_backup_hash(unsigned long long id);

struct backuphandle_s *find_backuphandle(struct ssh_session_s *session, unsigned long long id, unsigned int *error);
struct backuphandle_s *get_next_backuphandle(void **index, unsigned int hashvalue);
void free_backuphandle(struct backuphandle_s *handle);

int init_backuphandles(unsigned int *error);
void free_backuphandles();

#endif
