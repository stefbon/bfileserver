/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef FUSE_BACKUP_DB_COMMON_BASE_H
#define FUSE_BACKUP_DB_COMMON_BASE_H

#define FUSE_BACKUP_DB_BACKEND_MYSQL				1
#define FUSE_BACKUP_DB_BACKEND_SQLITE				2

#define FUSE_BACKUP_DB_TABLE_FILE				1
#define FUSE_BACKUP_DB_TABLE_STAT				2
#define FUSE_BACKUP_DB_TABLE_VERSION				4
#define FUSE_BACKUP_DB_TABLE_MIME				8
#define FUSE_BACKUP_DB_TABLE_BACKUP				16
#define FUSE_BACKUP_DB_TABLE_BACKUPSUB				32
#define FUSE_BACKUP_DB_TABLE_BACKUPMEMBER			64
#define FUSE_BACKUP_DB_TABLE_RULE				128
#define FUSE_BACKUP_DB_TABLE_SESSION				256
#define FUSE_BACKUP_DB_TABLE_BACKUPUSER				512
#define FUSE_BACKUP_DB_TABLE_CONFIG				1024

/*
    a file record can have different functions. It can be a:
    a. directory
    b. backup
    c. subdirectory of backup
    d. file
    e. 
*/

#define _DB_ENTRY_TYPE_DIRECTORY				0 /* default */
#define _DB_ENTRY_TYPE_BACKUP					1
#define _DB_ENTRY_TYPE_BACKUPSUB				2
#define _DB_ENTRY_TYPE_FILE					3
#define _DB_ENTRY_TYPE_VERSION					4
#define _DB_ENTRY_TYPE_MIME					5

#define _DB_ENTRY_TYPE_MASK					31
#define _DB_ENTRY_TYPE_DELETED					32
#define _DB_ENTRY_TYPE_TOBEDELETED				64

#define _DB_VERSION_TYPE_FIRST					1
#define _DB_VERSION_TYPE_CHANGED				2
#define _DB_VERSION_TYPE_LAST					4

#define _DB_VERSION_SUBTYPE_UNKNOWN				0

#define _DB_VERSION_SUBTYPE_FIRST_CREATED			1
#define _DB_VERSION_SUBTYPE_FIRST_CHANGEDOPTIONS		2
#define _DB_VERSION_SUBTYPE_FIRST_CHANGEDRULES			4
#define _DB_VERSION_SUBTYPE_FIRST_MOVETO			8
#define _DB_VERSION_SUBTYPE_FIRST_MOVETO_UNKNOWN		16
#define _DB_VERSION_SUBTYPE_FIRST_MODIFIED			32

#define _DB_VERSION_SUBTYPE_CHANGED_UNKNOWN			1
#define _DB_VERSION_SUBTYPE_CHANGED_SIZE			2
#define _DB_VERSION_SUBTYPE_CHANGED_MODIFY			4
#define _DB_VERSION_SUBTYPE_CHANGED_USER			8
#define _DB_VERSION_SUBTYPE_CHANGED_GROUP			16

#define _DB_VERSION_SUBTYPE_LAST_REMOVED			1
#define _DB_VERSION_SUBTYPE_LAST_CHANGEDOPTIONS			2
#define _DB_VERSION_SUBTYPE_LAST_CHANGEDRULES			4
#define _DB_VERSION_SUBTYPE_LAST_MOVEFROM			8
#define _DB_VERSION_SUBTYPE_LAST_MOVEFROM_UNKNOWN		16
#define _DB_VERSION_SUBTYPE_LAST_MODIFIED			32

#define _DB_VERSION_FLAG_CREATE					1
#define _DB_VERSION_FLAG_REPLACE				2
#define _DB_VERSION_FLAG_UPDATE					4

#define _DB_USER_TYPE_OWNER					0
#define _DB_USER_TYPE_GUEST					1

#define _DB_LOG_TYPE_CREATE					1
#define _DB_LOG_TYPE_DELETE					2
#define _DB_LOG_TYPE_CHANGE					3

#ifdef HAVE_SQLITE3
#include <sqlite3.h>
#endif

#include "simple-list.h"
#include "clientsession.h"

struct db_conn_s {
#ifdef HAVE_SQLITE3
    sqlite3				*sqlite;
#endif
    unsigned char 			type;
    struct list_element_s		list;
    struct simple_lock_s		lock;
    struct backup_subsystem_s 		*backup;
};

#endif
