/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"

#include "backup-common.h"
#include "filehandle.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "shared/sftp-handle.h"
#include "shared/sftp-reply.h"
#include "shared/sftp-attributes-read.h"
#include "shared/sftp-path.h"
#include "shared/sftp-common-extended.h"

#include "db-common.h"


void backup_op_gettype(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);
//void backup_op_opendir(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);
//void backup_op_readdir(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);
//void backup_op_closedir(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);

static struct sftp_extension_s extensions[] = {
    { .len		= 27,
      .name		= "gettype@backup.bononline.nl",
      .cb		= backup_op_gettype,
    },
    { .len		= 25,
      .name		= "mapextension@bononline.nl",
      .cb		= sftp_op_mapextension,
    },
//    { .len		= 27,
//      .name		= "readdir@backup.bononline.nl",
//      .cb		= backup_op_readdir,
//    },
//    { .len		= 27,
//      .name		= "closedir@backup.bononline.nl",
//      .cb		= backup_op_closedir,
//    },
    { .len		= 0,
      .name		= NULL,
      .cb		= NULL,
    },
};

struct sftp_extension_s *get_next_backup_extension(struct sftp_extension_s *extension)
{

    if (extension==NULL) return &extensions[0];

    if ((char *) extension >= (char *) &extensions[0]) {
	unsigned int count = (((char *) extension - (char *) &extensions[0]) / sizeof (struct sftp_extension_s));

	if (count <= sizeof(extensions) / sizeof (struct sftp_extension_s)) {

	    extension=&extensions[count+1];

	    if (extension->len>0) return extension;

	}

    }

    return NULL;

}


static struct sftp_extension_s *get_backup_extension(char *name, unsigned int len)
{
    unsigned int count=0;
    struct sftp_extension_s *extension=&extensions[count];

    while (extension->len>0) {

	if (extension->len==len && strncmp(name, extension->name, len)==0) goto found;
	count++;
	extension=&extensions[count];

    }

    return NULL;

    found:

    return extension;

}

/* SSH_FXP_EXTENDED
    message has the form:
    - byte 				SSH_FXP_OPEN
    - uint32				id
    - string				name extended request
    - specfic data
    */

void backup_op_extended(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)

{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("backup_op_extended (%i)", (int) gettid());

    /* message should at least have 4 bytes for the path string, and 4 for the flags
	note an empty path is possible */

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	/* sftp packet size is at least:
	    - 4 + len ... name  */

	if (header->len >= len + 4) {
	    struct ssh_user_s *user=&session->user;
	    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
	    struct sftp_extension_s *extension=NULL;

	    extension=get_backup_extension(&buffer[pos], len);
	    pos+=len;

	    if (extension) {

		header->len -= pos;
		(* extension->cb)(session, header, &buffer[pos]);
		return;

	    }

	    status=SSH_FX_OP_UNSUPPORTED;

	}

    }

    error:

    logoutput("sftp_op_extended: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_EXTENDED - gettype@backup.bononline.nl
    message has the form:
    - string				path

    gives back the type of the dentry:
    - directory				- normal path component to a backup (not part of backup self)
    - backup-directory			- directory which is backup directory
    - backup-subdirectory		- directory which is part of backup directory (and therefore a subdirectory)
    - backup-file			- file which self is part of backup (and this is being "backupped")
    - backup-version			- version of file part of backup
    - backup-mime			- mimetype of files which are "backupped"
    - unknown
    */

void backup_op_gettype(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("backup_op_extended - gettype@backup.bononline.nl (%i)", (int) gettid());

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (header->len==4 + len) {
	    struct backup_subsystem_s *backup=&session->subsystem.backup;
	    unsigned int lenpath=get_fullpath_len(&session->user, len, &buffer[pos]);
	    char path[lenpath];
	    unsigned long long db_id=0;
	    char *type=NULL;
	    struct db_name_s db_name;

	    get_fullpath(&session->user, len, &buffer[pos], path);

	    if (strlen(path)==0 || (strlen(path)==1 && (strcmp(path, "/")==0 || strcmp(path, ".")==0)) || (strlen(path)==2 && strcmp(path, "/.")==0)) {

		db_id=0; /* root */
		logoutput("backup_op_gettype: root");

	    } else {

		db_id=get_db_id_path(backup, path, session->user.pwd.pw_uid, GET_PATH_MODE_FULL, &db_name);
		logoutput("backup_op_gettype: id %li", db_id);

		if (db_id==0) {

		    status=SSH_FX_NO_SUCH_FILE;
		    goto error;

		}

	    }

	    unsigned long long db_id_parent=0;
	    struct db_stat_s db_stat;
	    unsigned int error=0;

	    db_stat.id=db_id;

	    if (db_get_dentry(backup, &db_name, &db_id_parent, &db_stat, &error)==1) {

		if (db_stat.type == _DB_ENTRY_TYPE_DIRECTORY) {

		    type="directory";

		} else if (db_stat.type == _DB_ENTRY_TYPE_BACKUP) {

		    type="backup-directory";

		} else if (db_stat.type == _DB_ENTRY_TYPE_BACKUPSUB) {

		    type="backup-subdirectory";

		} else if (db_stat.type == _DB_ENTRY_TYPE_FILE) {

		    type="backup-file";

		} else if (db_stat.type == _DB_ENTRY_TYPE_VERSION) {

		    type="backup-version";

		} else if (db_stat.type == _DB_ENTRY_TYPE_MIME) {

		    type="backup-mime";

		} else {

		    type="unknown";

		}

	    }

	    if (type) {
		unsigned int len=strlen(type);
		char data[4 + len];

		store_uint32(data, len);
		memcpy(&data[4], type, len);

		reply_sftp_extension(session, header->id, data, 4 + len);
		return;

	    }

	}

    }

    error:

    logoutput("backup_op_gettype: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

/* SSH_FXP_EXTENDED - opendir@backup.bononline.nl
    message has the form:

    - string				path

    reply:

    - open directory of versions of "file" (== dentry which represents a file but is a directory)

    */

void backup_op_opendir_keep(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("backup_op_extended - opendir@backup.bononline.nl (%i)", (int) gettid());

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (header->len==4 + len) {
	    struct backup_subsystem_s *backup=&session->subsystem.backup;
	    unsigned int lenpath=get_fullpath_len(&session->user, len, &buffer[pos]);
	    char path[lenpath];
	    unsigned long long db_id=0;
	    char *type=NULL;
	    struct db_name_s db_name;

	    get_fullpath(&session->user, len, &buffer[pos], path);

	    if (strlen(path)==0 || (strlen(path)==1 && (strcmp(path, "/")==0 || strcmp(path, ".")==0)) || (strlen(path)==2 && strcmp(path, "/.")==0)) {

		db_id=0; /* root */
		logoutput("backup_op_gettype: root");

	    } else {

		db_id=get_db_id_path(backup, path, session->user.pwd.pw_uid, GET_PATH_MODE_FULL, &db_name);
		logoutput("backup_op_gettype: id %li", db_id);

		if (db_id==0) {

		    status=SSH_FX_NO_SUCH_FILE;
		    goto out;

		}

	    }

	}

    }

    out:

    logoutput("sftp_op_statvfs: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

