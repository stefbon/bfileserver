/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "filehandle.h"
#include "backuphandle.h"

#include "shared/sftp-reply-handle.h"
#include "shared/sftp-reply-data.h"
#include "shared/sftp-reply-status.h"
#include "shared/sftp-op-setstat.h"
#include "shared/sftp-attributes-read.h"

#include "backuphandle.h"
#include "db-common.h"

#define _SFTP_OPENACCESS_READ				( ACE4_READ_DATA | ACE4_READ_ATTRIBUTES )
#define _SFTP_OPENACCESS_WRITE				( ACE4_WRITE_DATA | ACE4_WRITE_ATTRIBUTES )
#define _SFTP_OPENACCESS_READWRITE			( ACE4_READ_DATA | ACE4_READ_ATTRIBUTES | ACE4_WRITE_DATA | ACE4_WRITE_ATTRIBUTES )
#define _SFTP_OPENACCESS_APPEND				( ACE4_APPEND_DATA )

/* translate the access and flags sftp parameters into posix
    do also some sane checking (write access is required for append etc) */

static int translate_sftp2posix(unsigned int access, unsigned int flags, unsigned int *posix, unsigned int *error)
{
    int result=0;

    if ((access & _SFTP_OPENACCESS_READ) && (access & _SFTP_OPENACCESS_WRITE)) {

	*posix|=O_RDWR;

	if (access & _SFTP_OPENACCESS_APPEND) {

	    if (flags & (SSH_FXF_APPEND_DATA | SSH_FXF_APPEND_DATA_ATOMIC)) {

		*posix|=O_APPEND;

	    } else {

		*error=EINVAL;
		goto error;

	    }

	}

    } else if (access & _SFTP_OPENACCESS_WRITE) {

	*posix|=O_WRONLY;

	if (access & _SFTP_OPENACCESS_APPEND) {

	    if (flags & (SSH_FXF_APPEND_DATA | SSH_FXF_APPEND_DATA_ATOMIC)) {

		*posix|=O_APPEND;

	    } else {

		*error=EINVAL;
		goto error;

	    }

	}

    } else if (access & _SFTP_OPENACCESS_READ) {
	unsigned int openflags=flags & SSH_FXF_ACCESS_DISPOSITION;

	*posix|=O_RDONLY;

	if (access & _SFTP_OPENACCESS_APPEND) {

	    *error=EINVAL;
	    goto error;

	}

	if (openflags != SSH_FXF_OPEN_EXISTING) {

	    *error=EINVAL;
	    goto error;

	}

    } else {

	*error=EINVAL;
	goto error;

    }

    if (flags & SSH_FXF_ACCESS_DISPOSITION) {

	if (flags & SSH_FXF_CREATE_NEW) {

	    *posix |= (O_EXCL | O_CREAT);

	} else if (flags & SSH_FXF_CREATE_TRUNCATE) {

	    *posix |= (O_CREAT | O_TRUNC);

	} else if (flags & SSH_FXF_OPEN_EXISTING) {

	    /* no additional posix flags ... */

	} else if (flags & SSH_FXF_OPEN_OR_CREATE) {

	    *posix |= O_CREAT;

	} else if (flags & SSH_FXF_TRUNCATE_EXISTING) {

	    *posix |= O_TRUNC;

	}

    } else {

	*error=EINVAL;

    }

    out:

    logoutput("translate_sftp2posix: access %i flags %i posix %i", access, flags, *posix);

    return 0;

    error:

    logoutput("translate_sftp2posix: received incompatible/incomplete open access and flags");
    return -1;

}

static unsigned int translate_open_error(unsigned int error)
{
    unsigned int status=SSH_FX_FAILURE;

    if (error==ENOENT) {

	status=SSH_FX_NO_SUCH_FILE;

    } else if (error==ENOTDIR) {

	status=SSH_FX_NO_SUCH_PATH;

    } else if (error==EACCES) {

	status=SSH_FX_PERMISSION_DENIED;

    } else if (error==EEXIST) {

	status=SSH_FX_FILE_ALREADY_EXISTS;

    }

    return status;

}

/* SSH_FXP_OPEN
    message has the form:
    - byte 				SSH_FXP_OPEN
    - uint32				id
    - string				path
    - uint32				access
    - uint32				flags
    - ATTRS				attrs
    */

void backup_op_open(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)

{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    error:

    logoutput("backup_op_error: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_READ
    message has the form:
    - byte 				SSH_FXP_READ
    - uint32				id
    - string				handle
    - uint64				offset
    - uint32				length
    */

void backup_op_read(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("backup_op_read: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

/* SSH_FXP_WRITE
    message has the form:
    - byte 				SSH_FXP_WRITE
    - uint32				id
    - string				handle
    - uint64				offset
    - string				data
    */

void backup_op_write(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    error:

    logoutput("backup_op_write: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

/* SSH_FXP_CLOSE
    message has the form:
    - byte 				SSH_FXP_CLOSE
    - uint32				id
    - string				handle
    */

void backup_op_close(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    struct backup_subsystem_s *backup=&session->subsystem.backup;
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("backup_op_close (%i)", (int) gettid());

    /* handle is 16 bytes long, so message is 4 + 16 = 20 */

    if (header->len==4 + BACKUP_HANDLE_SIZE) {
	unsigned int len=0;
	unsigned long long id=0;

	len=get_uint32(&buffer[0]);

	if (read_backuphandle(&buffer[4], len, &id)==BACKUP_HANDLE_SIZE) {
	    unsigned int error=0;
	    struct backuphandle_s *handle=find_backuphandle(session, id, &error);

	    if (handle) (* handle->close)(handle);

	}

    }

    reply_sftp_status_simple(session, header->id, SSH_FX_OK);
    return;

    disconnect:

    finish_session(session);

}
