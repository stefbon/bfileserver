/*
  2010, 2011, 2012, 2013, 2014, 2015 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>

#include <pthread.h>
#include <dirent.h>

#define LOGGING
#include "logging.h"

#include "clientsession.h"
#include "simple-list.h"
#include "simple-locking.h"

#include "backup/db-common-base.h"
#include "backup/db-common.h"

#ifdef HAVE_SQLITE3
#include "backup/sqlite/db-sqlite-common.h"
#include "backup/sqlite/db-sqlite-check.h"
#include "backup/sqlite/db-sqlite-dentry.h"
#include "backup/sqlite/db-sqlite-init.h"
#include "backup/sqlite/db-sqlite-version.h"
#endif

struct db_backend_s {
    int 			(* init_db)();
    int				(* check_db)(struct db_conn_s *db_conn, unsigned int *error);
    int				(* init_connection)(struct db_conn_s *db_conn, unsigned int *error);
    void			(* close_connection)(struct db_conn_s *db_conn);
    void			(* close_db)();
    void			(* free_data)(void *data);

    unsigned char 		(* get_dentry)(struct db_conn_s *db_conn, struct db_name_s *db_name, unsigned long long *db_id_parent, struct db_stat_s *db_stat, unsigned int *error);
    unsigned char 		(* lookup_dentry)(struct db_conn_s *db_conn, unsigned long long db_id_parent, struct db_name_s *db_name, unsigned int owner, struct db_stat_s *db_stat);
    unsigned char 		(* set_dentry_type)(struct db_conn_s *db_conn, struct db_stat_s *db_stat);

    void 			*(* opendir)(struct db_conn_s *db_conn, unsigned long long db_id, unsigned char type, unsigned int owner, unsigned int *error);
    unsigned char 		(* readdir)(void *data, unsigned char type, struct db_name_s *db_name, struct db_stat_s *db_stat);
    void 			(* closedir)(void *data);

    unsigned char 		(* create_dentry)(struct db_conn_s *db_conn, unsigned long long db_id_parent, unsigned int owner, struct db_name_s *db_name, unsigned char *created);

    unsigned char 		(* create_version)(struct db_conn_s *db_conn, unsigned long long fileid, unsigned long long snapshot, unsigned char *created);

};

#define DB_CONN_COUNT_MAX	10

static struct db_backend_s db_backend;

int check_database_backend(unsigned char type)
{
    int result=-1;

#ifdef HAVE_SQLITE3

    if (type==FUSE_BACKUP_DB_BACKEND_SQLITE) {

	result=0;

	db_backend.init_db=init_db_sqlite;
	db_backend.init_connection=init_connection_sqlite;
	db_backend.check_db=check_db_sqlite;
	db_backend.close_connection=close_connection_sqlite;
	db_backend.close_db=close_db_sqlite;
	db_backend.free_data=free_data_sqlite;

	db_backend.get_dentry=get_dentry_sqlite;
	db_backend.lookup_dentry=lookup_dentry_sqlite;
	db_backend.set_dentry_type=set_dentry_stat_sqlite;

	db_backend.opendir=opendir_sqlite;
	db_backend.readdir=readdir_sqlite;
	db_backend.closedir=closedir_sqlite;

	db_backend.create_dentry=create_dentry_sqlite;
	db_backend.create_version=create_version_sqlite;

    }

#endif

    return result;

}

static struct db_conn_s *get_db_conn(struct backup_subsystem_s *backup, unsigned int *error)
{
    struct db_conn_s *db_conn=NULL;
    struct list_element_s *list=NULL;

    logoutput("get_db_conn");

    pthread_mutex_lock(&backup->mutex);

    list=get_list_head(&backup->db_conn.head, &backup->db_conn.tail);

    if (list) {

	db_conn=(struct db_conn_s *) ( ((char *) list) - offsetof(struct db_conn_s, list));

    } else {

	db_conn=malloc(sizeof(struct db_conn_s));

	if (db_conn) {

	    db_conn->backup=backup;
	    db_conn->list.next=NULL;
	    db_conn->list.prev=NULL;
	    db_conn->type=0;
	    init_simple_nonelock(&backup->locking, &db_conn->lock);

	    *error=0;

	    logoutput("get_system_db_conn: init connection");

	    if (db_backend.init_connection(db_conn, error)==0) {

		backup->db_count++;

	    } else {

		logoutput("get_system_db_conn: unable to create a connection");

		db_backend.close_connection(db_conn);
		free(db_conn);
		db_conn=NULL;

		*error=-EIO;

	    }

	} else {

	    *error=ENOMEM;

	}

    }

    pthread_mutex_unlock(&backup->mutex);

    return db_conn;

}

static int lock_db_conn(struct db_conn_s *db_conn, const char *how)
{
    struct backup_subsystem_s *backup=db_conn->backup;

    logoutput("lock_db_conn: how %s", how);

    if (strcmp(how, "read")==0) {

	init_simple_readlock(&backup->locking, &db_conn->lock);

    } else {

	init_simple_writelock(&backup->locking, &db_conn->lock);

    }

    logoutput("lock_db_conn: simple_lock");

    return simple_lock(&db_conn->lock);

}

static int unlock_db_conn(struct db_conn_s *db_conn)
{
    return simple_unlock(&db_conn->lock);
}

void queue_db_conn(struct db_conn_s *db_conn)
{
    struct backup_subsystem_s *backup=db_conn->backup;

    pthread_mutex_lock(&backup->mutex);

    add_list_element_last(&backup->db_conn.head, &backup->db_conn.tail, &db_conn->list);

    pthread_mutex_unlock(&backup->mutex);

}

void close_all_db_conn(struct backup_subsystem_s *backup)
{
    struct list_element_s *list=NULL;

    pthread_mutex_lock(&backup->mutex);

    list=get_list_head(&backup->db_conn.head, &backup->db_conn.tail);

    while (list) {

	struct db_conn_s *db_conn=(struct db_conn_s *) ( ((char *) list) - offsetof(struct db_conn_s, list));
	db_backend.close_connection(db_conn);
	free(db_conn);

	list=get_list_head(&backup->db_conn.head, &backup->db_conn.tail);

    }

    pthread_mutex_unlock(&backup->mutex);

}

int init_db()
{
    logoutput("init_db");
    return db_backend.init_db();
}

void close_db()
{
    db_backend.close_db();
}

int db_check(struct backup_subsystem_s *backup, unsigned int *error)
{
    struct db_conn_s *db_conn=NULL;
    int result=-1;

    logoutput("db_check");

    db_conn=get_db_conn(backup, error);
    if (db_conn) {

	if (lock_db_conn(db_conn, "write")==0) {

	    result=db_backend.check_db(db_conn, error);
	    unlock_db_conn(db_conn);

	}

	queue_db_conn(db_conn);

    }

    return result;
}

void db_free_data(void *data)
{
    db_backend.free_data(data);
}

unsigned char db_get_dentry(struct backup_subsystem_s *backup, struct db_name_s *db_name, unsigned long long *db_id_parent, struct db_stat_s *db_stat, unsigned int *error)
{
    struct db_conn_s *db_conn=NULL;
    unsigned char result=0;

    db_conn=get_db_conn(backup, error);
    if (db_conn) {

	if (lock_db_conn(db_conn, "read")==0) {

	    result=db_backend.get_dentry(db_conn, db_name, db_id_parent, db_stat, error);
	    unlock_db_conn(db_conn);

	}

	queue_db_conn(db_conn);

    }

    return result;
}

unsigned char db_lookup_dentry(struct backup_subsystem_s *backup, unsigned long long parent_db_id, struct db_name_s *db_name, unsigned int owner, struct db_stat_s *db_stat)
{
    struct db_conn_s *db_conn=NULL;
    unsigned int error=0;
    unsigned char result=0;

    db_conn=get_db_conn(backup, &error);
    if (db_conn) {

	if (lock_db_conn(db_conn, "read")==0) {

	    result=db_backend.lookup_dentry(db_conn, parent_db_id, db_name, owner, db_stat);
	    unlock_db_conn(db_conn);

	}

	queue_db_conn(db_conn);

    }

    return result;
}


unsigned char db_set_dentry_type(struct backup_subsystem_s *backup, struct db_stat_s *db_stat)
{
    struct db_conn_s *db_conn=NULL;
    unsigned int error=0;
    unsigned char result=0;

    logoutput("db_set_dentry_type");

    db_conn=get_db_conn(backup, &error);
    if (db_conn) {

	if (lock_db_conn(db_conn, "write")==0) {

	    result=db_backend.set_dentry_type(db_conn, db_stat);
	    unlock_db_conn(db_conn);

	}

	queue_db_conn(db_conn);

    }

    return result;

}

//void db_delete_dentry_record(unsigned long long db_id)
//{
//    struct db_conn_s *db_conn=NULL;
//    unsigned int error=0;

//    db_conn=get_db_conn(&error);
//    if (db_conn) db_backend.delete_dentry_record(db_conn, db_id);

//}

void *db_opendir(struct backup_subsystem_s *backup, unsigned long long db_id, unsigned char type, unsigned int owner, unsigned int *error)
{
    struct db_conn_s *db_conn=NULL;
    void *ptr=NULL;

    db_conn=get_db_conn(backup, error);
    if (db_conn) {

	if (lock_db_conn(db_conn, "read")==0) {

	    ptr=db_backend.opendir(db_conn, db_id, type, owner, error);
	    unlock_db_conn(db_conn);

	}

	queue_db_conn(db_conn);

    }

    return ptr;

}

unsigned char db_readdir(void *data, unsigned char type, struct db_name_s *db_name, struct db_stat_s *db_stat)
{
    return db_backend.readdir(data, type, db_name, db_stat);
}

void db_closedir(void *data)
{
    db_backend.closedir(data);
}

unsigned char db_create_dentry(struct backup_subsystem_s *backup, unsigned long long parent_db_id, unsigned int owner, struct db_name_s *db_name, unsigned char *created)
{
    struct db_conn_s *db_conn=NULL;
    unsigned int error=0;
    unsigned char result=0;

    logoutput("db_create_dentry");

    db_conn=get_db_conn(backup, &error);
    if (db_conn) {

	logoutput("db_create_dentry: db_conn lock");

	if (lock_db_conn(db_conn, "write")==0) {

	    logoutput("db_create_dentry: db_backend.create_entry %li owner %i name %.*s", parent_db_id, owner, db_name->len, db_name->name);

	    result=db_backend.create_dentry(db_conn, parent_db_id, owner, db_name, created);
	    unlock_db_conn(db_conn);

	}

	queue_db_conn(db_conn);

    }

    return result;
}

unsigned char db_create_version(struct backup_subsystem_s *backup, unsigned long long fileid, unsigned long long snapshot, unsigned char *created)
{
    struct db_conn_s *db_conn=NULL;
    unsigned int error=0;
    unsigned char result=0;

    logoutput("db_create_version");

    db_conn=get_db_conn(backup, &error);
    if (db_conn) {

	logoutput("db_create_version: db_conn lock");

	if (lock_db_conn(db_conn, "write")==0) {

	    logoutput("db_create_version: db_backend.create_version fileid %li snapshot %li", fileid, snapshot);

	    result=db_backend.create_version(db_conn, fileid, snapshot, created);
	    unlock_db_conn(db_conn);

	}

	queue_db_conn(db_conn);

    }

    return result;
}

unsigned long long get_db_id_path(struct backup_subsystem_s *backup, char *path, unsigned int owner, unsigned int mode, struct db_name_s *db_name)
{
    char *sep=NULL;
    struct db_stat_s db_stat;
    char *name=NULL;
    unsigned long long db_id=0;
    unsigned int len=strlen(path);
    char *pos=path;

    while (pos < path + len && *pos=='/') pos++;
    if (pos == path + len) return 0;

    searchentry:

    sep=memchr(pos, '/', len);
    if (sep) {

	*sep='\0';

    } else {

	/* when not interested in last component stop here */

	if (mode==GET_PATH_MODE_PARENT) {

	    db_name->name=pos;
	    db_name->len=strlen(pos);
	    return db_id;

	}

    }

    db_name->name=pos;
    db_name->len=strlen(pos);

    if (db_lookup_dentry(backup, db_id, db_name, owner, &db_stat)==1) {

	db_id=db_stat.id;

	if (sep) {

	    *sep='/';
	    pos=sep+1;
	    goto searchentry;

	}

	return db_id;

    }

    return 0;

}

void copy_db_stat_stat(struct db_stat_s *db_stat, struct stat *st)
{

    st->st_size=db_stat->size;
    st->st_mode=db_stat->mode;
    st->st_uid=db_stat->uid;
    st->st_gid=db_stat->gid;
    st->st_mtim.tv_sec=db_stat->mtim;
    st->st_mtim.tv_nsec=db_stat->mtim_nsec;
    st->st_ctim.tv_sec=db_stat->ctim;
    st->st_ctim.tv_nsec=db_stat->ctim_nsec;
}

void copy_stat_db_stat(struct db_stat_s *db_stat, struct stat *st)
{

    db_stat->size=st->st_size;
    db_stat->mode=st->st_mode;
    db_stat->uid=st->st_uid;
    db_stat->gid=st->st_gid;
    db_stat->mtim=st->st_mtim.tv_sec;
    db_stat->mtim_nsec=st->st_mtim.tv_nsec;
    db_stat->ctim=st->st_ctim.tv_sec;
    db_stat->ctim_nsec=st->st_ctim.tv_nsec;
}
