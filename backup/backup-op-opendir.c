/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>
#include <sys/syscall.h>

#include "main.h"
#define LOGGING
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "commonhandle.h"
#include "filehandle.h"
#include "dirhandle.h"
#include "shared/sftp-handle.h"
#include "shared/sftp-reply-handle.h"
#include "shared/sftp-reply-data.h"
#include "shared/sftp-reply-status.h"
#include "shared/sftp-reply-name.h"
#include "shared/sftp-attributes-write.h"

#include "backuphandle.h"
#include "db-common.h"

#define BACKUP_READDIR_NAMES_SIZE		1024

int send_backup_handle(struct ssh_session_s *session, struct sftp_header_s *header, struct backuphandle_s *handle)
{
    char bytes[BACKUP_HANDLE_SIZE];
    write_backuphandle(handle, bytes);
    return reply_sftp_handle(session, header->id, bytes, BACKUP_HANDLE_SIZE);
}

void backup_op_opendir(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    struct backup_subsystem_s *backup=&session->subsystem.backup;
    struct ssh_user_s *user=&session->user;
    unsigned int status=SSH_FX_BAD_MESSAGE;
    unsigned int error=0;
    struct backuphandle_s *handle=NULL;
    unsigned long long db_id=0;
    char path[header->len]; /* big enough */
    struct db_name_s db_name;
    unsigned char type=_DB_ENTRY_TYPE_DIRECTORY;

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	/* sftp packet size is at least:
	    - 4 + len ... path (len maybe zero) */

	if (header->len == len + 4) {

	    memcpy(path, &buffer[pos], len);
	    pos+=len;
	    path[len]='\0';

	} else {

	    goto error;

	}

    }

    logoutput("_backup_op_opendir: path %s", path);

    /* TODO: here path is always absolute
	- if path is not starting with a slash, it's relative to the db id of users home */

    if (strlen(path)==0 || (strlen(path)==1 && (strcmp(path, "/")==0 || strcmp(path, ".")==0)) || (strlen(path)==2 && strcmp(path, "/.")==0)) {

	db_id=0; /* root */

    } else {

	db_id=get_db_id_path(backup, path, user->pwd.pw_uid, GET_PATH_MODE_FULL, &db_name);

	if (db_id==0) {

	    status=SSH_FX_NO_SUCH_FILE;
	    goto error;

	}

    }

    if (db_id>0) {
	struct db_stat_s db_stat;

	db_stat.id=db_id;

	if (db_get_dentry(backup, &db_name, NULL, &db_stat, &error)!=1) {

	    logoutput("backup_op_opendir: failed to get dentry type");
	    status=SSH_FX_FAILURE;
	    goto error;

	}

	type=db_stat.type;

    }

    logoutput("backup_op_opendir: create handle");

    handle=create_backuphandle(session, db_id, &error);

    if (handle==NULL) {

	status=SSH_FX_FAILURE;
	goto error;

    }

    handle->type=type;

    logoutput("backup_op_opendir: open");

    handle->data=db_opendir(backup, db_id, type, user->pwd.pw_uid, &error);

    if (handle->data==NULL) {

	logoutput("backup_op_opendir: error %i open (%s)", error, strerror(error));
	status=SSH_FX_FAILURE;
	goto error;

    }

    if (send_backup_handle(session, header, handle)==-1) {

	logoutput("backup_op_opendir: error sending handle reply");

    }

    return;

    error:

    logoutput("backup_op_opendir: status %i", status);

    if (handle) free_backuphandle(handle);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_READ
    message has the form:
    - byte 				SSH_FXP_READ
    - uint32				id
    - string				handle
    */

void backup_op_readdir(struct ssh_session_s *session, struct sftp_header_s *header, char *data)
{
    struct backup_subsystem_s *backup=&session->subsystem.backup;
    unsigned int status=SSH_FX_BAD_MESSAGE;
    //unsigned int valid=SSH_FILEXFER_ATTR_SIZE | SSH_FILEXFER_ATTR_OWNERGROUP | SSH_FILEXFER_ATTR_PERMISSIONS;
    unsigned int valid=SSH_FILEXFER_ATTR_SIZE | SSH_FILEXFER_ATTR_OWNERGROUP | SSH_FILEXFER_ATTR_PERMISSIONS | SSH_FILEXFER_ATTR_ACCESSTIME | SSH_FILEXFER_ATTR_MODIFYTIME | SSH_FILEXFER_ATTR_SUBSECOND_TIMES | SSH_FILEXFER_ATTR_CTIME;

    logoutput("backup_op_readdir (%i)", (int) gettid());

    /* handle is 16 bytes long, so message is 4 + 16 = 20 bytes */

    if (header->len == 4 + BACKUP_HANDLE_SIZE) {
	unsigned int len=0;
	unsigned long long id=0;

	len=get_uint32(&data[0]);

	if (read_backuphandle(&data[4], len, &id)==BACKUP_HANDLE_SIZE) {
	    unsigned int error=0;
	    struct backuphandle_s *handle=find_backuphandle(session, id, &error);

	    if (handle) {
		unsigned int error=0;
		char buffer[BACKUP_READDIR_NAMES_SIZE];
		unsigned int pos=0;
		unsigned int count=0;
		struct db_name_s db_name;
		struct db_stat_s db_stat;

		if (handle->data==NULL) {

		    status=SSH_FX_FAILURE;
		    goto out;

		} else if (handle->flags & BACKUPHANDLE_FLAG_EOF) {

		    status=SSH_FX_EOF;
		    goto out;

		}

		readoneentry:

		if ((handle->flags & BACKUPHANDLE_FLAG_EOF)==0 && db_readdir(handle->data, handle->type, &db_name, &db_stat)==1) {
		    struct stat st;

		    logoutput("backup_op_readdir: reply found %.*s", db_name.len, db_name.name);

		    copy_db_stat_stat(&db_stat, &st);
		    error=0;
		    pos+=write_readdir_attr(&session->subsystem.backup, &buffer[pos], BACKUP_READDIR_NAMES_SIZE - pos, db_name.name, &st, valid, &error);

		    if (error==0) {

			count++;
			goto readoneentry;

		    }

		} else {

		    handle->flags |= BACKUPHANDLE_FLAG_EOF;

		}

		logoutput("backup_op_readdir: reply_sftp_names: count %i", count);

		if (reply_sftp_names(session, header->id, count, buffer, pos, 0)==-1) {

		    logoutput("backup_op_readdir: error sending readdir names");

		}

		return;

	    } else {

		logoutput("backup_op_readdir: handle not found");

		if (error==EPERM) {

		    /* serious error: client wants to use a handle he has no permissions for */

		    logoutput("backup_op_readdir: client has no permissions to use handle");
		    goto disconnect;

		}

		status=SSH_FX_INVALID_HANDLE;

	    }

	}

    }

    out:

    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:
    finish_session(session);

}
