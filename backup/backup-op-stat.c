/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "shared/sftp-attributes-write.h"

#include "shared/sftp-reply-attrs.h"
#include "shared/sftp-reply-status.h"

#include "db-common.h"



/* SSH_FXP_(L)STAT/
    message has the form:
    - byte 				SSH_FXP_STAT or SSH_FXP_LSTAT
    - uint32				id
    - string				path
    - uint32				flags
    */

void backup_op_stat(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    struct backup_subsystem_s *backup=&session->subsystem.backup;
    unsigned int status=SSH_FX_BAD_MESSAGE;
    unsigned int valid=SSH_FILEXFER_ATTR_SIZE | SSH_FILEXFER_ATTR_OWNERGROUP | SSH_FILEXFER_ATTR_PERMISSIONS | SSH_FILEXFER_ATTR_ACCESSTIME | SSH_FILEXFER_ATTR_MODIFYTIME | SSH_FILEXFER_ATTR_SUBSECOND_TIMES | SSH_FILEXFER_ATTR_CTIME;
    char path[header->len]; /* big enough */
    unsigned long long db_id=0;
    struct db_name_s db_name;

    logoutput("backup_op_stat");

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	/* sftp packet size is at least:
	    - 4 + len ... path (len maybe zero) */

	if (header->len > len + 4) {

	    memcpy(path, &buffer[pos], len);
	    path[len]='\0';

	} else {

	    logoutput("backup_op_stat: ! header len %i > len %i", header->len, len);
	    goto error;

	}

    }

    logoutput("backup_op_stat: path %s", path);

    if (strlen(path)==0 || (strlen(path)==1 && (strcmp(path, "/")==0 || strcmp(path, ".")==0)) || (strlen(path)==2 && strcmp(path, "/.")==0)) {

	db_id=0; /* root */
	logoutput("backup_op_stat: root");

    } else {

	db_id=get_db_id_path(backup, path, session->user.pwd.pw_uid, GET_PATH_MODE_FULL, &db_name);
	logoutput("backup_op_stat: id %li", db_id);

	if (db_id==0) {

	    status=SSH_FX_NO_SUCH_FILE;
	    goto error;

	}

    }

    if (db_id==0) {
	struct backup_subsystem_s *backup=&session->subsystem.backup;
	struct sftp_attr_s attr;
	unsigned int size=0;
	struct stat st;

	st.st_size=0;
	st.st_mode=(S_IFDIR | (S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH));
	st.st_uid=session->user.pwd.pw_uid;
	st.st_gid=session->user.pwd.pw_gid;
	/* change this */
	get_current_time(&st.st_mtim);
	get_current_time(&st.st_ctim);
	get_current_time(&st.st_atim);

	logoutput("sftp_op_stat: uid %i gid %i", st.st_uid, st.st_gid);

	size=write_attributes_len(backup, &attr, &st, valid);
	char buffer[size];

	size=write_attributes(backup, buffer, size, &attr, valid);

	if (reply_sftp_attrs(session, header->id, buffer, size)==-1) {

	    logoutput("sftp_op_stat: error sending attr");

	}

	return;

    } else {
	unsigned long long db_id_parent=0;
	char name[255];
	struct db_name_s db_name;
	struct db_stat_s db_stat;
	unsigned int error=0;

	memset(name, '\0', 255);

	db_stat.id=db_id;
	db_name.name=name;
	db_name.len=255;

	if (db_get_dentry(backup, &db_name, &db_id_parent, &db_stat, &error)==1) {
	    struct backup_subsystem_s *backup=&session->subsystem.backup;
	    struct sftp_attr_s attr;
	    unsigned int size=0;
	    struct stat st;

	    copy_stat_db_stat(&db_stat, &st);

	    st.st_uid=session->user.pwd.pw_uid;
	    st.st_gid=session->user.pwd.pw_gid;
	    st.st_mode=(S_IFDIR | (S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)); /* for now */

	    size=write_attributes_len(backup, &attr, &st, valid);
	    char buffer[size];

	    size=write_attributes(backup, buffer, size, &attr, valid);

	    if (reply_sftp_attrs(session, header->id, buffer, size)==-1) {

		logoutput("sftp_op_stat: error sending attr");

	    }

	    return;

	}

    }

    error:

    reply_sftp_status_simple(session, header->id, SSH_FX_NO_SUCH_FILE);
}

void backup_op_lstat(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{

    logoutput("backup_op_lstat");

    backup_op_stat(session, header, buffer);

}

/* SSH_FXP_FSTAT/
    message has the form:
    - byte 				SSH_FXP_FSTAT
    - uint32				id
    - string				handle
    - uint32				flags
    */

void backup_op_fstat(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;
    error:

    reply_sftp_status_simple(session, header->id, status);

    disconnect:

    finish_session(session);

}
