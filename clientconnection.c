/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pwd.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#include "logging.h"

#include "main.h"
#include "utils.h"
#include "pathinfo.h"

#include "workerthreads.h"
#include "beventloop.h"
#include "beventloop-signal.h"
#include "beventloop-timer.h"
#include "beventloop-xdata.h"

#include "options.h"
#include "pidfile.h"
#include "localsocket.h"
#include "clientsession.h"

struct fs_options_struct fs_options;

static signed char check_trusted_program(pid_t pid)
{
    unsigned int len=512;
    char procexe[32];
    signed char result=-1;

    snprintf(procexe, 32, "/proc/%i/exe", (int) pid);

    while(1) {
	char path[len];
	int size=0;

	size=readlink(procexe, path, len);

	if (size==len) {

	    size+=512;
	    continue;

	} else {

	    path[size]='\0';

	    logoutput("check_trusted_program: found program %s for pid %i", path, pid);

	    /* TODO: allow more SSH servers */
	    if (strcmp(path, "/usr/bin/sshd")==0) result=0;
	    break;

	}

    }

    return result;

}

static char *read_environ_pid(pid_t pid, unsigned int *len)
{
    int fd=0;
    char path[64];
    unsigned int size=4096;
    char *environ=NULL;

    snprintf(path, 64, "/proc/%i/environ", (int) pid);
    fd=open(path, O_RDONLY);

    if (fd>=0) {

	while (1) {
	    char buffer[size];
	    int read=0;

	    memset(buffer, '\0', size);
	    read=pread(fd, buffer, size, 0);

	    if (read==-1|| read==0) {

		if (environ) free(environ);
		*len=0;
		return NULL;

	    } else if (read>=size) {

		if (environ) free(environ);
		*len=0;
		size+=512;
		continue;

	    }

	    environ=malloc(read+1);

	    if (environ) {

		logoutput("read_environ_pid: found environment for pid %i (size %i)", (int) pid, read);

		memcpy(environ, buffer, read);
		environ[read]='\0';
		*len=read+1;

	    }

	    break;

	}

	close(fd);

    }

    return environ;
}


static int read_environ_value(pid_t pid, char *name, char **str)
{
    unsigned int size=0;
    char *environ=read_environ_pid(pid, &size);

    logoutput("read_environ_value: get value for %s (env=%s)", name, (environ) ? "set" : "unset");

    if (environ) {
	unsigned int len=strlen(name);
	char buffer[size+1];
	char search[len+2];
	char *pos=NULL;

	buffer[0]='\0';
	memcpy(&buffer[1], environ, size);
	free(environ);

	search[0]='\0';
	memcpy(&search[1], name, len);
	search[len+1]='=';

	logoutput("read_environ_value: look for %.*s", len, &search[1]);

	pos=memmem(buffer, size+1, search, len+2);

	if (pos) {
	    char *sep=memchr(pos, '=', (unsigned int)(buffer + size + 1 - pos));

	    if (sep) {

		logoutput("read_environ_value: found");

		/* value found */

		len=strlen(sep + 1);
		*str=malloc(len + 1);
		if (*str) {

		    memcpy(*str, sep + 1, len + 1); /* including trailing zero byte */
		    return (len);

		}

	    }

	}

    }

    return 0;

}

static int process_client_socket_event(int fd, void *data, uint32_t events)
{
    struct fs_connection_s *conn=(struct fs_connection_s *) data;

    if (events & (EPOLLHUP|EPOLLRDHUP)) {

	/* remote hangup... which will probably not happen (test this!)
	    cause the sshd process will send a message to close the connection */

	(* conn->ops.client.disconnect)(conn, 1);

    } else {

	(* conn->ops.client.event)(conn, data, events);

    }

    return 0;

}

/* receive up to 3 messages at the same time send by client
    - every msghdr has two iovec
*/

static int receive_sftp_init(struct fs_connection_s *client)
{
    char type[3];
    char buffers[3][64];
    struct iovec iovecs[3][2];
    struct mmsghdr msgs[3];
    struct timespec timeout;
    int fd=-1;
    unsigned int len=get_msg_controllen(NULL, "int");
    char extra[3][len];

    memset(msgs, 0, sizeof(msgs));
    for (unsigned int i=0; i<3; i++) {

	type[i]=0;
	memset(buffers[i], '\0', 64);

	iovecs[i][0].iov_base		= &type[i];
	iovecs[i][0].iov_len		= 1;

	iovecs[i][1].iov_base		= buffers[i];
	iovecs[i][1].iov_len		= 64;

	msgs[i].msg_hdr.msg_iov		= &iovecs[i][0];
	msgs[i].msg_hdr.msg_iovlen	= 2;

	msgs[i].msg_hdr.msg_control	= extra[i];
	msgs[i].msg_hdr.msg_controllen	= len;

    }

    timeout.tv_sec=1;
    timeout.tv_nsec=0;

    if (recvmmsg(client->fd, msgs, 3, 0, &timeout)==3) {

	for (unsigned int i=0; i<3; i++) {

	    if (type[i]==1) {

		logoutput("receive_sftp_init: received BFILE_MSG_INIT with %s", buffers[i]);

	    } else if (type[i]==2) {

		fd=read_fd_msg(&msgs[i]);
		logoutput("receive_sftp_init: received BFILE_MSG_FD with fd %i and %s", fd, buffers[i]);

	    } else if (type[i]==3) {

		logoutput("receive_sftp_init: received BFILE_MSG_CLOSE");

	    } else {

		logoutput("receive_sftp_init: not reckognized type %i", type[i]);

	    }

	}

    }

    return fd;

}

static void complete_client_connection(void *ptr)
{
    struct fs_connection_s *client=(struct fs_connection_s *) ptr;
    struct ssh_session_s *session=get_session_connection(client);
    struct bevent_xdata_s *xdata=NULL;
    char *address=NULL;
    unsigned int len=0;
    unsigned int error=0;
    int fd=-1;

    if (check_trusted_program(client->ops.client.pid)==-1) {

	logoutput("complete_client_connection: error checking peer program");
	// goto disconnect;

    }

    /* get the address of the remote host via the pid of connecting (local) process
	(with openssh this is set in the env variable SSH_CONNECTION or SSH_CLIENT)
    */

    len=read_environ_value(client->ops.client.pid, "SSH_CONNECTION", &address);

    if (address && len>0) {
	char *sep=NULL;

	sep=memchr(address, ' ', len);
	if (sep) *sep='\0';
	len=strlen(address);

	logoutput("complete_client_connection: found address %s from envvar SSH_CONNECTION of pid %i", address, (int) client->ops.client.pid);

	if (check_family_ip_address(address, "ipv4")==1) {

	    memcpy(session->address.ip.v4, address, len);
	    session->address.type=NETWORK_ADDRESS_TYPE_IPV4;

	} else if (check_family_ip_address(address, "ipv6")==1) {

	    memcpy(session->address.ip.v6, address, len);
	    session->address.type=NETWORK_ADDRESS_TYPE_IPV6;

	} else {

	    goto disconnect;

	}

    }

    if (address) {

	free(address);
	address=NULL;
	len=0;

    }

    /* receive the first messages */

    // fd=receive_sftp_init(client);

    if (fd>0) {

	/* fd received is the new fd */

	logoutput("complete_client_connection: replace client fd %i with fd %i (TODO)", client->fd, fd);
	close(client->fd);
	client->fd=fd;

    }

    /* use the standard eventloop */

    xdata=add_to_beventloop(client->fd, EPOLLIN | EPOLLET, process_client_socket_event, (void *) client, &client->xdata, NULL);

    if (! xdata) {

	logoutput("complete_client_connection: error adding client fd %i to mainloop", client->fd);
	goto disconnect;

    }

    return;

    disconnect:

    if (address) {
	free(address);
	address=NULL;
    }

    if (xdata) remove_xdata_from_beventloop(xdata);
    (* client->ops.client.disconnect)(client, 0);

}

/* start a thread to complete the client connection */

static void init_client_connection(struct fs_connection_s *client)
{
    unsigned int error=0;
    work_workerthread(NULL, 0, complete_client_connection, (void *) client, &error);
    if (error>0) logoutput("init_client_connection: error %i startin thread (%s)", error, strerror(error));
}

void disconnect_client_connection(struct fs_connection_s *conn, unsigned char remote)
{
    struct fs_connection_s *s_conn=conn->ops.client.server;

    logoutput("disconnect_client_connection: fd %i", conn->fd);

    pthread_mutex_lock(&s_conn->ops.server.mutex);
    remove_list_element(&s_conn->ops.server.head, &s_conn->ops.server.tail, &conn->list);
    pthread_mutex_unlock(&s_conn->ops.server.mutex);

    /* what more:
	- close connection
	- remove from eventloop
	- free session
	*/

    /* what when the connection is disconnected not remote
	for example when this daemon is terminated when connections are still up
	send a message?? or does the local sshd(...) process handle this properly? */

    logoutput("disconnect_client_connection: close fd %i", conn->fd);

    if (conn->fd>0) {

	close(conn->fd);
	conn->fd=0;
	conn->xdata.fd=0;

    }

    logoutput("disconnect_client_connection: remove fd %i", conn->fd);
    remove_xdata_from_beventloop(&conn->xdata);

}

static void process_client_event(struct fs_connection_s *client, void *ptr, uint32_t events)
{
    struct ssh_session_s *session=get_session_connection(client);
    struct ssh_receive_s *receive=&session->receive;
    int lenread=0;
    unsigned int error=0;

    /* read data */
    /* recv, check and queue rawdata */

    readbuffer:

    lenread=recv(client->fd, receive->buffer, receive->size, 0);

    if (lenread>0) {

	(* receive->queue_data)(session, receive->buffer, lenread);

    } else {

	if (lenread==0) {

	    (* client->ops.client.disconnect)(client, 1);

	} else if (error==EAGAIN || error==EWOULDBLOCK) {

	    goto readbuffer;

	} else if (error==ECONNRESET || error==ENOTCONN || error==EBADF || error==ENOTSOCK) {

	    logoutput("process_client_event: error %i (%s)", error, strerror(error));

	} else {

	    logoutput("process_client_event: error %i (%s)", error, strerror(error));

	}

    }

}

/* accept connection on sftp socket */

struct fs_connection_s *accept_client_connection_sftp(uid_t uid, gid_t gid, pid_t pid, void *ptr)
{
    struct ssh_session_s *session=NULL;

    logoutput("accept_client_connection_sftp");

    /* TODO: filter on uid */

    session=create_session(uid, SSH_SUBSYSTEM_TYPE_SFTP);

    if (session) {
	struct fs_connection_s *client=&session->connection;

	init_connection(client, FS_CONNECTION_TYPE_LOCALCLIENT);
	client->ops.client.disconnect=disconnect_client_connection;
	client->ops.client.event=process_client_event;
	client->ops.client.init=init_client_connection;
	client->ops.client.uid=uid;
	client->ops.client.pid=pid;

	init_sftp_subsystem(session);
	return client;

    }

    return NULL;

}

struct fs_connection_s *accept_client_connection_backup(uid_t uid, gid_t gid, pid_t pid, void *ptr)
{
    struct ssh_session_s *session=NULL;

    logoutput("accept_client_connection_backup");

    /* TODO: filter on uid */

    session=create_session(uid, SSH_SUBSYSTEM_TYPE_BACKUP);

    if (session) {
	struct fs_connection_s *client=&session->connection;

	init_connection(client, FS_CONNECTION_TYPE_LOCALCLIENT);
	client->ops.client.disconnect=disconnect_client_connection;
	client->ops.client.event=process_client_event;
	client->ops.client.init=init_client_connection;
	client->ops.client.uid=uid;
	client->ops.client.pid=pid;

	init_backup_subsystem(session);
	return client;

    }

    return NULL;

}
