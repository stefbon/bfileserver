/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/statvfs.h>
#include <sys/mount.h>

#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#include "logging.h"

#include "main.h"
#include "utils.h"
#include "pathinfo.h"

#include "workerthreads.h"
#include "beventloop.h"
#include "beventloop-signal.h"
#include "beventloop-timer.h"
#include "beventloop-xdata.h"

#include "options.h"

// #include "fschangenotify.h"
#include "monitorsessions.h"
#include "monitormounts.h"

#include "pidfile.h"
#include "localsocket.h"
#include "clientconnection.h"
#include "filehandle.h"

struct fs_options_struct fs_options;
char *program_name=NULL;

static void workspace_signal_handler(struct beventloop_s *bloop, void *data, struct signalfd_siginfo *fdsi)
{
    unsigned int signo=fdsi->ssi_signo;

    if ( signo==SIGHUP || signo==SIGINT || signo==SIGTERM ) {

	logoutput("workspace_signal_handler: got signal (%i): terminating", signo);
	bloop->status=BEVENTLOOP_STATUS_DOWN;

    } else if ( signo==SIGIO ) {

	logoutput("workspace_signal_handler: received SIGIO signal");

	/*
	    TODO:
	    when receiving an SIGIO signal another application is trying to open a file
	    is this really the case?
	    then the fuse fs is the owner!?
	*/

    } else if ( signo==SIGPIPE ) {

	logoutput("workspace_signal_handler: received SIGPIPE signal");

    } else if ( signo==SIGCHLD ) {

	logoutput("workspace_signal_handler: received SIGCHLD signal");

    } else if ( signo==SIGUSR1 ) {

	logoutput("workspace_signal_handler: received SIGUSR1 signal");

    } else {

        logoutput("workspace_signal_handler: received unknown %i signal", signo);

    }

}

int main(int argc, char *argv[])
{
    int res=0;
    unsigned int error=0;
    struct bevent_xdata_s *xdata=NULL;
    struct fs_connection_s sftpsocket;
    struct fs_connection_s backupsocket;

    /* daemonize */

    res=custom_fork();

    if (res<0) {

        fprintf(stderr, "MAIN: error daemonize.");
        return 1;

    } else if (res>0) {

	fprintf(stdout, "MAIN: created a service with pid %i.\n", res);
	return 0;

    }

    umask(0);
    program_name=argv[0];

    /* parse commandline options and initialize the fuse options */

    if (parse_arguments(argc, argv, &error)==-1) {

	if (error>0) logoutput_error("MAIN: error, cannot parse arguments, error: %i (%s).", error, strerror(error));
	goto options;

    }

    init_workerthreads(NULL);
    set_max_numberthreads(NULL, 6);

    if (init_beventloop(NULL, &error)==-1) {

        logoutput_error("MAIN: error creating eventloop, error: %i (%s).", error, strerror(error));
        goto post;

    } else {

	logoutput("MAIN: creating eventloop");

    }

    if (enable_beventloop_signal(NULL, workspace_signal_handler, NULL, &error)==-1) {

	logoutput_error("MAIN: error adding signal handler to eventloop: %i (%s).", error, strerror(error));
        goto out;

    } else {

	logoutput("MAIN: adding signal handler");

    }

    if (add_mountinfo_watch(NULL, &error)==-1) {

        logoutput_error("MAIN: unable to open mountmonitor, error=%i (%s)", error, strerror(error));
        goto out;

    } else {

	logoutput("MAIN: open mountmonitor");

    }

    if (init_commonhandles(&error)==-1) {

        logoutput_error("MAIN: unable to initialize global handle table, error=%i (%s)", error, strerror(error));
        goto out;

    } else {

	logoutput("MAIN: open global handle table");

    }

    // if (init_fschangenotify(NULL, &error)==-1) {

	// logoutput_error("MAIN: error initializing fschange notify, error: %i (%s)", error, strerror(error));
	// goto out;

    // }

    if (create_socket_path(&fs_options.socket)==0) {
	unsigned int alreadyrunning=0;
	unsigned int count=0;

	checkpidfile:

	alreadyrunning=check_pid_file(&fs_options.socket);

	if (alreadyrunning>0 && count < 10) {
	    char procpath[64];
	    struct stat st;

	    snprintf(procpath, 64, "/proc/%i/cmdline", alreadyrunning);

	    /* check here for existence of cmdline
		a better check will be to test also the cmdline contains this programname if it exists */

	    if (stat(procpath, &st)==-1) {

		/* pid file found, but no process */

		remove_pid_file(&fs_options.socket, (pid_t) alreadyrunning);
		alreadyrunning=0;
		count++;
		goto checkpidfile;

	    }

	}

	if (fs_options.sftp==1) {
	    unsigned int len=fs_options.socket.len + 32;
	    char path[len];

	    len=snprintf(path, len, "%s/sftp.sock", fs_options.socket.path);
	    if (len>0) {
		struct pathinfo_s socketpath=PATHINFO_INIT;

		socketpath.path=path;
		socketpath.len=len;

		if (check_socket_path(&socketpath, alreadyrunning)==-1) goto out;

		init_connection(&sftpsocket, FS_CONNECTION_TYPE_LOCALSERVER);

		if (create_local_serversocket(socketpath.path, &sftpsocket, NULL, accept_client_connection_sftp, &error)>=0) {

		    logoutput("MAIN: created sftp server socket %s", socketpath.path);

		} else {

		    logoutput("MAIN: error %i creating socket %s (%s)", error, socketpath.path, strerror(error));
		    goto out;

		}

	    }

	}

	if (fs_options.backup==1) {
	    unsigned int len=fs_options.socket.len + 32;
	    char path[len];

	    len=snprintf(path, len, "%s/backup.sock", fs_options.socket.path);
	    if (len>0) {
		struct pathinfo_s socketpath=PATHINFO_INIT;

		socketpath.path=path;
		socketpath.len=len;

		if (check_socket_path(&socketpath, alreadyrunning)==-1) goto out;

		init_connection(&backupsocket, FS_CONNECTION_TYPE_LOCALSERVER);

		if (create_local_serversocket(socketpath.path, &backupsocket, NULL, accept_client_connection_backup, &error)>=0) {

		    logoutput("MAIN: created backup server socket %s", socketpath.path);

		} else {

		    logoutput("MAIN: error %i creating socket %s (%s)", error, socketpath.path, strerror(error));
		    goto out;

		}

	    }

	}

    } else {

	logoutput("MAIN: error creating directory for socket %s", fs_options.socket.path);

    }

    create_pid_file(&fs_options.socket);
    res=start_beventloop(NULL);

    out:

    //logoutput("MAIN: end fschangenotify");
    //end_fschangenotify();

    post:

    logoutput("MAIN: terminate workerthreads");

    terminate_workerthreads(NULL, 0);

    logoutput("MAIN: destroy eventloop");
    clear_beventloop(NULL);
    remove_pid_file(&fs_options.socket, getpid());
    free_commonhandles();

    options:

    logoutput("MAIN: free options");
    free_options();

    if (error>0) {

	logoutput_error("MAIN: error (error: %i).", error);
	return 1;

    }

    return 0;

}
