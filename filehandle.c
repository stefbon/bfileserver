/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <pwd.h>
#include <grp.h>

#include <linux/kdev_t.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"
#include "simple-locking.h"
#include "commonhandle.h"
#include "filehandle.h"

static void close_filehandle(struct commonhandle_s *handle)
{
    struct filehandle_s *filehandle=(struct filehandle_s *) ( ((char *) handle) - offsetof(struct filehandle_s, handle));

    if (filehandle->handle.fd>0) {

	close(filehandle->handle.fd);
	filehandle->handle.fd=0;

    }

    free(filehandle);

}

static int get_lock_flags_dummy(struct filehandle_s *h, const char *what)
{
    return 0;
}

struct filehandle_s *find_filehandle(struct ssh_session_s *session, dev_t dev, uint64_t ino, unsigned int fd, unsigned int *error)
{
    struct commonhandle_s *handle=find_commonhandle(session, dev, ino, fd, error);

    if (handle && handle->type==COMMONHANDLE_TYPE_FILE) {

	return (struct filehandle_s *) ( ((char *) handle) - offsetof(struct filehandle_s, handle));

    }

    return NULL;

}

struct filehandle_s *insert_filehandle(struct ssh_session_s *session, dev_t dev, uint64_t ino, struct insert_filehandle_s *result)
{
    struct filehandle_s *filehandle=NULL;

    filehandle=malloc(sizeof(struct filehandle_s));

    if (filehandle) {

	memset(filehandle, 0, sizeof(struct filehandle_s));

	filehandle->handle.session=session;
	filehandle->handle.flags=0;
	filehandle->handle.type=COMMONHANDLE_TYPE_FILE;
	filehandle->handle.dev=dev;
	filehandle->handle.ino=ino;
	filehandle->handle.fd=0;
	filehandle->handle.refcount=0;
	filehandle->handle.close=close_filehandle;

	filehandle->posix_flags=0;
	filehandle->get_lock_flags=get_lock_flags_dummy;
	memcpy(&filehandle->server, &result->server, sizeof(union serverflags_u));

	insert_commonhandle_hash(&filehandle->handle);

    } else {

	result->error=ENOMEM;

    }

    return filehandle;

}

void decrease_filehandle(struct filehandle_s **pfilehandle)
{
    struct filehandle_s *filehandle=*pfilehandle;
    struct simple_lock_s lock;

    writelock_commonhandles(&lock);

    filehandle->handle.refcount--;

    if (filehandle->handle.refcount==0 && (filehandle->handle.flags & COMMONHANDLE_FLAG_CLOSE)) {

	remove_commonhandle_hash(&filehandle->handle);
	if (filehandle->handle.fd>0) close(filehandle->handle.fd);
	free(filehandle);
	*pfilehandle=NULL;

    }

    unlock_commonhandles(&lock);

}

struct filehandle_s *start_create_filehandle(struct ssh_session_s *session, char *path, struct insert_filehandle_s *result)
{
    struct filehandle_s *filehandle=NULL;
    struct commonhandle_s *handle=NULL;
    struct stat st;
    void *index=NULL;
    struct simple_lock_s lock;

    writelock_commonhandles(&lock);

    if (stat(path, &st)==0) {

	result->error=EEXIST;
	unlock_commonhandles(&lock);
	return NULL;

    }

    handle=get_next_commonhandle(&index, 0);

    while (handle) {

	/* test another client/process is busy creating this file by path
	    path is (temporary) stored in uint64_t ino */

	if (handle->flags & COMMONHANDLE_FLAG_CREATE) {
	    char *tmp=NULL;

	    tmp=(char *) (uintptr_t) handle->ino;

	    if (strcmp(tmp, path)==0) {

		filehandle=(struct filehandle_s *) ( ((char *) handle) - offsetof(struct filehandle_s, handle));
		result->status=EEXIST;
		break;

	    }

	}

	handle=get_next_commonhandle(&index, 0);

    }

    if (handle==NULL) {

	filehandle=insert_filehandle(session, 0, 0, result);

	if (filehandle) {

	    filehandle->handle.ino=(uint64_t) path;
	    filehandle->handle.flags|=COMMONHANDLE_FLAG_CREATE;

	}

    }

    unlock_commonhandles(&lock);
    return filehandle;

}

void complete_create_filehandle(struct filehandle_s *filehandle, dev_t dev, uint64_t ino, unsigned int fd)
{
    struct commonhandle_s *handle=&filehandle->handle;

    if (handle->flags & COMMONHANDLE_FLAG_CREATE) {
	struct simple_lock_s lock;

	/* rehash since the ino is available */

	writelock_commonhandles(&lock);

	remove_commonhandle_hash(handle);
	handle->dev=dev;
	handle->ino=ino;
	handle->fd=fd;
	handle->flags-=COMMONHANDLE_FLAG_CREATE;
	insert_commonhandle_hash(handle);

	unlock_commonhandles(&lock);

    }

}

/* insert a filehandle
    check for locking conflicts */

struct filehandle_s *start_insert_filehandle(struct ssh_session_s *session, dev_t dev, uint64_t ino, int (* grant_access)(struct filehandle_s *h, struct insert_filehandle_s *r), struct insert_filehandle_s *result)
{
    struct filehandle_s *filehandle=NULL;
    struct commonhandle_s *handle=NULL;
    unsigned int hashvalue=0;
    void *index=NULL;
    int lockflags=0;
    struct simple_lock_s lock;

    logoutput("start_insert_filehandle: dev %i ino %li", dev, (long) ino);

    hashvalue=calculate_ino_hash(ino);
    writelock_commonhandles(&lock);

    handle=get_next_commonhandle(&index, hashvalue);

    while (handle) {

	if (handle->dev!=dev || handle->ino!=ino || handle->type!=COMMONHANDLE_TYPE_FILE) {

	    handle=get_next_commonhandle(&index, hashvalue);
	    continue;

	}

	/* handle found: compare the different access/locks */

	filehandle=(struct filehandle_s *) ( ((char *) handle) - offsetof(struct filehandle_s, handle));
	if (grant_access(filehandle, result)==-1) break;
	handle=get_next_commonhandle(&index, hashvalue);
	filehandle=NULL;

    }

    if (filehandle==NULL) {

	/* no handle found: no conflict */
	filehandle=insert_filehandle(session, dev, ino, result);

    } else {

	filehandle=NULL;

    }

    unlock_commonhandles(&lock);
    return filehandle;

}

void remove_filehandle(struct filehandle_s **pfilehandle)
{
    struct filehandle_s *filehandle=*pfilehandle;
    struct simple_lock_s lock;

    writelock_commonhandles(&lock);
    remove_commonhandle_hash(&filehandle->handle);
    unlock_commonhandles(&lock);

    close_filehandle(&filehandle->handle);
    *pfilehandle=NULL;

}
