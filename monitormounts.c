/*
  2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>
#include <dirent.h>

#include <inttypes.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/mount.h>

#include <pthread.h>

#ifndef ENOATTR
#define ENOATTR ENODATA        /* No such attribute */
#endif

#include "logging.h"

#include "main.h"
#include "utils.h"
#include "pathinfo.h"
#include "beventloop.h"
#include "beventloop-xdata.h"

#include "workerthreads.h"

#include "mountinfo.h"
#include "mountinfo-monitor.h"
#include "monitormounts.h"

static struct bevent_xdata_s xdata;

static int update_mountinfo(unsigned long generation, struct mountentry_s *(*next) (void **index, unsigned long generation, unsigned char type))
{
    struct mountentry_s *entry=NULL;
    void *index=NULL;

    logoutput("update_mountinfo: generation %li", generation);

    entry=next(&index, generation, MOUNTLIST_ADDED);

    while (entry) {

	logoutput("update_mountinfo: found %i:%i %s", entry->major, entry->minor, entry->mountpoint);
        entry=next(&index, generation, MOUNTLIST_ADDED);

    }

}

unsigned char ignore_mountinfo(char *source, char *fs, char *path)
{

    /* do not ignore fuse */

    if (strcmp(fs, "fuse")==0 || strcmp(fs, "fuseblk")==0) return 0;

    if (strcmp(source, "proc")==0 || strcmp(source, "sys")==0 || strcmp(source, "dev")==0 || strcmp(source, "devpts")==0 ||
	strcmp(source, "cgroup")==0 || strcmp(source, "securityfs")==0 || strcmp(source, "pstore")==0 || strcmp(source, "systemd-1")==0 ||
	strcmp(source, "debugfs")==0 || strcmp(source, "hugetlbfs")==0 || strcmp(source, "configfs")==0) {

	return 1;

    }

    if (strlen(path)>4 && strncmp(path, "/dev/", 5)==0) return 1;
    if (strlen(path)>4 && strncmp(path, "/sys/", 5)==0) return 1;
    if (strlen(path)>5 && strncmp(path, "/proc/", 5)==0) return 1;

    return 0;

}

int add_mountinfo_watch(struct beventloop_s *loop, unsigned int *error)
{
    init_xdata(&xdata);
    if (! loop) loop=get_mainloop();

    if (open_mountmonitor(&xdata, error)==0) {

	set_updatefunc_mountmonitor(update_mountinfo);
	set_ignorefunc_mountmonitor(ignore_mountinfo);
	set_threadsqueue_mountmonitor(NULL);

	if (add_to_beventloop(xdata.fd, EPOLLPRI, xdata.callback, NULL, &xdata, loop)) {

    	    logoutput("add_mountinfo_watch: mountinfo fd %i added to eventloop", xdata.fd);

	    /* read the mountinfo to initialize */
	    (* xdata.callback)(0, NULL, 0);
	    return 0;

	} else {

	    logoutput("add_mountinfo_watch: unable to add mountinfo fd %i to eventloop", xdata.fd);
	    *error=EIO;

	}

    } else {

	logoutput("add_mountinfo_watch: unable to open mountmonitor");

    }

    return -1;

}
