/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"

#include "sftp-common-protocol.h"
#include "sftp-supported.h"
#include "filehandle.h"
#include "sftp-handle.h"
#include "sftp-reply.h"
#include "sftp-op.h"
#include "sftp-attributes-read.h"
#include "sftp-path.h"

#define _SFTP_OPENACCESS_READ				( ACE4_READ_DATA | ACE4_READ_ATTRIBUTES )
#define _SFTP_OPENACCESS_WRITE				( ACE4_WRITE_DATA | ACE4_WRITE_ATTRIBUTES )
#define _SFTP_OPENACCESS_READWRITE			( ACE4_READ_DATA | ACE4_READ_ATTRIBUTES | ACE4_WRITE_DATA | ACE4_WRITE_ATTRIBUTES )
#define _SFTP_OPENACCESS_APPEND				( ACE4_APPEND_DATA )

/* translate the access and flags sftp parameters into posix
    do also some sane checking (write access is required for append etc) */

static int translate_sftp2posix(unsigned int access, unsigned int flags, unsigned int *posix, unsigned int *error)
{
    int result=0;

    if ((access & _SFTP_OPENACCESS_READ) && (access & _SFTP_OPENACCESS_WRITE)) {

	*posix|=O_RDWR;

	if (access & _SFTP_OPENACCESS_APPEND) {

	    if (flags & (SSH_FXF_APPEND_DATA | SSH_FXF_APPEND_DATA_ATOMIC)) {

		*posix|=O_APPEND;

	    } else {

		*error=EINVAL;
		goto error;

	    }

	}

    } else if (access & _SFTP_OPENACCESS_WRITE) {

	*posix|=O_WRONLY;

	if (access & _SFTP_OPENACCESS_APPEND) {

	    if (flags & (SSH_FXF_APPEND_DATA | SSH_FXF_APPEND_DATA_ATOMIC)) {

		*posix|=O_APPEND;

	    } else {

		*error=EINVAL;
		goto error;

	    }

	}

    } else if (access & _SFTP_OPENACCESS_READ) {
	unsigned int openflags=flags & SSH_FXF_ACCESS_DISPOSITION;

	*posix|=O_RDONLY;

	if (access & _SFTP_OPENACCESS_APPEND) {

	    *error=EINVAL;
	    goto error;

	}

	if (openflags != SSH_FXF_OPEN_EXISTING) {

	    *error=EINVAL;
	    goto error;

	}

    } else {

	*error=EINVAL;
	goto error;

    }

    if (flags & SSH_FXF_ACCESS_DISPOSITION) {

	if (flags & SSH_FXF_CREATE_NEW) {

	    *posix |= (O_EXCL | O_CREAT);

	} else if (flags & SSH_FXF_CREATE_TRUNCATE) {

	    *posix |= (O_CREAT | O_TRUNC);

	} else if (flags & SSH_FXF_OPEN_EXISTING) {

	    /* no additional posix flags ... */

	} else if (flags & SSH_FXF_OPEN_OR_CREATE) {

	    *posix |= O_CREAT;

	} else if (flags & SSH_FXF_TRUNCATE_EXISTING) {

	    *posix |= O_TRUNC;

	}

    } else {

	*error=EINVAL;

    }

    out:

    logoutput("translate_sftp2posix: access %i flags %i posix %i", access, flags, *posix);

    return 0;

    error:

    logoutput("translate_sftp2posix: received incompatible/incomplete open access and flags");
    return -1;

}

static unsigned int translate_open_error(unsigned int error)
{
    unsigned int status=SSH_FX_FAILURE;

    if (error==ENOENT) {

	status=SSH_FX_NO_SUCH_FILE;

    } else if (error==ENOTDIR) {

	status=SSH_FX_NO_SUCH_PATH;

    } else if (error==EACCES) {

	status=SSH_FX_PERMISSION_DENIED;

    } else if (error==EEXIST) {

	status=SSH_FX_FILE_ALREADY_EXISTS;

    }

    return status;

}

static int send_sftp_handle(struct ssh_session_s *session, struct sftp_header_s *header, struct filehandle_s *filehandle)
{
    char bytes[16];
    write_sftp_filehandle(filehandle->handle.dev, filehandle->handle.ino, filehandle->handle.fd, bytes);
    return reply_sftp_handle(session, header->id, bytes, 16);
}

static void sftp_op_open_new(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer, unsigned int left, char *path, unsigned int sftp_access, unsigned int sftp_flags, unsigned int posix)
{
    struct ssh_user_s *user=&session->user;
    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
    unsigned int status=0;
    unsigned int valid=0;
    unsigned int error=0;
    struct stat st;
    uid_t uid_keep=0;
    gid_t gid_keep=0;
    struct sftp_attr_s attr;
    struct filehandle_s *handle=NULL;
    struct insert_filehandle_s result;
    int fd=0;

    memset(&result, 0, sizeof(struct insert_filehandle_s));
    memset(&attr, 0, sizeof(struct sftp_attr_s));
    memset(&st, 0, sizeof(struct stat));

    st.st_uid=user->pwd.pw_uid; /* sane default */
    st.st_gid=user->pwd.pw_gid; /* sane default */
    st.st_mode=S_IFREG | (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH); /* default file with perm 0666 */
    st.st_size=0; /* default zero size */

    uid_keep=setfsuid(user->pwd.pw_uid);
    gid_keep=setfsgid(user->pwd.pw_gid);

    /* read the attr */

    if (read_attributes_v06(sftp, buffer, left, &attr)==0) {

	status=SSH_FX_INVALID_PARAMETER;
	logoutput("sftp_op_open_new: error reading attributes");
	goto error;

    }

    if (attr.valid[SFTP_ATTR_INDEX_USERGROUP]==1) {

	/* check the user and/or group are reckognized
	    and if set to the connecting user they do not have to be set
	    since they are set automatically cause setfsuid and setfsgid are used */

	if (attr.flags & SFTP_ATTR_FLAG_USERNOTFOUND) {

	    status=SSH_FX_OWNER_INVALID;
	    logoutput("sftp_op_open_new: error user not found");
	    goto error;

	} else if (attr.flags & SFTP_ATTR_FLAG_VALIDUSER) {

	    if (attr.uid==user->pwd.pw_uid) attr.flags -= SFTP_ATTR_FLAG_VALIDUSER;

	}

	if (attr.flags & SFTP_ATTR_FLAG_GROUPNOTFOUND) {

	    status=SSH_FX_GROUP_INVALID;
	    logoutput("sftp_op_open_new: error group not found");
	    goto error;

	} else if (attr.flags & SFTP_ATTR_FLAG_VALIDGROUP) {

	    if (attr.gid==user->pwd.pw_gid) attr.flags -= SFTP_ATTR_FLAG_VALIDGROUP;

	}

	if (!(attr.flags & (SFTP_ATTR_FLAG_VALIDGROUP | SFTP_ATTR_FLAG_VALIDUSER))) attr.valid[SFTP_ATTR_INDEX_USERGROUP]=0;

    }

    if (attr.valid[SFTP_ATTR_INDEX_PERMISSIONS]) {

	st.st_mode=attr.type | attr.permissions;
	attr.valid[SFTP_ATTR_INDEX_PERMISSIONS]=0; /* not needed futher afterwards by the setstat */

	if (! S_ISREG(st.st_mode)) {

	    status=(st.st_mode>0) ? SSH_FX_FILE_IS_A_DIRECTORY : SSH_FX_PERMISSION_DENIED;
	    logoutput("sftp_op_open_new: error type not file (%i)", (int) st.st_mode);
	    goto error;

	}

    }

    result.server.sftp.access=sftp_access;
    result.server.sftp.flags=sftp_flags;

    handle=start_create_filehandle(session, path, &result);

    if (handle==NULL) {

	status=result.status; /* possibly additional information in result about locking for example */
	goto error;

    }

    fd=open(path, posix, st.st_mode);
    error=errno;

    if (fd>0) {
	struct stat test;

	/* when size is zero ignore it */
	if (attr.valid[SFTP_ATTR_INDEX_SIZE]==1 && attr.size==0) attr.valid[SFTP_ATTR_INDEX_SIZE]=0;

	if (attr.valid[SFTP_ATTR_INDEX_PERMISSIONS] | attr.valid[SFTP_ATTR_INDEX_SIZE] | attr.valid[SFTP_ATTR_INDEX_USERGROUP] | 
	    attr.valid[SFTP_ATTR_INDEX_ATIME] | attr.valid[SFTP_ATTR_INDEX_MTIME]) {

	    /* set stat values: some protection to make it atomic... TODO */

	    if (_setstat_fd(fd, &attr, &error)==-1) {

		/* remove the file: protection? */

		logoutput("sftp_op_open: error setting stat values");
		close(fd);
		unlink(path);
		status=SSH_FX_FAILURE;
		goto error;

	    }

	}

	memset(&test, 0, sizeof(struct stat));

	if (fstat(fd, &test)==0) {

	    complete_create_filehandle(handle, test.st_dev, test.st_ino, fd);

	} else {

	    error=errno;
	    status=SSH_FX_FAILURE;
	    status=translate_open_error(error);
	    goto error;

	}

    } else {

	status=SSH_FX_FAILURE;
	if (error==0) error=EIO;
	status=translate_open_error(error);
	goto error;

    }

    uid_keep=setfsuid(uid_keep);
    gid_keep=setfsgid(gid_keep);

    if (send_sftp_handle(session, header, handle)==-1) {

	logoutput("sftp_op_open_new: error sending handle reply");

    }

    return;

    error:

    uid_keep=setfsuid(uid_keep);
    gid_keep=setfsgid(gid_keep);
    if (handle) remove_filehandle(&handle);

    logoutput("sftp_op_error_new: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

void sftp_op_open_existing(struct ssh_session_s *session, struct sftp_header_s *header, char *path, unsigned int sftp_access, unsigned int sftp_flags, unsigned int posix)
{
    struct ssh_user_s *user=&session->user;
    unsigned int status=0;
    unsigned int valid=0;
    unsigned int error=0;
    struct stat st;
    uid_t uid_keep=0;
    gid_t gid_keep=0;
    int fd=0;
    struct filehandle_s *filehandle=NULL;
    struct insert_filehandle_s result;

    memset(&result, 0, sizeof(struct insert_filehandle_s));
    memset(&st, 0, sizeof(struct stat));

    uid_keep=setfsuid(user->pwd.pw_uid);
    gid_keep=setfsgid(user->pwd.pw_gid);

    if (lstat(path, &st)==-1) {

	error=errno;

	if (error==ENOENT) {

	    status=SSH_FX_NO_SUCH_FILE;

	} else {

	    status=SSH_FX_FAILURE;

	}

	goto error;

    }

    if (S_ISDIR(st.st_mode)) {

	status=SSH_FX_FILE_IS_A_DIRECTORY;
	goto error;

    }

    result.server.sftp.access=sftp_access;
    result.server.sftp.flags=sftp_flags;

    filehandle=start_insert_filehandle_sftp(session, st.st_dev, st.st_ino, &result);

    if (filehandle==NULL) {

	status=result.status; /* possibly additional information in result about locking for example */
	goto error;

    }

    logoutput("sftp_op_open_existing: open path %s", path);

    fd=open(path, posix);
    error=errno;

    if (fd==-1) {

	status=translate_open_error(error);
	goto error;

    }

    filehandle->handle.fd=(unsigned int) fd;

    logoutput("sftp_op_open_existing: fd %i", fd);

    if (send_sftp_handle(session, header, filehandle)==-1) {

	logoutput("sftp_op_open_existing: error sending handle reply");

    }

    uid_keep=setfsuid(uid_keep);
    gid_keep=setfsgid(gid_keep);
    return;

    error:

    uid_keep=setfsuid(uid_keep);
    gid_keep=setfsgid(gid_keep);
    if (filehandle) remove_filehandle(&filehandle);

    logoutput("sftp_op_error_existing: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_OPEN
    message has the form:
    - byte 				SSH_FXP_OPEN
    - uint32				id
    - string				path
    - uint32				access
    - uint32				flags
    - ATTRS				attrs
    */

void sftp_op_open(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)

{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_open (%i)", (int) gettid());

    /* message should at least have 4 bytes for the path string, and 4 for the flags
	note an empty path is possible */

    if (header->len>=12) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	/* sftp packet size is at least:
	    - 4 + len ... path (len maybe zero)
	    - 4       ... access
	    - 4       ... flags
	    - x       ... ATTR (optional, only required when file is created) */

	if (header->len >= len + 12) {
	    struct ssh_user_s *user=&session->user;
	    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
	    unsigned int size=get_fullpath_len(user, len, &buffer[pos]); /* get size of buffer for path */
	    char path[size+1];
	    unsigned int sftp_access=0;
	    unsigned int sftp_flags=0;
	    unsigned int posix=0;
	    unsigned int error=0;

	    get_fullpath(user, len, &buffer[pos], path);
	    pos+=len;
	    sftp_access=get_uint32(&buffer[pos]);
	    pos+=4;
	    sftp_flags=get_uint32(&buffer[pos]);
	    pos+=4;

	    if (translate_sftp2posix(sftp_access, sftp_flags, &posix, &error)==-1) {

		status=SSH_FX_PERMISSION_DENIED;
		logoutput("sftp_op_open: error %i translating sftp to posix (%s)", error, strerror(error));
		goto error;

	    }

	    /* TODO: */

	    if (posix & O_CREAT) {

		sftp_op_open_new(session, header, &buffer[pos], header->len - pos, path, sftp_access, sftp_flags, posix);

	    } else {

		sftp_op_open_existing(session, header, path, sftp_access, sftp_flags, posix);

	    }

	    return;

	}

    }

    error:

    logoutput("sftp_op_error: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_READ
    message has the form:
    - byte 				SSH_FXP_READ
    - uint32				id
    - string				handle
    - uint64				offset
    - uint32				length
    */

void sftp_op_read(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_read (%i)", (int) gettid());

    /* handle is 16 bytes long, so message is 4 + 16 + 8 + 4 = 32 bytes */

    if (header->len==32) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (len==SFTP_HANDLE_SIZE) {
	    unsigned int error=0;
	    struct filehandle_s *filehandle=find_sftp_filehandle(&buffer[pos], session, &error);
	    uint64_t offset=0;
	    uint32_t size=0;

	    pos+=16;

	    offset=get_uint64(&buffer[pos]);
	    pos+=8;
	    size=get_uint32(&buffer[pos]);
	    pos+=4;

	    /* TODO: handle max-read-size (2K?) */

	    if (filehandle) {
		char data[size];
		int count=0;
		unsigned int error=0;

		count=pread(filehandle->handle.fd, data, size, offset);
		error=errno;

		if (count>=0) {

		    logoutput("sftp_op_read: size %i off %i read %i bytes ", (int) size, (int) offset, count);

		    if (reply_sftp_data(session, header->id, data, count, 0)==-1) {

			logoutput("sftp_op_read: error sending data");

		    }

		    //decrease_filehandle(&filehandle);
		    return;

		}

		logoutput("sftp_op_read: error %i reading data (%s)", error, strerror(error));
		status=SSH_FX_FAILURE;

		if (error==EBADF) {

		    /* check the posix open value */
		    status=SSH_FX_INVALID_PARAMETER;

		}

		//decrease_filehandle(&filehandle);

	    } else {

		logoutput("sftp_op_read: handle not found");

		if (error==EPERM) {

		    /* serious error: client wants to use a handle he has no permissions for */

		    logoutput("sftp_op_read: client has no permissions to use handle");
		    goto disconnect;

		}

		status=SSH_FX_INVALID_HANDLE;

	    }

	}

    }

    logoutput("sftp_op_read: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

/* SSH_FXP_WRITE
    message has the form:
    - byte 				SSH_FXP_WRITE
    - uint32				id
    - string				handle
    - uint64				offset
    - string				data
    */

void sftp_op_write(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_write (%i)", (int) gettid());

    /* handle is 16 bytes long, so message is 4 + 16 + 8 + 4 + size data */

    if (header->len > 16 + SFTP_HANDLE_SIZE) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (len==SFTP_HANDLE_SIZE) {
	    unsigned int error=0;
	    struct filehandle_s *filehandle=find_sftp_filehandle(&buffer[pos], session, &error);
	    uint64_t offset=0;
	    uint32_t size=0;

	    pos+=16;

	    offset=get_uint64(&buffer[pos]);
	    pos+=8;
	    size=get_uint32(&buffer[pos]);
	    pos+=4;

	    if (size==0) {

		status=SSH_FX_INVALID_PARAMETER;
		// if (filehandle) decrease_filehandle(&filehandle);
		goto error;

	    }

	    /* TODO: handle max write size ? */

	    if (filehandle) {
		int count=0;

		count=pwrite(filehandle->handle.fd, &buffer[pos], size, offset);
		error=errno;

		if (count>0) {

		    reply_sftp_status_simple(session, header->id, SSH_FX_OK);
		    //decrease_filehandle(&filehandle);
		    return;

		}

		if (error==0) error=EIO;
		logoutput("sftp_op_write: error %i writing %i bytes (%s)", error, size, strerror(error));

		if (error==EBADF) {

		    /* check the underlying fs: is it read only? */
		    status=SSH_FX_PERMISSION_DENIED;

		} else if (error==EDQUOT) {

		    status=SSH_FX_QUOTA_EXCEEDED;

		} else if (error==ENOSPC) {

		    status=SSH_FX_NO_SPACE_ON_FILESYSTEM;

		} else if (error==EINVAL) {

		    /* check the underlying fs: is it read only? */
		    status=SSH_FX_WRITE_PROTECT;

		} else if (error==EFBIG) {

		    /* check here the file was openen without O_APPEND */
		    status=SSH_FX_INVALID_PARAMETER;

		} else {

		    /* errors like write */
		    status=SSH_FX_FAILURE;

		}

		//decrease_filehandle(&filehandle);

	    } else {

		if (error==EPERM) {

		    /* serious error: client wants to use a handle he has no permissions for */

		    logoutput("sftp_op_write: client has no permissions to use handle");
		    goto disconnect;

		}

		status=SSH_FX_INVALID_HANDLE;

	    }

	}

    }

    error:

    logoutput("sftp_op_write: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

/* SSH_FXP_CLOSE
    message has the form:
    - byte 				SSH_FXP_CLOSE
    - uint32				id
    - string				handle
    */

void sftp_op_close(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_close (%i)", (int) gettid());

    /* handle is 16 bytes long, so message is 4 + 16 = 20 */

    if (header->len==4 + SFTP_HANDLE_SIZE) {
	unsigned int len=0;

	len=get_uint32(&buffer[0]);

	if (len==SFTP_HANDLE_SIZE) {
	    unsigned int error=0;

	    if (release_sftp_handle(&buffer[4], session, &error)==0) {

		reply_sftp_status_simple(session, header->id, SSH_FX_OK);
		return;

	    }

	    if (error==EPERM) {

		/* serious error: client wants to use a handle he has no permissions for */

		logoutput("sftp_op_close: client has no permissions to use handle");
		goto disconnect;

	    }

	    if (error==ENOENT || error==EINVAL) {

		status=SSH_FX_INVALID_HANDLE;

	    }

	}

    }

    logoutput("sftp_op_close: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}
