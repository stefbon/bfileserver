/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "sftp-common-protocol.h"
#include "sftp-supported.h"
#include "sftp-attributes-write.h"

#include "sftp-reply.h"

#include "filehandle.h"
#include "sftp-handle.h"
#include "sftp-path.h"

/* SSH_FXP_REMOVE/
    message has the form:
    - byte 				SSH_FXP_REMOVE
    - uint32				id
    - string				path
    */

void sftp_op_remove(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    /* message should at least have 4 bytes for the path string, and 4 for the flags
	note an empty path is possible */

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;
	unsigned int valid=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (len + 8 == header->len) {
	    struct ssh_user_s *user=&session->user;
	    unsigned int pathlen=get_fullpath_len(user, len, &buffer[pos]);
	    uid_t uid_keep=setfsuid(user->pwd.pw_uid);
	    gid_t gid_keep=setfsgid(user->pwd.pw_gid);
	    int result=0;
	    unsigned int error=0;
	    char path[pathlen];

	    get_fullpath(user, len, &buffer[pos], path);

	    logoutput("sftp_op_remove: path %s", path);

	    if (unlink(path)==0) {

		setfsuid(uid_keep);
		setfsgid(gid_keep);

		reply_sftp_status_simple(session, header->id, SSH_FX_OK);
		return;

	    } else {

		setfsuid(uid_keep);
		setfsgid(gid_keep);

		status=SSH_FX_FAILURE;

		if (errno==ENOENT) {

		    status=SSH_FX_NO_SUCH_FILE;

		} else if (errno==ENOTDIR) {

		    status=SSH_FX_NO_SUCH_PATH;

		} else if (errno==EISDIR) {

		    status=SSH_FX_FILE_IS_A_DIRECTORY;

		} else if (errno==EACCES) {

		    status=SSH_FX_PERMISSION_DENIED;

		}

	    }

	}

    }

    logoutput("sftp_op_remove: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_RMDIR/
    message has the form:
    - byte 				SSH_FXP_REMOVE
    - uint32				id
    - string				path
    */

void sftp_op_rmdir(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    /* message should at least have 4 bytes for the path string, and 4 for the flags
	note an empty path is possible */

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;
	unsigned int valid=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (len + 8 == header->len) {
	    struct ssh_user_s *user=&session->user;
	    unsigned int pathlen=get_fullpath_len(user, len, &buffer[pos]);
	    uid_t uid_keep=setfsuid(user->pwd.pw_uid);
	    gid_t gid_keep=setfsgid(user->pwd.pw_gid);
	    int result=0;
	    unsigned int error=0;
	    char path[pathlen];

	    get_fullpath(user, len, &buffer[pos], path);

	    logoutput("sftp_op_rmdir: path %s", path);

	    if (rmdir(path)==0) {

		setfsuid(uid_keep);
		setfsgid(gid_keep);

		reply_sftp_status_simple(session, header->id, SSH_FX_OK);
		return;

	    } else {

		setfsuid(uid_keep);
		setfsgid(gid_keep);

		status=SSH_FX_FAILURE;

		if (errno==ENOENT) {

		    status=SSH_FX_NO_SUCH_PATH;

		} else if (errno==ENOTDIR) {

		    status=SSH_FX_NOT_A_DIRECTORY;

		} else if (errno==ENOTEMPTY) {

		    status=SSH_FX_DIR_NOT_EMPTY;

		} else if (errno==EACCES) {

		    status=SSH_FX_PERMISSION_DENIED;

		}

	    }

	}

    }

    logoutput("sftp_op_rmdir: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

