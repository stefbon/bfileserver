/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <pwd.h>
#include <grp.h>

#include <linux/kdev_t.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-hash.h"
#include "localsocket.h"
#include "clientsession.h"

#include "ssh-utils.h"

#include "commonhandle.h"
#include "filehandle.h"
#include "dirhandle.h"

#include "sftp-common-protocol.h"

/* manage handles for sftp
    every handle has the form:
    dev:inode:fd

    - 4 bytes				dev
    - 8 bytes				inode
    - 4 bytes				fd
    - 1 byte				type:
    ---------
    17 bytes

    - encrypt and decrypt?

*/

/* get the filehandle from the handle string used in sftp */

struct filehandle_s *find_sftp_filehandle(char *buffer, struct ssh_session_s *session, unsigned int *error)
{
    unsigned char pos=0;
    dev_t dev=0;
    uint64_t ino=0;
    unsigned int fd=0;
    unsigned char type=0;

    /* interpret the buffer */

    dev=get_uint32(&buffer[pos]);
    pos+=4;
    ino=get_uint64(&buffer[pos]);
    pos+=8;
    fd=get_uint32(&buffer[pos]);
    pos+=4;
    type=(unsigned char) buffer[pos];

    if (type==COMMONHANDLE_TYPE_FILE) return find_filehandle(session, dev, ino, fd, error);

    return NULL;

}

static int grant_access_sftp(struct filehandle_s *handle, struct insert_filehandle_s *result)
{

    if (result->server.sftp.flags & SSH_FXF_BLOCK_READ) {

	if ((* handle->get_lock_flags)(handle, "read")==-1) {

	    result->status=SSH_FX_LOCK_CONFLICT;
	    // result->info.lock.uid=... get uid from handle->ptr
	    return -1;

	}

    }

    if (result->server.sftp.flags & SSH_FXF_BLOCK_WRITE) {

	if ((* handle->get_lock_flags)(handle, "write")==-1) {

	    result->status=SSH_FX_LOCK_CONFLICT;
	    // result->info.lock.uid=... get uid from handle->ptr
	    return -1;

	}

    }

    if (result->server.sftp.flags & SSH_FXF_BLOCK_DELETE) {

	if ((* handle->get_lock_flags)(handle, "delete")==-1) {

	    result->status=SSH_FX_LOCK_CONFLICT;
	    // result->info.lock.uid=... get uid from handle->ptr
	    return -1;

	}

    }

    return 0;

}

/* insert a global handle
    check for locking conflicts */

struct filehandle_s *start_insert_filehandle_sftp(struct ssh_session_s *session, dev_t dev, uint64_t ino, struct insert_filehandle_s *result)
{
    return start_insert_filehandle(session, dev, ino, grant_access_sftp, result);
}

/* construct a handle to be used for fs operations */

static unsigned char write_sftp_handle(dev_t dev, uint64_t ino, unsigned int fd, unsigned char type, char *buffer)
{
    unsigned char pos=0;

    if (buffer) {

	store_uint32(&buffer[pos], dev);
	pos+=4;
	store_uint64(&buffer[pos], ino);
	pos+=8;
	store_uint32(&buffer[pos], fd);
	pos+=4;
	buffer[pos]=type;

    } else {

	pos=17;

    }

    return pos;

}

unsigned char write_sftp_filehandle(dev_t dev, uint64_t ino, unsigned int fd, char *buffer)
{
    return write_sftp_handle(dev, ino, fd, COMMONHANDLE_TYPE_FILE, buffer);
}

int release_sftp_handle(char *buffer, struct ssh_session_s *session, unsigned int *error)
{
    unsigned char pos=0;
    dev_t dev=0;
    uint64_t ino=0;
    unsigned int fd=0;
    unsigned char type=0;

    /* interpret the buffer */

    dev=get_uint32(&buffer[pos]);
    pos+=4;
    ino=get_uint64(&buffer[pos]);
    pos+=8;
    fd=get_uint32(&buffer[pos]);
    pos+=4;
    type=(unsigned char) buffer[pos];

    return close_commonhandle(session, dev, ino, fd, type, error);

}

int release_sftp_filehandle(char *buffer, struct ssh_session_s *session, unsigned int *error)
{
    return release_sftp_handle(buffer, session, error);
}

struct dirhandle_s *insert_sftp_dirhandle(struct ssh_session_s *session, dev_t dev, uint64_t ino, unsigned int *error)
{
    return insert_dirhandle(session, dev, ino, error);
}

struct dirhandle_s *find_sftp_dirhandle(char *buffer, struct ssh_session_s *session, unsigned int *error)
{
    unsigned char pos=0;
    dev_t dev=0;
    uint64_t ino=0;
    unsigned int fd=0;
    unsigned char type=0;

    logoutput("find_sftp_dirhandle");

    /* interpret the buffer */

    dev=get_uint32(&buffer[pos]);
    pos+=4;
    ino=get_uint64(&buffer[pos]);
    pos+=8;
    fd=get_uint32(&buffer[pos]);
    pos+=4;
    type=(unsigned char) buffer[pos];

    if (type==COMMONHANDLE_TYPE_DIR) return find_dirhandle(session, dev, ino, fd, error);

    return NULL;

}

/* construct a handle to be used for fs operations
    fill buffer
    buffer is 16 bytes*/

unsigned char write_sftp_dirhandle(dev_t dev, uint64_t ino, unsigned int fd, char *buffer)
{
    return write_sftp_handle(dev, ino, fd, COMMONHANDLE_TYPE_DIR, buffer);
}

int release_sftp_dirhandle(char *buffer, struct ssh_session_s *session, unsigned int *error)
{
    return release_sftp_handle(buffer, session, error);
}

struct commonhandle_s *find_sftp_commonhandle(char *buffer, struct ssh_session_s *session)
{
    unsigned char pos=0;
    dev_t dev=0;
    uint64_t ino=0;
    unsigned int fd=0;
    unsigned char type=0;
    unsigned int error=0;

    dev=get_uint32(&buffer[pos]);
    pos+=4;
    ino=get_uint64(&buffer[pos]);
    pos+=8;
    fd=get_uint32(&buffer[pos]);
    pos+=4;
    type=(unsigned char) buffer[pos];

    return find_commonhandle(session, dev, ino, fd, &error);
}
