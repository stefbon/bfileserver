/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>
#include <sys/statvfs.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"

#include "filehandle.h"

#include "shared/sftp-common-protocol.h"
#include "shared/sftp-supported.h"
#include "shared/sftp-handle.h"
#include "shared/sftp-reply.h"
#include "shared/sftp-attributes-read.h"
#include "shared/sftp-path.h"

/* SSH_FXP_EXTENDED - mapextension@bononline.nl
    message has the form:
    - string				"mapextension@bononline.nl"
    - string				name of extension to map
    */

void sftp_op_mapextension(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_extended - mapextension$bononline.nl (%i)", (int) gettid());

    if (session->status.type != SSH_SUBSYSTEM_TYPE_SFTP && session->status.type != SSH_SUBSYSTEM_TYPE_BACKUP) {

	status=SSH_FX_OP_UNSUPPORTED;
	goto out;

    }

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (header->len==4 + len) {
	    struct sftp_extension_s *extension=NULL;
	    unsigned char op2use=0;

	    extension=(session->status.type == SSH_SUBSYSTEM_TYPE_BACKUP ) ? get_next_backup_extension(extension) : get_next_sftp_extension(extension);

	    while (extension && extension->len>0) {

		if (extension->len==len && strncmp(&buffer[pos], extension->name, len)==0) break;
		extension=(session->status.type == SSH_SUBSYSTEM_TYPE_BACKUP ) ? get_next_backup_extension(extension) : get_next_sftp_extension(extension);

	    }

	    if (extension && extension->len>0) {

		if (session->status.type == SSH_SUBSYSTEM_TYPE_SFTP) {
		    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;

		    pthread_mutex_lock(&sftp->mutex);

		    if (sftp->extended_op_start < SFTP_EXTENDED_OP_MAX) {

			op2use=sftp->extended_op_start;
			sftp->cb[op2use] = extension->cb;
			sftp->extended_op_start++;

		    } else {

			status=SSH_FX_FAILURE;

		    }

		    pthread_mutex_unlock(&sftp->mutex);

		} else if (session->status.type == SSH_SUBSYSTEM_TYPE_BACKUP) {
		    struct backup_subsystem_s *backup=&session->subsystem.backup;

		    pthread_mutex_lock(&backup->mutex);

		    if (backup->extended_op_start < SFTP_EXTENDED_OP_MAX) {

			op2use=backup->extended_op_start;
			backup->cb[op2use] = extension->cb;
			backup->extended_op_start++;

		    } else {

			status=SSH_FX_FAILURE;

		    }

		    pthread_mutex_unlock(&backup->mutex);

		}

		if (op2use>0) {
		    char byte[1];

		    byte[0]=op2use;

		    reply_sftp_extension(session, header->id, byte, 1);
		    return;

		}

	    } else {

		status=SSH_FX_INVALID_PARAMETER;

	    }

	}

    }

    out:

    logoutput("sftp_op_mapextension: status %i", status);
    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

