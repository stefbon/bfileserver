/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "sftp-common-protocol.h"
#include "sftp-supported.h"

/*
    an attrs response
	- byte 			SSH_FXP_ATTRS
	- uint32		id
	- ATTRS			attrs

    here the attributes are presented as buffer (not string)
    construction of this buffer is done upstream */

int reply_sftp_attrs(struct ssh_session_s *session, unsigned int id, char *buffer, unsigned int len)
{
    char data[len+9];
    unsigned int pos=4;
    unsigned int error=0;

    data[pos]=SSH_FXP_ATTRS;
    pos++;

    store_uint32(&data[pos], id);
    pos+=4;

    memcpy(&data[pos], buffer, len);
    pos+=len;

    store_uint32(&data[0], pos-4);

    return send_session_data(session, data, pos, &error);

}
