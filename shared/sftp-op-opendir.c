/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>
#include <sys/syscall.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "simple-locking.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "sftp-common-protocol.h"
#include "sftp-supported.h"
#include "commonhandle.h"
#include "filehandle.h"
#include "dirhandle.h"
#include "sftp-handle.h"
#include "sftp-reply.h"
#include "sftp-attributes-write.h"
#include "sftp-path.h"

#define SFTP_READDIR_NAMES_SIZE		1024

static int send_sftp_handle(struct ssh_session_s *session, struct sftp_header_s *header, struct dirhandle_s *dirhandle)
{
    char bytes[SFTP_HANDLE_SIZE];
    logoutput("send_sftp_handle");
    write_sftp_dirhandle(dirhandle->handle.dev, dirhandle->handle.ino, dirhandle->handle.fd, bytes);
    return reply_sftp_handle(session, header->id, bytes, SFTP_HANDLE_SIZE);
}

static void _sftp_op_opendir(struct ssh_session_s *session, struct sftp_header_s *header, char *path)
{
    struct ssh_user_s *user=&session->user;
    unsigned int status=0;
    unsigned int error=0;
    struct stat st;
    uid_t uid_keep=0;
    gid_t gid_keep=0;
    struct dirhandle_s *dirhandle=NULL;
    int fd=-1;

    logoutput("_sftp_op_opendir: path %s", path);

    memset(&st, 0, sizeof(struct stat));

    uid_keep=setfsuid(user->pwd.pw_uid);
    gid_keep=setfsgid(user->pwd.pw_gid);

    if (lstat(path, &st)==-1) {

	error=errno;

	if (error==ENOENT) {

	    status=SSH_FX_NO_SUCH_FILE;

	} else {

	    status=SSH_FX_FAILURE;

	}

	goto error;

    }

    if (! S_ISDIR(st.st_mode)) {

	status=SSH_FX_NOT_A_DIRECTORY;
	goto error;

    }

    logoutput("_sftp_op_opendir: insert dirhandle");

    dirhandle=insert_sftp_dirhandle(session, st.st_dev, st.st_ino, &error);

    if (dirhandle==NULL) {

	status=SSH_FX_FAILURE;
	goto error;

    }

    logoutput("_sftp_op_opendir: open");

    fd=open(path, O_DIRECTORY);
    error=errno;

    logoutput("_sftp_op_opendir: open post 01 fd %i", fd);

    if (fd==-1) {

	logoutput("_sftp_op_opendir: error %i open (%s)", error, strerror(error));
	status=SSH_FX_FAILURE;
	goto error;

    }

    logoutput("_sftp_op_opendir: open post 02");
    dirhandle->handle.fd=(unsigned int) fd;
    logoutput("_sftp_op_opendir: open post 03");
    dirhandle->size=SFTP_READDIR_NAMES_SIZE;

    logoutput("_sftp_op_opendir: send sftp handle");

    if (send_sftp_handle(session, header, dirhandle)==-1) {

	logoutput("_sftp_op_opendir: error sending handle reply");

    }

    uid_keep=setfsuid(uid_keep);
    gid_keep=setfsgid(gid_keep);
    return;

    error:

    logoutput("_sftp_op_opendir: status %i", status);

    uid_keep=setfsuid(uid_keep);
    gid_keep=setfsgid(gid_keep);
    if (dirhandle) release_dirhandle(&dirhandle);

    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_OPENDIR
    message has the form:
    - byte 				SSH_FXP_OPEN
    - uint32				id
    - string				path
    */

void sftp_op_opendir(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_opendir (%i)", (int) gettid());

    /* message should at least have 4 bytes for the path string
	note an empty path is possible */

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	/* sftp packet size is at least:
	    - 4 + len ... path (len maybe zero) */

	if (header->len == len + 4) {
	    struct ssh_user_s *user=&session->user;
	    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
	    unsigned int size=get_fullpath_len(user, len, &buffer[pos]); /* get size of buffer for path */
	    char path[size+1];
	    unsigned int error=0;

	    get_fullpath(user, len, &buffer[pos], path);
	    pos+=len;

	    _sftp_op_opendir(session, header, path);
	    return;

	}

    }

    error:

    logoutput("sftp_op_opendir: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

struct linux_dirent64 {
    ino64_t			d_ino;
    off64_t			d_off;
    unsigned short		d_reclen;
    unsigned char		d_type;
    char			d_name[];
};

/* SSH_FXP_READ
    message has the form:
    - byte 				SSH_FXP_READ
    - uint32				id
    - string				handle
    */

void sftp_op_readdir(struct ssh_session_s *session, struct sftp_header_s *header, char *data)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;
    unsigned int valid=SSH_FILEXFER_ATTR_SIZE | SSH_FILEXFER_ATTR_UIDGID | SSH_FILEXFER_ATTR_PERMISSIONS | SSH_FILEXFER_ATTR_ACCESSTIME | SSH_FILEXFER_ATTR_MODIFYTIME | SSH_FILEXFER_ATTR_SUBSECOND_TIMES | SSH_FILEXFER_ATTR_CTIME;

    logoutput("sftp_op_readdir (%i)", (int) gettid());

    /* handle is 16 bytes long, so message is 4 + 16 = 20 bytes */

    if (header->len == 4 + SFTP_HANDLE_SIZE) {
	unsigned int len=0;

	len=get_uint32(&data[0]);

	if (len==SFTP_HANDLE_SIZE) {
	    unsigned int error=0;
	    struct dirhandle_s *dirhandle=find_sftp_dirhandle(&data[4], session, &error);

	    if (dirhandle) {
		unsigned int error=0;
		char buffer[SFTP_READDIR_NAMES_SIZE];
		unsigned int pos=0;
		unsigned int count=0;
		struct linux_dirent64 *de=NULL;
		unsigned char eof=0;
		int left=0;

		if (dirhandle->buffer==NULL) {

		    dirhandle->buffer=malloc(dirhandle->size);
		    if (dirhandle->buffer==NULL) {

			status=SSH_FX_FAILURE;
			goto out;

		    }

		    dirhandle->pos=0;

		}

		if (dirhandle->read==0) {
		    int read=0;

		    /* read a batch of dirents */

		    read =syscall(SYS_getdents64, dirhandle->handle.fd, (struct linux_dirent64 *) dirhandle->buffer, dirhandle->size);

		    if (read==-1) {

			status=SSH_FX_FAILURE;
			goto out;

		    } else if (read==0) {

			eof=1;
			goto finish;

		    }

		    dirhandle->read=(unsigned int) read;
		    left=dirhandle->read;

		}

		getdent:

		if (left>0) {
		    struct stat st;

		    de=(struct linux_dirent64 *) &dirhandle->buffer[dirhandle->pos];

		    logoutput("sftp_op_readdir: found %s", de->d_name);

		    if (fstatat(dirhandle->handle.fd, de->d_name, &st, AT_SYMLINK_NOFOLLOW)==0) {

			/* does it fit? */
			logoutput("sftp_op_readdir: ++");

			error=0;
			pos+=write_readdir_attr(&session->subsystem.sftp, &buffer[pos], SFTP_READDIR_NAMES_SIZE - pos, de->d_name, &st, valid, &error);

			if (error==0) {

			    count++;
			    dirhandle->pos += de->d_reclen;
			    left -= de->d_reclen;
			    if (dirhandle->pos < dirhandle->read) goto getdent;
			    dirhandle->pos=0;
			    dirhandle->read=0;

			}

		    } else {

			/* fstatat did not find this entry */

			dirhandle->pos += de->d_reclen;
			goto getdent;

		    }

		} else {

		    eof=1;

		}

		finish:

		logoutput("sftp_op_readdir: reply count %i pos %i", count, pos);

		if (reply_sftp_names(session, header->id, count, buffer, pos, eof)==-1) {

		    logoutput("sftp_op_readdir: error sending readdir names");

		}

		return;


	    } else {

		logoutput("sftp_op_readdir: handle not found");

		if (error==EPERM) {

		    /* serious error: client wants to use a handle he has no permissions for */

		    logoutput("sftp_op_readdir: client has no permissions to use handle");
		    goto disconnect;

		}

		status=SSH_FX_INVALID_HANDLE;

	    }

	}

    }

    out:

    reply_sftp_status_simple(session, header->id, status);
    return;

    disconnect:

    finish_session(session);

}

