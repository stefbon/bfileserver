/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "sftp-common-protocol.h"
#include "sftp-supported.h"
#include "sftp-attributes-read.h"
#include "sftp-reply.h"
#include "filehandle.h"
#include "sftp-handle.h"

/* TODO: some lock protection to make it atomic and a rollback construction */

static int _setstat_path(char *path, struct sftp_attr_s *attr, unsigned int *error)
{
    int result=0;

    if (attr->valid[SFTP_ATTR_INDEX_SIZE]) {

	if (truncate(path, attr->size)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set size to %li (%s)", errno, (long unsigned int) attr->size, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_USERGROUP]) {
	uid_t uid=(attr->flags & SFTP_ATTR_FLAG_VALIDUSER) ? attr->uid : (uid_t) -1;
	gid_t gid=(attr->flags & SFTP_ATTR_FLAG_VALIDGROUP) ? attr->gid : (gid_t) -1;

	/* set the uid and gid
	    NOTE: using (uid_t) -1 and (gid_t) -1 in fchown will ignore this value
	    */

	if (fchownat(0, path, uid, gid, 0)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set user and/or group (%s)", errno, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_PERMISSIONS]) {

	if (fchmodat(0, path, attr->type | attr->permissions, 0)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set mode (%s)", errno, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_ATIME] || attr->valid[SFTP_ATTR_INDEX_MTIME]) {
	struct timespec times[2];

	if (attr->valid[SFTP_ATTR_INDEX_ATIME]) {

	    times[0].tv_sec=attr->atime.sec;
	    times[0].tv_nsec=attr->atime.nsec;

	} else {

	    times[0].tv_sec=0;
	    times[0].tv_nsec=UTIME_OMIT;

	}

	if (attr->valid[SFTP_ATTR_INDEX_MTIME]) {

	    times[1].tv_sec=attr->mtime.sec;
	    times[1].tv_nsec=attr->mtime.nsec;

	} else {

	    times[1].tv_sec=0;
	    times[1].tv_nsec=UTIME_OMIT;

	}

	if (utimensat(0, path, times, 0)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set atime and/or mtime (%s)", errno, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_CTIME]) {

	logoutput("_setstat_path: change of ctime not supported");

    }

    out:

    return result;

}

int _setstat_fd(unsigned int fd, struct sftp_attr_s *attr, unsigned int *error)
{
    int result=0;

    if (attr->valid[SFTP_ATTR_INDEX_SIZE]) {

	if (ftruncate(fd, attr->size)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set size to %li (%s)", errno, (long unsigned int) attr->size, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_USERGROUP]) {
	uid_t uid=(attr->flags & SFTP_ATTR_FLAG_VALIDUSER) ? attr->uid : (uid_t) -1;
	gid_t gid=(attr->flags & SFTP_ATTR_FLAG_VALIDGROUP) ? attr->gid : (gid_t) -1;

	/* set the uid and gid
	    NOTE: using (uid_t) -1 and (gid_t) -1 in fchown will ignore this value
	    */

	if (fchown(fd, uid, gid)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set user and/or group (%s)", errno, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_PERMISSIONS]) {

	if (fchmod(fd, attr->type | attr->permissions)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set mode (%s)", errno, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_ATIME] || attr->valid[SFTP_ATTR_INDEX_MTIME]) {
	struct timespec times[2];

	if (attr->valid[SFTP_ATTR_INDEX_ATIME]) {

	    times[0].tv_sec=attr->atime.sec;
	    times[0].tv_nsec=attr->atime.nsec;

	} else {

	    times[0].tv_sec=0;
	    times[0].tv_nsec=UTIME_OMIT;

	}

	if (attr->valid[SFTP_ATTR_INDEX_MTIME]) {

	    times[1].tv_sec=attr->mtime.sec;
	    times[1].tv_nsec=attr->mtime.nsec;

	} else {

	    times[1].tv_sec=0;
	    times[1].tv_nsec=UTIME_OMIT;

	}

	if (futimens(fd, times)==-1) {

	    *error=errno;
	    logoutput("_setstat_path: error %i set atime and/or mtime (%s)", errno, strerror(errno));
	    result=-1;
	    goto out;

	}

    }

    if (attr->valid[SFTP_ATTR_INDEX_CTIME]) {

	logoutput("_setstat_path: change of ctime not supported");

    }

    out:

    return result;

}


/* SSH_FXP_SETSTAT
    message has the form:
    - byte 				SSH_FXP_SETSTAT
    - uint32				id
    - string				path
    - ATTR				attr
    */

void sftp_op_setstat(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_setstat");

    /* message should at least have 4 bytes for the path string, and 4 for the attributes
	note an empty path is possible */

    if (header->len>=8) {
	unsigned int pos=0;
	unsigned int left=header->len;
	struct sftp_attr_s attr;
	struct ssh_string_s path;

	memset(&attr, 0, sizeof(struct sftp_attr_s));

	path.len=get_uint32(&buffer[pos]);
	pos+=4;
	left-=4;
	path.ptr=&buffer[pos];
	pos+=path.len;
	left-=path.len;

	/* read attributes first */

	if (read_attributes_v06(sftp, &buffer[pos], left, &attr)>0) {
	    struct ssh_user_s *user=&session->user;
	    uid_t uid_keep=setfsuid(user->pwd.pw_uid);
	    gid_t gid_keep=setfsgid(user->pwd.pw_gid);
	    int result=0;
	    unsigned int error=0;

	    logoutput("sftp_op_setstat: pathlen %i", path.len);

	    if (path.len==0) {

		/* empty path resolves to the users home */
		result=_setstat_path(user->pwd.pw_dir, &attr, &error);

	    } else if (path.ptr[0]=='/') {
		char fullpath[path.len+1];

		/* absolute path */
		memcpy(fullpath, path.ptr, path.len);
		fullpath[path.len]='\0';
		result=_setstat_path(fullpath, &attr, &error);

	    } else {
		char fullpath[user->len_home + path.len + 2];
		unsigned int index=0;

		/* relative to users homedirectory */
		memcpy(fullpath, user->pwd.pw_dir, user->len_home);
		index=user->len_home;
		fullpath[index]='/';
		index++;
		memcpy(&fullpath[index], path.ptr, path.len);
		index+=path.len;
		fullpath[index]='\0';

		result=_setstat_path(fullpath, &attr, &error);

	    }

	    setfsuid(uid_keep);
	    setfsgid(gid_keep);

	    logoutput("sftp_op_setstat: result %i", result);

	    if (result==0) {

		reply_sftp_status_simple(session, header->id, 0);
		return;

	    } else {

		status=SSH_FX_FAILURE;
		if (error==0) error=EIO;

		if (error==ENOENT) {

		    status=SSH_FX_NO_SUCH_FILE;

		} else if (error==ENOTDIR) {

		    status=SSH_FX_NO_SUCH_PATH;

		} else if (error==EACCES || error==EPERM) {

		    status=SSH_FX_PERMISSION_DENIED;

		}

	    }

	}

    }

    logoutput("sftp_op_setstat: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

/* SSH_FXP_FSETSTAT/
    message has the form:
    - byte 				SSH_FXP_FSETSTAT
    - uint32				id
    - string				handle
    - ATTR				attr
    */

void sftp_op_fsetstat(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    struct sftp_subsystem_s *sftp=&session->subsystem.sftp;
    unsigned int status=SSH_FX_BAD_MESSAGE;

    logoutput("sftp_op_fsetstat");

    /* message should at least have 4 bytes for the handle, and 4 for the flags */

    if (header->len>=8) {
	unsigned int pos=0;
	unsigned int len=0;
	unsigned int left=header->len;
	unsigned int valid=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;
	left-=4;

	if (len + 8 < header->len && len==16) {
	    int result=0;
	    unsigned int error=0;
	    struct filehandle_s *filehandle=find_sftp_filehandle(&buffer[pos], session, &error);

	    if (filehandle) {
		struct sftp_attr_s attr;

		memset(&attr, 0, sizeof(struct sftp_attr_s));
		pos+=16;
		left-=16;

		if (read_attributes_v06(sftp, &buffer[pos], left, &attr)>0) {
		    struct ssh_user_s *user=&session->user;
		    uid_t uid_keep=setfsuid(user->pwd.pw_uid);
		    gid_t gid_keep=setfsgid(user->pwd.pw_gid);
		    int result=0;

		    result=_setstat_fd(filehandle->handle.fd, &attr, &error);

		    setfsuid(uid_keep);
		    setfsgid(gid_keep);

		    logoutput("sftp_op_setstat: result %i", result);

		    if (result==0) {

			reply_sftp_status_simple(session, header->id, 0);
			return;

		    } else {

			status=SSH_FX_FAILURE;
			if (error==0) error=EIO;

			if (error==ENOENT) {

			    status=SSH_FX_NO_SUCH_FILE;

			} else if (error==ENOTDIR) {

			    status=SSH_FX_NO_SUCH_PATH;

			} else if (error==EACCES || error==EPERM) {

			    status=SSH_FX_PERMISSION_DENIED;

			}

		    }

		}

	    } else {

		if (error==EPERM) {

		    /* serious error: client wants to use a handle he has no permissions for */

		    logoutput("sftp_op_fsetstat: client has no permissions to use handle");
		    goto disconnect;

		}

		status=SSH_FX_INVALID_HANDLE;

	    }

	}

    }

    logoutput("sftp_op_fsetstat: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

    disconnect:

    finish_session(session);

}
