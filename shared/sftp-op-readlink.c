/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <sys/fsuid.h>
#include <pwd.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "simple-list.h"
#include "localsocket.h"

#include "clientsession.h"
#include "ssh-utils.h"

#include "sftp-common-protocol.h"
#include "sftp-supported.h"
#include "sftp-attributes-write.h"

#include "sftp-reply.h"

#include "filehandle.h"
#include "sftp-handle.h"
#include "sftp-path.h"

static int reply_sftp_symlink(struct ssh_session_s *session, unsigned int id, char *target, unsigned int len)
{
    char data[len + 22];
    unsigned int pos=4;
    unsigned int error=0;

    data[pos]=SSH_FXP_NAME;
    pos++;

    store_uint32(&data[pos], id);
    pos+=4;

    store_uint32(&data[pos], 1); /* count always 1 with this case: symlink target*/
    pos+=4;

    store_uint32(&data[pos], len);
    pos+=4;

    memcpy(&data[pos], target, len);
    pos+=len;

    store_uint32(&data[pos], 0);
    pos+=4;

    data[pos]=0;
    pos++;

    store_uint32(&data[0], pos-4);

    return send_session_data(session, data, pos, &error);

}


/* SSH_FXP_READLINK/
    message has the form:
    - byte 				SSH_FXP_READLINK
    - uint32				id
    - string				path
    */

void sftp_op_readlink(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer)
{
    unsigned int status=SSH_FX_BAD_MESSAGE;

    /* message should at least have 4 bytes for the path string */

    if (header->len>=4) {
	unsigned int pos=0;
	unsigned int len=0;
	unsigned int valid=0;

	len=get_uint32(&buffer[pos]);
	pos+=4;

	if (len + 4 == header->len) {
	    struct ssh_user_s *user=&session->user;
	    unsigned int size=get_fullpath_len(user, len, &buffer[pos]); /* get size of buffer for path */
	    char path[size+1];
	    uid_t uid_keep=0;
	    gid_t gid_keep=0;
	    unsigned int error=0;
	    unsigned int buffsize=512;

	    get_fullpath(user, len, &buffer[pos], path);

	    while (1) {
		char buff[buffsize];
		int read=0;

		uid_keep=setfsuid(user->pwd.pw_uid);
		gid_keep=setfsgid(user->pwd.pw_gid);

		read=readlink(path, buff, buffsize);

		setfsuid(uid_keep);
		setfsgid(gid_keep);

		if (read==buffsize) {

		    buffsize+=512;
		    continue;

		} else if (read==-1) {

		    if (error==ENOENT) {

			status=SSH_FX_NO_SUCH_FILE;

		    } else if (error==ENOTDIR) {

			status=SSH_FX_NO_SUCH_PATH;

		    } else if (error==EACCES) {

			status=SSH_FX_PERMISSION_DENIED;

		    }

		    break;

		} else {

		    /* reply with read bytes */

		    if (reply_sftp_symlink(session, header->id, buff, read)==-1) {

			logoutput("sftp_op_readlink: error sending reply");

		    }

		    break;

		}

	    }

	}

    }

    logoutput("sftp_op_readlink: status %i", status);
    reply_sftp_status_simple(session, header->id, status);

}

