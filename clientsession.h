/*
  2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#ifndef BFILESERVER_CLIENTSESSION_H
#define BFILESERVER_CLIENTSESSION_H

#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <dirent.h>
#include <pwd.h>

#include "simple-list.h"
#include "simple-locking.h"
#include "beventloop.h"
#include "beventloop-xdata.h"
#include "localsocket.h"

#define SSH_STATUS_INIT				1
#define SSH_STATUS_SUBSYSTEM			2
#define SSH_STATUS_ERROR			4
#define SSH_STATUS_FINISH			8

#define SSH_SUBSYSTEM_TYPE_SFTP			1
#define SSH_SUBSYSTEM_TYPE_BACKUP		2

#define SFTP_EXTENDED_OP_MIN			210
#define SFTP_EXTENDED_OP_MAX			255

struct ssh_status_s {
    unsigned char				status;
    unsigned int				type;
};

#define NETWORK_ADDRESS_TYPE_IPV4		1
#define NETWORK_ADDRESS_TYPE_IPV6		2

struct network_address_s {
    unsigned char				type;
    union {
	char					v4[INET_ADDRSTRLEN];
	char					v6[INET6_ADDRSTRLEN];
    } ip;
};

#define SSH_SIGNAL_FLAG_MUTEXALLOCATED		1
#define SSH_SIGNAL_FLAG_CONDALLOCATED		2

struct ssh_signal_s {
    unsigned int				flags;
    pthread_mutex_t				*mutex;
    pthread_cond_t				*cond;
};

struct ssh_payload_s {
    unsigned char				type;
    struct list_element_s			list;
    struct ssh_session_s			*session;
    unsigned int				size;
    char					buffer[];
};

struct payload_queue_s {
    struct list_element_s			*head;
    struct list_element_s			*tail;
    struct ssh_signal_s				signal;
    void					(* process_payload)(struct ssh_payload_s *payload);
};

struct ssh_receive_s {
    struct payload_queue_s			payload_queue;
    unsigned int				threads;
    void					(* queue_data)(struct ssh_session_s *session, char *buffer, unsigned int len);
    unsigned int				size;
    char					*buffer;
};

struct ssh_user_s {
    struct passwd				pwd;
    char					*buffer;
    unsigned int				size;
    unsigned int				len_username;
    unsigned int				len_home;
};

struct sftp_header_s {
    unsigned char				type;
    unsigned int				id;
    unsigned int 				len;
};

typedef void (* sftp_cb_t)(struct ssh_session_s *s, struct sftp_header_s *h, char *buffer);

#define SFTP_STATUS_INIT			1
#define SFTP_STATUS_UP				2
#define SFTP_STATUS_ERROR			3

#define SFTP_ATTR_INDEX_SIZE			0
#define SFTP_ATTR_INDEX_USERGROUP		1
#define SFTP_ATTR_INDEX_PERMISSIONS		2
#define SFTP_ATTR_INDEX_ATIME			3
#define SFTP_ATTR_INDEX_ATIME_N			4
#define SFTP_ATTR_INDEX_MTIME			5
#define SFTP_ATTR_INDEX_MTIME_N			6
#define SFTP_ATTR_INDEX_CTIME			7
#define SFTP_ATTR_INDEX_CTIME_N			8

#define SFTP_ATTR_FLAG_NOUSER			1
#define SFTP_ATTR_FLAG_NOGROUP			2
#define SFTP_ATTR_FLAG_USERNOTFOUND		4
#define SFTP_ATTR_FLAG_GROUPNOTFOUND		8
#define	SFTP_ATTR_FLAG_VALIDUSER		16
#define	SFTP_ATTR_FLAG_VALIDGROUP		32

struct sftp_subsystem_s;

struct ssh_string_s {
    unsigned int				len;
    char					*ptr;
};

struct sftp_user_s {
    union {
	struct ssh_string_s 			name;
	unsigned int				id;
    } remote;
    uid_t					local_uid;
};

struct sftp_group_s {
    union {
	struct ssh_string_s 			name;
	unsigned int				id;
    } remote;
    gid_t					local_gid;
};

struct sftp_time_s {
    int64_t					sec;
    uint32_t					nsec;
};

struct sftp_attr_s {
    unsigned int 				flags;
    unsigned char				valid[9];
    unsigned int				count;
    unsigned int				type;
    uint64_t					size;
    uid_t					uid;
    struct ssh_string_s				user;
    gid_t					gid;
    struct ssh_string_s				group;
    uint32_t					permissions;
    struct sftp_time_s				atime;
    struct sftp_time_s				mtime;
    struct sftp_time_s				ctime;
};

struct sftp_extension_s {
    unsigned int				len;
    const char					*name;
    void					(* cb)(struct ssh_session_s *session, struct sftp_header_s *header, char *buffer);
};

struct sftp_subsystem_s {
    unsigned int 				version;
    unsigned int				status;
    sftp_cb_t					cb[256];
    unsigned char				extended_op_start;
    pthread_mutex_t				mutex;
};

struct backup_subsystem_s {
    unsigned int 				version;
    unsigned int				status;
    sftp_cb_t					cb[256];
    unsigned char				extended_op_start;
    pthread_mutex_t				mutex;
    unsigned int				db_count;
    struct list_header_s			db_conn;
    struct simple_locking_s			locking;
};

#define COMMONHANDLE_TYPE_FILE			1
#define COMMONHANDLE_TYPE_DIR			2

#define COMMONHANDLE_FLAG_CREATE		1
#define COMMONHANDLE_FLAG_CLOSE			2

#define FILEHANDLE_BLOCK_READ			1
#define FILEHANDLE_BLOCK_WRITE			2
#define FILEHANDLE_BLOCK_DELETE			4

/*
    TODO:
    - add protection to not close (=remove) a handle when busy
    - add functions like read, write, close, fstat, fsetstat, block, unblock, fsync and readdir, closedir
*/

struct commonhandle_s {
    struct ssh_session_s			*session;
    unsigned char				flags;
    unsigned char				type;
    dev_t					dev;
    uint64_t					ino;
    unsigned int				fd;
    unsigned int				refcount;
    void					(* close)(struct commonhandle_s *handle);
};

union serverflags_u {
    struct sftp_s {
	unsigned int				access;
	unsigned int				flags;
    } sftp;
};

struct filehandle_s {
    struct commonhandle_s			handle;
    unsigned int				posix_flags;
    union serverflags_u				server;
    int						(* get_lock_flags)(struct filehandle_s *h, const char *what);
};

struct insert_filehandle_s {
    unsigned int				status;
    unsigned int				error;
    union serverflags_u				server;
    union {
	struct lockconflict_s {
	    uid_t				uid;
	    struct network_address_s		address;
	} lock;
    } info;
};

struct dirhandle_s {
    struct commonhandle_s			handle;
    unsigned int				size;
    char					*buffer;
    unsigned int				pos;
    unsigned int				read;
};

struct ssh_session_s {
    struct ssh_status_s				status;
    struct ssh_user_s				user;
    struct fs_connection_s 			connection;
    struct network_address_s			address;
    struct ssh_receive_s			receive;
    union subsystem_s {
	struct sftp_subsystem_s			sftp;
	struct backup_subsystem_s		backup;
    } subsystem;
};

struct ssh_session_s *get_session_connection(struct fs_connection_s *conn);
struct ssh_session_s *create_session(uid_t uid, unsigned char type);

void set_session_process_payload(struct ssh_session_s *session, void (* process_payload_cb)(struct ssh_payload_s *payload));
void finish_session(struct ssh_session_s *session);
void terminate_session_unlocked(struct ssh_session_s *session);
int send_session_data(struct ssh_session_s *session, char *data, unsigned int size, unsigned int *error);

#endif
