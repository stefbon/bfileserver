/*
  2010, 2011, 2012, 2013, 2014 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/
#ifndef BFILESERVER_OPTIONS_H
#define BFILESERVER_OPTIONS_H

#include "pathinfo.h"

#define BFILESERVER_CONFIGFILE 		"/etc/bfileserver/options"
#define BFILESERVER_SOCKET 		"/var/lib/bfileserver/"

struct fs_options_struct {
    struct pathinfo_s 			configfile;
    struct pathinfo_s 			socket;
    unsigned char			fuse;
    unsigned char			sftp;
    unsigned char			backup;
};

// Prototypes

int parse_arguments(int argc, char *argv[], unsigned int *error);
void free_options();

#endif
