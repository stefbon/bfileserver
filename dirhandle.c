/*
  2010, 2011, 2012, 2103, 2014, 2015, 2016, 2017 Stef Bon <stefbon@gmail.com>

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "global-defines.h"

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <err.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <inttypes.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vfs.h>
#include <pwd.h>
#include <grp.h>

#include <linux/kdev_t.h>

#include "main.h"
#include "logging.h"
#include "pathinfo.h"
#include "utils.h"

#include "beventloop.h"
#include "localsocket.h"
#include "clientsession.h"
#include "ssh-utils.h"
#include "simple-locking.h"
#include "commonhandle.h"
#include "dirhandle.h"

static void close_dirhandle(struct commonhandle_s *handle)
{

    if (handle->type==COMMONHANDLE_TYPE_DIR) {
	struct dirhandle_s *dirhandle=(struct dirhandle_s *) ( ((char *) handle) - offsetof(struct dirhandle_s, handle));

	if (handle->fd>0) {

	    close(handle->fd);
	    handle->fd=0;

	}

	if (dirhandle->buffer) {

	    free(dirhandle->buffer);
	    dirhandle->buffer=NULL;

	}

	free(dirhandle);

    }


}

struct dirhandle_s *find_dirhandle(struct ssh_session_s *session, dev_t dev, uint64_t ino, unsigned int fd, unsigned int *error)
{
    struct commonhandle_s *handle=find_commonhandle(session, dev, ino, fd, error);
    struct dirhandle_s *dirhandle=NULL;

    if (handle && handle->type==COMMONHANDLE_TYPE_DIR) {

	dirhandle = (struct dirhandle_s *) ( ((char *) handle) - offsetof(struct dirhandle_s, handle));

    }

    return dirhandle;

}

struct dirhandle_s *insert_dirhandle(struct ssh_session_s *session, dev_t dev, uint64_t ino, unsigned int *error)
{
    struct dirhandle_s *dirhandle=NULL;

    logoutput("insert_dirhandle");

    dirhandle=malloc(sizeof(struct dirhandle_s));

    if (dirhandle) {

	memset(dirhandle, 0, sizeof(struct dirhandle_s));

	dirhandle->handle.session=session;
	dirhandle->handle.flags=0;
	dirhandle->handle.type=COMMONHANDLE_TYPE_DIR;
	dirhandle->handle.dev=dev;
	dirhandle->handle.ino=ino;
	dirhandle->handle.fd=0;
	dirhandle->handle.refcount=0;
	dirhandle->handle.close=close_dirhandle;

	dirhandle->size=0;
	dirhandle->buffer=NULL;
	dirhandle->pos=0;
	dirhandle->read=0;

	logoutput("insert_dirhandle: insert_commonhandle_hash");

	insert_commonhandle_hash(&dirhandle->handle);

	logoutput("insert_dirhandle: insert_commonhandle_hash: after");

    } else {

	*error=ENOMEM;

    }

    return dirhandle;

}

void decrease_dirhandle(struct dirhandle_s **pdirhandle)
{
    struct dirhandle_s *dirhandle=*pdirhandle;
    struct simple_lock_s lock;

    writelock_commonhandles(&lock);

    dirhandle->handle.refcount--;

    if (dirhandle->handle.refcount==0 && (dirhandle->handle.flags & COMMONHANDLE_FLAG_CLOSE)) {

	remove_commonhandle_hash(&dirhandle->handle);
	close_dirhandle(&dirhandle->handle);
	*pdirhandle=NULL;

    }

    unlock_commonhandles(&lock);

}

void release_dirhandle(struct dirhandle_s **pdirhandle)
{
    struct dirhandle_s *dirhandle=*pdirhandle;
    struct simple_lock_s lock;

    writelock_commonhandles(&lock);
    remove_commonhandle_hash(&dirhandle->handle);
    unlock_commonhandles(&lock);

    close_dirhandle(&dirhandle->handle);
    *pdirhandle=NULL;

}
